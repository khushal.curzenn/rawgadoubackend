const mongoose = require("mongoose");
const CoupensSchema = new mongoose.Schema({
    name:{ type: String, required: true },
     type:{ type: String, default:"" },
    code: { type: String, required: true },
    discountamount: { type: Number, required: true },
    limit: { type: Number, required: true },
    used: { type: Number, default:0 },
    provider_id:{ type: String, default:"" },
    start_date: { type: Date, required: true },
    end_date: { type: Date, required: true },
    status:{type:Boolean,default:true},
    products:{type:Array}
});
module.exports = mongoose.model("Coupens", CoupensSchema);