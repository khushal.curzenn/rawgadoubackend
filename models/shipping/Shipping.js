const mongoose=require("mongoose");
const ShippingSchema=mongoose.Schema({
    type:{type:String,default:"u"},
    method_name:{type:String,default:""},
    zone_list:{type:Array,default:[]},
    catagory_list:{type:Array,default:[]},
    // dimension:{
    //     min_height:{type:Number},
    //     max_height:{type:Number},
    //     min_width:{type:Number},
    //     max_width:{type:Number},
    //     min_length:{type:Number},
    //     max_length:{type:Number}
    // },
    min_weight:{type:Number,default:0},
    max_weight:{type:Number,default:0},
    cost_base:{type:Number,default:0},
    cost_additional_weight:{type:Number,default:0},
    cose_per_unit:{type:Number,default:0},
    number_of_days:{type:Number,default:0},
    vendor_id:{type:String,default:0}
});
module.exports=mongoose.model("Shipping",ShippingSchema);