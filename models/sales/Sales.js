const mongoose=require("mongoose");

const SalesSchema=new mongoose.Schema({
    product_id:{type:String},
      totalsales: {
        type: Number,
        required: true,
      },
      userIds:{type:Array}
},{
    timestamps:true
}
);

module.exports=mongoose.model("sales",SalesSchema);