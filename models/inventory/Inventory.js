const mongoose=require("mongoose");

const InventorySchema=new mongoose.Schema({
    provider_id:{type:String,default:""},
    product_id:{type:String,default:""},
      quantity: {
        type: Number,
        required: true,
      }
},{
    timestamps:true
}
);

module.exports=mongoose.model("inventory",InventorySchema);