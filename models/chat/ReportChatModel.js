const mongoose = require("mongoose");
const Schema = require("mongoose");

const ReportChatSchema = new mongoose.Schema({
  room_id:{type:String,default:null},
  product_id:{type:String,default:null},
  seller_id:{type:String,default:null},
  user_id:{type:String,default:null},
  reported_by:{type:Number,default:0}, //1 =User 2 = Seller
  reason:{type:String,default:null},
  created_at:{type:Date,default:Date.now},
  updated_at:{type:Date,default:Date.now},
});

module.exports = mongoose.model("reportchat", ReportChatSchema);