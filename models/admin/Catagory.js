const mongoose=require("mongoose");
const CatagorySchema=mongoose.Schema({
   mastercatagory:{type:mongoose.Types.ObjectId},
   //title:{type:String,unique:true},
   title:{type:String,default:''},
   image:{type:String,default:''},
   is_parent:{type:Boolean},
   is_active:{type:Boolean,default:true},
   price:{type:Number}
});
module.exports=mongoose.model("Catagory",CatagorySchema);