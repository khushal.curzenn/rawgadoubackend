const mongoose=require("mongoose");
const PromotionSchema=new mongoose.Schema({
    promotion_name:{type:String,required:true},
    promotion_description:{type:String,required:true},
    promotion_expiry_date:{type:Date,required:true},
    promotion_start_date:{type:Date,required:true},
    promotion_type:{type:String,required:true},
    promotion_code:{type:String,required:true},
    promotion_amount:{type:Number,required:true},
    promotion_percentage:{type:Number,required:true},
    promotion_status:{type:String,required:true},
});
module.exports=mongoose.model("Promotion",PromotionSchema);