const mongoose=require("mongoose");
const ContactusSchema=mongoose.Schema({
   
    title:{type:String,required:true},
    reason:{type:String,required:true},
    type:{type:String},
    id:{type:String},
    message:{type:String},
    docs:{type:Array},
    name:{type:String}
});
module.exports=mongoose.model("Contactus",ContactusSchema);