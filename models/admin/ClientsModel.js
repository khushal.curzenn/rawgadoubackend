const mongoose = require("mongoose");
const Schema = require("mongoose");

const ClientsSchema = new mongoose.Schema({
  user_id: [{ type: Schema.Types.ObjectId,ref:"User" }],
  full_name: { type: String, default: null },
  email: { type: String, default: null },
  contact_person_name: { type: String, default: null },
  mobile_number: { type: Number, default: null },
  comment: { type: String, default: null },
  is_checked:{type:Boolean, default:true},

  contact_person: { type: String, default: null },
  streat_po_box: { type: String, default: null },
  office_code: { type: String, default: null },
  building_number: { type: String, default: null },
  lift: { type: String, default: null },
  floor: { type: String, default: null },
  intercom: { type: String, default: null },
  locality: { type: String, default: null },
  sender_contact_number: { type: Number, default: null },
  address: { type: String, default: null },
  location: {
   type: { type: String },
   coordinates: []
  },
  status: { type: Number, default: 1 }, // 0 Not verified 1 verified 2 unapproved
  deleted_at: { type: Number, default: 0 },
  created_at: { type: Date, default: Date.now },
  updated_at: { type: Date, default: Date.now }
});

ClientsSchema.index({location: '2dsphere'});
module.exports = mongoose.model("clients", ClientsSchema);