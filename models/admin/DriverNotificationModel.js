const mongoose = require("mongoose");
const Schema = require("mongoose");
const DrivernotificationSchema = new mongoose.Schema({

  route_id:[{ type: Schema.Types.ObjectId,ref:"routeplans" , default: null}], 
  driver_id:[{ type: Schema.Types.ObjectId,ref:"drivers" , default: null}],
  message:{type:String,default:null},
  is_read:{ type: Number, default: 0 },//0 not read  1 read 
  created_at: { type: Date, default: Date.now },
  updated_at: { type: Date, default: Date.now }
});


module.exports = mongoose.model("drivernotifications", DrivernotificationSchema);