const mongoose = require("mongoose");
const Schema = require("mongoose");

const DriversupportSchema = new mongoose.Schema({
  description: { type: String, default: null },
  //check_time: { type: Number, default: (new Date()).getTime() },
  created_at: { type: Date, default: Date.now },
  updated_at: { type: Date, default: Date.now }
});

 
module.exports = mongoose.model("driversupport", DriversupportSchema);