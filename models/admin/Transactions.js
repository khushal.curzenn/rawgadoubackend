const mongoose = require("mongoose");
const TransactionsSchema = new mongoose.Schema({
   
   payer_id: { type: String },
    
    payment_amount: { type: Number },
    payment_type: { type: String},
    payment_transaction_id: { type: String },
    payment_status: { type: String },
    payment_date: { type: Date },
    remarks: { type: String },
    
});
module.exports = mongoose.model("Transactions", TransactionsSchema);