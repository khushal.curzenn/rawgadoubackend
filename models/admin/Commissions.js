const mongoose=require("mongoose");
const CommissionSchema=mongoose.Schema({
   
    entityId:{type:String},
    commission:{type:Number,default:0}
});
module.exports=mongoose.model("Commission",CommissionSchema);