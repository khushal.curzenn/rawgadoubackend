const mongoose=require("mongoose");
const AdminCommisionSchema=mongoose.Schema({
    commision:{type:Number,required:true,unique:true},
    active:{type:Boolean,required:true},
});
module.exports=mongoose.model("AdminCommision",AdminCommisionSchema);