const mongoose = require("mongoose");
const Schema = require("mongoose");
const StopsSchema = new mongoose.Schema({
  shipment_id:[{ type: Schema.Types.ObjectId,ref:"shipments" , default: null}],
  route_id:[{ type: Schema.Types.ObjectId,ref:"routeplans" , default: null}],
  orderType: { type: String, default: "" },
  user_order_id: { type: String, default: "" },
  user_order_id_arr: { type: Array, default: [] },
  routeType: { type: String, default: 'stop' },
  fullName: { type: String, default: "" },
  email: { type: String,  default: ""},
  mobileNumber: { type: String, default: "" },
  notes: { type: String,  default: "" },
  priority: { type: String,  default: "" },
  serviceTime: { type: String,  default: "" },
  businessName: { type: String,  default: "" },
  stopType: { type: String,  default: "" },
  parcelFront: { type: String,  default: "" },
  parcelLeft: { type: String,  default: "" },
  parcelFloor: { type: String,  default: "" },
  parcelCount: { type: String,  default: "" },
  image: { type: String, default: "" },
  address: { type: String, default: "" },
  location: {
   type: { type: String },
   coordinates: []
  },
  status: { type: Number, default: 0 }, // 1 Assigned to driver 2 start 3 delivered 4 cancled
  deliveryText: { type: String, default: "" },
  deliverySignature: { type: String, default: "" },
  deliveryPhoto: { type: String, default: "" },
  deliveryNoteClient: { type: String, default: "" },
  deliveryNoteCompany: { type: String, default: "" },
  position: { type: Number, default: 0 }, 
  distance: { type: Number, default: 0 },
  distanceTime: { type: Number, default: 0 },
  trip_status:{ type: Number, default: 0 },//0 free  1 busy
  created_at: { type: Date, default: Date.now },
  updated_at: { type: Date, default: Date.now }
});
StopsSchema.index({location: '2dsphere'});
module.exports = mongoose.model("stops", StopsSchema);