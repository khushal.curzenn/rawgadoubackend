const mongoose=require("mongoose");

const AuctionbidsSchema=mongoose.Schema({
    user_id:{type:String,default:null},
   product_id:{type:String,default:null},
   bid_id:{type:String,default:null},
   bid_amount:{type:Number,default:0},
   bid_winner:{type:Boolean,default:false},
   is_payment:{type:Boolean,default:false},
   bid_time:{type:Date,default:new Date()},
   created_at: { type: Date, default: Date.now },

});
module.exports=mongoose.model("auctionbids",AuctionbidsSchema);