const mongoose = require("mongoose");
const UserWebHistorySchema = new mongoose.Schema({
  user_id:{type:String},
  product_id:{type:String},
  created_at:{type:Date,default:new Date()}
});
module.exports=mongoose.model("userwebhistory",UserWebHistorySchema);