const mongoose = require("mongoose");

const VariationPriceSchema = mongoose.Schema({
    variation_1: { type: String, default: null },
    variation_option_1: { type: Array, default: [] },
    variation_2: { type: String, default: null },
    variation_option_2: { type: Array, default: [] },
    price: { type: Array, default: [] },
    stock: { type: Array, default: [] },
    variations: {
        type: Array, default: []
    },
    variation_options: {
        type: Array, default: []
    },
    product_id: { type: String },
    sku: { type: Array, default: [] },
    images:{type:Array,default:[]}
});
module.exports = mongoose.model("VariationPrice", VariationPriceSchema);