const mongoose=require("mongoose");

const AuctioninitiateSchema = mongoose.Schema({
    user_id:{type:String,default:null},
   product_id:{type:String,default:null},
   bid_id:{type:String,default:null},
   amount:{type:Number,default:0},
   is_payment:{type:Boolean,default:false},
   payment_date: { type: Date, default: null },
   created_at: { type: Date, default: Date.now },

});
module.exports=mongoose.model("auctioninitiate",AuctioninitiateSchema);