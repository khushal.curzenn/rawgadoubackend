const mongoose=require("mongoose");
const variation_optionsSchema=new mongoose.Schema({
  images:{type:Array,default:[]},
  option:{type:String,default:""}
})
const sales_infoSchema=new mongoose.Schema({
  // "sku":{type:String,default:"0"},
  // "price":{type:Number,default:0},
  // "stock":{type:Number,default:0},
  //image or others
  type:{type:String,default:""},
  variation:{type:String,default:""},
  options:{type:[variation_optionsSchema],default:[]},
});
const VariationPriceSchema = mongoose.Schema({
    sku:{ type: String },
    images:{type:Array,default:[]},
    price: { type: Number, default: 0 },
    stock: { type: Number ,default: 0 },
    variation_1: { type: String, default: null },
    variation_option_1: { type: String, default: null },
    variation_2: { type: String, default: null },
    variation_option_2: { type: String, default: null },
    // variations: {
    //     type: Array, default: []
    // },
    // variation_options: {
    //     type: Array, default: []
    // },
    product_id: { type: String },

});
const ProductSchema=new mongoose.Schema({
    category_id:{type:Array, default:[]},
    condition:{type:Boolean,default:true},
    sku:{type:String},
    stock:{type:Number},
    parent_sku:{type:String},
    title:{type:String},
    bid_id:{type:String,default:null},
    is_auction:{type:Boolean,default:false},
    auction_price: { type: Number, default: 0 },
    auction_start_date:{type:Date,default:new Date()},
    auction_end_date:{type:Date,default:new Date()},
    
    price:{type:Number,default:0},
    description:{type:String},
    catagory:{type:String},
    images:{type:Array},
    dangerous_goods:{type:Boolean,default:false},
    video:{type:String},
    provider_id:{type:String},
    spec_attributes:{type:Array},
    reported:{
      violations:{type:Number,default:0},
      users:{type:Array,default:[]}
    },

    sales_info:{type:[sales_infoSchema]},
    sales_info_variation_prices:{type:[VariationPriceSchema]},
    dispatch_info:{
    weight:{type:Number,default:0},
    size:{
        width:{type:Number,default:0},
        Length:{type:Number,default:0},
        Height:{type:Number,default:0}
    },
    shipping_details:{type:Object},
    },
    other_details:{type:Object},
    is_active:{type:Boolean,default:true},
    enable_variation:{type:Boolean,default:false},
    view_count:{type:Number,default:0},
    seller_will_bear_shipping:{type:Boolean,default:true},
    is_delisted:{type:Boolean,default:false},
    is_deleted:{type:Boolean,default:false},
    created_at: { type: Date, default: Date.now },

})
ProductSchema.statics.makeActive=async function(id){
  let product=await this.findOne({_id:id})
  product.is_active=true;
  product.save().then(()=>true)
}
ProductSchema.statics.makeInActive=async function(id){
    let product=await this.findOne({_id:id})
    product.is_active=false;
    product.save().then(()=>true)
    
  }
ProductSchema.statics.softdelete=async function(id){
    let product=await this.findOne({_id:id});
    product.is_deleted=true
    product.save().then(()=>true)
}

ProductSchema.statics.filterwithAttributes=async function(AttrObj){
  return this.find(AttrObj)
}
const Product= mongoose.model("products", ProductSchema);
module.exports = {
   Product,
    ProductSchema
};

