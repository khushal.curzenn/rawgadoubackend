const mongoose = require("mongoose");

const ProductstempimagesSchema = new mongoose.Schema({
  random_product_id: { type: String, default: null },
  images: { type: Array, default: [] },
  created_at: { type: Date, default: Date.now },
  updated_at: { type: Date, default: Date.now }
});

module.exports = mongoose.model("productstempimages", ProductstempimagesSchema);