
const mongoose = require('mongoose');
const moment = require("moment")
require("../config/database").connect();
const bookingmodel=require("../models/events/Booking");
const OrganizerModel=require("../models/events/Organizer");
const { Product } = require("../models/products/Product");
const AuctionbidsModel = require("../models/products/AuctionbidsModel");

const Users = require("../models/user/User");
const  customConstant  = require("../helpers/customConstant");
const nodemailer = require("nodemailer");
//
 
const { forEach } = require("async");
const transport = nodemailer.createTransport({
  name: "Rawgadou",
  host: process.env.MAILER_HOST,
  port: process.env.MAILER_PORT,
  auth: {
    user: process.env.MAILER_EMAIL_ID,
    pass: process.env.MAILER_PASSWORD,
  }
});
const checkifappointmentiscomplete = async () => {
    console.log("cron job");
    console.log("cron job");
    try{
      
        let today_date = new Date();
  
        var pro_rec_auction_over = await Product.aggregate([
            {
              $match:{
                "is_auction":true
              }
            },{
              $match:{
                "auction_end_date":{$lt:today_date}
              }
            },
            {
              $lookup:{
                from:"auctionbids",
                let:{"pro_idd":{"$toString":"$_id"}},
                pipeline:[
                  {
                    $match:{
                      $expr:{
                        $eq:["$product_id","$$pro_idd"]
                      }
                    }
                  },
                  {
                    $sort:{
                      "bid_time":-1
                    }
                  },
                  {
                    $limit:1
                  }
                ],
                as:"auctionAmount"
              }
            },
            {
              $skip:0
            },
            {
              $limit:1
            }
            // { $match: { auctionAmount: { $ne: [] } } },
            // {
            //   $project:{
            //     "auctionAmount": 1,
            //     "_id": 1,
            //   }
            // },
        ]);
        for(let x=0; x<pro_rec_auction_over.length; x++)
        {
          //console.log("id ",pro_rec_auction_over[x] );
          //console.log("id ",pro_rec_auction_over[x]._id );
          //console.log("id ",pro_rec_auction_over[x].auctionAmount );
          if(pro_rec_auction_over[x].auctionAmount)
          {
            if(pro_rec_auction_over[x].auctionAmount.length > 0)
            {
              await AuctionbidsModel.updateOne({_id:pro_rec_auction_over[x].auctionAmount[0]._id},{bid_winner:true})
              let id = pro_rec_auction_over[x].auctionAmount[0]._id;
              let user_id = pro_rec_auction_over[x].auctionAmount[0].user_id;
              let product_id = pro_rec_auction_over[x].auctionAmount[0].product_id;
              let rec_pro = await Product.findOne({_id:product_id},{auction_start_date:1,auction_end_date:1});
              let winner_user = await Users.findOne({_id:user_id},{email:1});
              //console.log("rec_pro ", rec_pro);
              //console.log("winner_user ", winner_user);
              let auction_start_date = rec_pro.auction_start_date;
              let auction_end_date = rec_pro.auction_end_date;
              //return false;
              let old_auction_bidding = await AuctionbidsModel.aggregate(
                [
                  {
                    $match:
                    {
                      $and:[
                      {product_id: product_id},
                      {user_id: { $ne:user_id }},
                      {bid_time:{$gte:auction_start_date }},
                      {bid_time:{ $lte:auction_end_date } }
                      ]
                    }
                  },
                  {
                    $lookup:{
                      from:"users",
                      let:{"user_id":{"$toObjectId":"$user_id"}},
                      pipeline:[
                        {
                          $match:{
                            $expr:{
                              $eq:[ "$_id","$$user_id"]
                            }
                          }
                        }
                      ],
                      as:"all_users"
                    }
                  },
                  {
                    $lookup:{
                      from:"products",
                      let:{"pro_id":{"$toObjectId":"$product_id"}},
                      pipeline:[
                        {
                          $match:{
                            $expr:{
                              $eq:["$_id","$$pro_id"]
                            }
                          }
                        }
                      ],
                      as:"product_rec"
                    }
                  },
                  {
                    $project:{
                      "all_users.email":1,
                      "all_users.first_name":1,
                      "all_users.last_name":1,
                      "product_rec.title":1
                    }
                  }
                ]);
              //console.log("old_auction_bidding ", JSON.stringify(old_auction_bidding));
              
              let all_email = old_auction_bidding.map((val)=> val.all_users[0].email);
              //console.log("all_email ", all_email);
              //return false;
              // old_auction_bidding.forEach(async(val)=>{
              //   // console.log("val ", val.all_users[0].email);
              //   // console.log("val ", val );
              //   // let email = val.all_users[0].email;
              //   // let fullname = val.all_users[0].first_name+" "+val.all_users[0].last_name;
              // });
              var base_url_server = customConstant.base_url;
              let bid_amount = pro_rec_auction_over[x].auctionAmount[0].bid_amount;
              var imagUrl = base_url_server+"public/uploads/logo.png";
              let html = "";
              html += "<div class='mail-content' style='position: relative;display: block;font-size: 14px;line-height: 25px;font-family: Poppins, sans-serif;'>";
              html += "<div class='content'></div>";
               
                html+= `Les enchères sur ce produit sont terminées. L’article a été adjugé à ${bid_amount} CFA pour un autre enchérisseur.`;
                //html+= '<a href="'+product_url+'">'+' pour la vérification de email, cliquez ici '+'</strong>';
              html += "</div>";
              html += "<div class='mail-footer'>";
              html += "<img src='"+imagUrl+"' height='150' width='250'>";
              html += "</div>";
              html += "<i>Rawgadou</i>";
        
              let messageEmail = html;
              const message = {
              from: process.env.MAILER_FROM_EMAIL_ID, // Sender address
              to: all_email,         // recipients
              subject: "Nouvelle L’article a été adjugé", // Subject line
              html: messageEmail // Plain text body
              };
              await transport.sendMail(message, function(err, info) {
                  if (err) {
                      console.log("errr",err);
                      //error_have = 1;
                      
                  } else {
                    //console.log("all_email ", all_email);
                      console.log("no err 2190 ");
                      let html = "";
                          html += "<div class='mail-content' style='position: relative;display: block;font-size: 14px;line-height: 25px;font-family: Poppins, sans-serif;'>";
                          html += "<div class='content'></div>";
                          
                            html+= `La Vente aux Enchères est terminée. Félicitations ! Vous êtes adjudicataire du Rawgadou. Nous vous contacterons dans les 24h pour la livraison du l’article. Merci de préparer le paiement.`;
                            //html+= '<a href="'+product_url+'">'+' pour la vérification de email, cliquez ici '+'</strong>';
                          html += "</div>";
                          html += "<div class='mail-footer'>";
                          html += "<img src='"+imagUrl+"' height='150' width='250'>";
                          html += "</div>";
                          html += "<i>Rawgadou</i>";
                          let messageEmail = html;
                          const message = {
                          from: process.env.MAILER_FROM_EMAIL_ID, // Sender address
                          to: winner_user.email,         // recipients
                          subject: "Vente aux enchères remportée", // Subject line
                          html: messageEmail // Plain text body
                          };
                          transport.sendMail(message, function(err, info) {
                              if (err) {
                                  console.log("errr",err);
                                  //error_have = 1;
                                  
                              } else {
                                console.log("no errr 2201 " );
                               }
                            });
                  }
              });
              
            }
          }
          await Product.updateOne({_id:pro_rec_auction_over[x]._id},{is_auction:false});
        }
        console.log("success cron run");
        //console.log(today_date);
        // return res.send({
        //   status:true,
        //   message:"Success",
        //   data:pro_rec_auction_over
        // });
      }catch(e){
        console.log(e.message);
      }
}


module.exports = {
   
    checkifappointmentiscomplete
}
