
const Event=require("../../models/events/Event");
const EventRating = require("../../models/actions/EventRatings");
const User= require("../../models/user/User");
var QRCode = require('qrcode');
const OrganizerModel=require("../../models/events/Organizer");
const CatagoryModel=require("../../models/events/ECatagory");
const EventbannerModel = require("../../models/events/EventbannerModel");

const {getPreviousDay,frenchDayDate}=require("../../modules/dates")
const EPaymentRequestmodel=require("../../models/admin/EPaymentRequest");
const bookingmodel=require("../../models/events/Booking");
const stringGen= require("../../modules/randomString");
const pdf = require('html-pdf');
const ejs = require("ejs");
const path = require("path");
const fs = require('fs');
const BASE_URL=process.env.BASE_URL;
const mongoose=require("mongoose");
const OrderModel = require("../../models/orders/order");
const sellerModel=require("../../models/seller/Seller");
const customConstant = require('../../helpers/customConstant');
//const User = require('../../models/user/User');
async function updatearrayelement(model,olddata,dataarray,elemid,modelid,datatoupdate) {
    let isaddressalreadythere = olddata[dataarray]?.filter((add) => add?._id+'' == elemid)
    console.log("isaddressalreadythere",isaddressalreadythere)
    const query = { _id: modelid };
    let fieldtoqueryinarray=`${dataarray}.$[item]`
    console.log("fieldtoqueryinarray",fieldtoqueryinarray)
    const updateDocument = {
       
    };
    updateDocument[fieldtoqueryinarray]=datatoupdate
    console.log("updateDocument",updateDocument)
    const options = {
        arrayFilters: [
            {
                "item._id": elemid,

            },
        ],
    };
    const result = await model.updateOne(query, updateDocument, options);
}
module.exports = {

    createevent: async (req, res) => {
        
        const {dateandtime,title,performers,org_id,org_name,description,catagory,paidornot,tags,tickets,address,price,typeofevent,modeofevent,id,ticketid,tickettype,ticketprice,date,time,datetimeid,totoal_ticket} = req.body;
        //console.log(req.body);return false;
        let images = [];
        if (req?.files?.photo) {
            req.files.photo?.map((image) => {
                let filename = "media/" + image.filename;
                images.push(filename);
            })
        }
        let coordinates=[0,0]
        let dateandtime1;
        let address1;
        let newtickets;
        let newtags;
        try{
            coordinates=[address[0]?.latlong[1],address[0]?.latlong[0]]
        }catch(e){
            coordinates=[0,0]
        }
      
        // try{
        //     dateandtime1=JSON.parse(dateandtime)
        // }catch(e){
        //     dateandtime1=dateandtime
        // }
        // try{
        //     address1=JSON.parse(address)
        // }catch(e){
        //     address1=address
        // }
        // try{
        //     newtickets=JSON.parse(tickets)
        // }catch(e){
        //     newtickets=tickets
        // }
        const location_cor_obj={type:"Point",coordinates:coordinates}
        // newService.location_cor=location_cor_obj
        
        const photos=[];
        
        try{
            dateandtime1=JSON.parse(dateandtime);
            address1=JSON.parse(address);
            newtickets=JSON.parse(tickets);
            newtags=JSON.parse(tags);
        }catch{
            
            if(dateandtime)
            {
                dateandtime1=JSON.parse(dateandtime);
            }
            if(address)
            {
                address1=JSON.parse(address);
            }
            
            if(tickets)
            {
                newtickets=JSON.parse(tickets);
            }
            if(tags)
            {
                newtags=JSON.parse(tags);
            }
            
        }
        
        dateandtime1=dateandtime1.map((e)=>{
            console.log(e,e.date)
            let newdate=new Date(e.date)
            console.log(e,e.date,newdate)
            return {
                date:newdate,
                time:e.time
            }
        })
        //console.log(dateandtime1);return false;
        // console.log("req.body",req.body,"dateandtime1",dateandtime1,"address1",address1,"newtickets",newtickets,"newtags",newtags)
        // return null
        if (id) {
            const olddata=await Event.findById(id);
           
           
            if(ticketid){
                let ticketdatatoupdate={
                    ...(tickettype&&{type:tickettype}),
                    ...(ticketprice&&{price:ticketprice}),
                    "_id":mongoose.Types.ObjectId(ticketid)
                }
                await updatearrayelement(Event,olddata,"tickets",ticketid,id,ticketdatatoupdate)
            }
            //console.log(req.body);return false
            // if(datetimeid){
            //     let datetimetoupdate={
            //         ...(date&&{date:new Date(date)}),
            //         ...(time&&{time:time}),
            //         "_id":mongoose.Types.ObjectId(datetimeid)}
            //     await updatearrayelement(Event,olddata,"datetime",datetimeid,id,datetimetoupdate)
            //     // console.log("olddata,datetime,datetimeid,id,datetimetoupdate",olddata,"datetime",datetimeid,id,datetimetoupdate)
            // }
            //mongoose.set("debug",true);
            // if(dateandtime1)
            // {
            //     //console.log("here")
            //     await Event.findByIdAndUpdate(id,{
            //         "datetime":dateandtime1
            //     });
            // }
        //     let dateandtime = dateandtime1;
        //     console.log("dateandtime");
        //     console.log(dateandtime);
           //mongoose.set("debug",true);
            await Event.findByIdAndUpdate(id, {
                ...(performers && { performers: performers }),
                ...(title && { title: title }),
                ...(totoal_ticket && { totoal_ticket: totoal_ticket }),
                
                //...((dateandtime1.length&&!datetimeid) && { $addToSet:{datetime:dateandtime1} }),
                // ...(time && { time: time }),
                ...(org_id && { org_id: org_id }),
                ...(org_name && { org_name: org_name }),
                ...(paidornot?.toString() && { paidornot: paidornot }),
                //...(tags.length && { tags: tags }),
                ...(catagory && { catagory: catagory }),
                //...((newtickets.length&&!ticketid) && { $push:{tickets: newtickets} }),
                //...(address1.length&&{location_cor:location_cor_obj}),
                ...(address && { address: address1 }),
                ...(price && { price: price }),
                ...(typeofevent && { typeofevent: typeofevent }),
                ...(modeofevent && { modeofevent: modeofevent }),
                ...(images.length && { media: images }),
                ...(address1&&{address:address1}),
                ...(dateandtime1&&{datetime:dateandtime1}),
                ...(description&&{description:description}),
                
            }).then((result) => {
                return res.send({
                    status: true,
                    message: "L'événement a été mis à jour avec succès"
                })
            })

        } else {
            const event = new Event();
            event.performers = performers;
            event.title = title;
            event.org_id = org_id;
            event.org_name=org_name;
            event.datetime = dateandtime1;
            event.location_cor=location_cor_obj;
            event.paidornot = paidornot;
            event.catagory=catagory;
            event.tags=tags;
            event.tickets=newtickets;
            event.price = price;
            event.typeofevent = typeofevent;
            event.modeofevent = modeofevent;
            event.address = address1;
            event.media = images;
            event.description = description;
            event.totoal_ticket = totoal_ticket;
            event.save()
                .then((result) => {
                    return res.send({
                        status: true,
                        message: "L'événement a été enregistré avec succès"
                    })
                })
        }
    },
    deleteevent: async (req, res) => {
        const id = req.params.id;
        await Event.findByIdAndUpdated(id,{is_deleted:true})
            .then((result) => {
                return res.send({
                    status: true,
                    message: "Événement supprimé avec succès"
                })
            });
    },
    getallevents: async(req,res)=>{
        console.log("here");
        //db.events.find({ "datetime.date": { '$gte': new Date("Fri, 31 Mar 2023 00:00:00 GMT") } })
        var today_date = new Date();
        const events=await Event.aggregate([
                {
                    $match:{
                            is_deleted:false,
                            "datetime.date":{$gte:new Date()
                        }
                    }
                }
            ]);
            // {
                    
            //     datetime.date : {$gte:today_date}
            // },
            // datetime.date: {
            //     $gte:["$$date.date",today]
            // },


            // {$match:{user_id:id,date:{$gte:new Date()}}},

        return res.send({
            status:true,
            message:"fetched successfully",
            data:events
        })
    },
    // geteventbyid: async(req,res)=>{
    //     const id=req.params.id;
    //     const events=await Event.findById(id);
    //     return res.send({
    //         status:true,
    //         message:"fetched successfully",
    //         data:events
    //     })
    // },
    saveratingandreview: async (req, res) => {
        const user_id = req.body.user_id;
        const type = req.body.type;
        const event_id = req.body.event_id;
        const rating = req.body.rating;
        const review = req.body.review;
        const org_id=req.body.org_id;
        const photos=[];
     
        if(req.files.photo){
            let photoname
            req.files.photo?.map((photo)=>{
                photoname="products/"+photo.filename;
                photos.push(photoname)
            })
        }

        const event = await Event.findById(event_id);
        if(!event){
            return res.send({
                status:false,
                message:"there is no such event"
            })
        }
        if (event?.status==true) {
            
        
                const alreadyexists = await EventRating.findOne({event_id:event_id});
                
                if (alreadyexists) {
                    const datatoupdate={
                        ...(rating&&{rating:rating}),
                        ...(review&&{review:review}),
                        ...(photos.length&&{media:photos})
                        
                    }
                    await EventRating.findByIdAndUpdate(alreadyexists._id,datatoupdate)
                    .then((result)=>{
                        return res.send({
                            status: false,
                            message: "rating updated successfully",
                            data: null,
                            errmessage: ""
                        });
                    })
                   
                }
                else {
                    const newrating = new EventRating();
                    newrating.media=photos;
                    newrating.user_id = user_id;
                    newrating.media = photos;
                    newrating.org_id = org_id;
                    newrating.event_id = event_id;
                    newrating.rating = rating;
                    newrating.review = review;
                    newrating.save().then((rating) => {

                    return res.send({
                            status: true,
                            message: "La révision a été sauvegardée avec succès",
                            data: rating,
                            errmessage: "",
                        });

                    });
                }
            
        }else{
            return res.send({
                status:false,
                message:"event is not completed yet, wait for it to get completed"
            })
        }
    },
    getratingandreviewbyeventid: async (req, res) => {
        const event_id = req.params.event_id;
        const rating= await EventRating.aggregate([
            {$match:{ event_id: event_id }},
            {$addFields:{
                eventIDOBJ:{"$toObjectId":"$event_id"},
                userIDOBJ:{"$toObjectId":"$user_id"}
            }},
            {$lookup:{
                from:"events",
                localField:"eventIDOBJ",
                foreignField:"_id",
                as:"event"
            }},
            {$lookup:{
                from:"users",
                localField:"userIDOBJ",
                foreignField:"_id",
                as:"user"
            }}
        ]);
        return res.send({
            status: true,
            message: "La revue a été récupérée avec succès",
            data: rating,
            errmessage: "",
        });
    },
    getratingandreviewbyuserid: async (req, res) => {
        const user_id = req.params.user_id;
        const rating =await EventRating.aggregate([
            {$match:{ user_id: user_id }},
           
            {$addFields:{
                eventIDOBJ:{"$toObjectId":"$event_id"},
                userIDOBJ:{"$toObjectId":"$user_id"}
            }},
            {$lookup:{
                from:"events",
                localField:"eventIDOBJ",
                foreignField:"_id",
                as:"event"
            }},
            {$lookup:{
                from:"users",
                localField:"userIDOBJ",
                foreignField:"_id",
                as:"user"
            }}
        ]);
        return res.send({
            status: true,
           
            data: rating,
            errmessage: "",

        });
    },
    deleteratingandreview: async(req, res) => {
        const rating_id = req.params.review_id;
      await  EventRating.findByIdAndRemove(rating_id).then((rating) => {
            return res.send({
                status: true,
                message: "La révision a été supprimée avec succès",
                data: rating,
                errmessage: "",
            });
        }).catch((err) => {
            return res.send({
                status: false,
                message: "",
                data: null,
                errmessage: "Erreur dans la récupération des avis ",
            });
        })
    },
    makeEventasactive:async(req,res)=>{
        const id=req.params.id;
        await Event.makeActive(id).then((result)=>{
            return res.send({
                status:true,
                message:"updated"
            })
        })
    },
    makeEventasInactive:async(req,res)=>{
        const id=req.params.id;
        await Event.makeInActive(id).then((result)=>{
            return res.send({
                status:true,
                message:"updated"
            })
        })
    },
    createeditordeleteorganizer:async(req,res)=>{
        const {
            op,
            catagory,
            name,
            email,
            phone,
            address,
            bank,
            accountname,
            iban,
            bic
        }=req.body
        if(op=="c"){
           const olduser= await OrganizerModel.findOne({
                email:email
            })
            if(olduser){
                return res.send({
                    status:true,
                    message:"Eamil existe déjà",
                    data:null
                })  
            }
            await OrganizerModel.create({
                ...(catagory&&{catagory:catagory}),
                ...(name&&{name:name}),
                ...(email&&{email:email}),
                ...(phone&&{phone:phone}),
                ...(address&&{address:address}),
                ...(bank&&{bank:bank}),
                ...(accountname&&{accountname:accountname}),
                ...(iban&&{iban:iban}),
                ...(bic&&{bic:bic})
            }).then((result)=>{
                return res.send({
                    status:true,
                    message:"L'organisateur a été créé avec succès",
                    data:result
                })
            })
        }else if(op=="u"){
            await OrganizerModel.findOneAndUpdate({
                email:email
            },{
                ...(catagory&&{catagory:catagory}),
                ...(name&&{name:name}),
                ...(phone&&{phone:phone}),
                ...(address&&{address:address}),
                ...(bank&&{bank:bank}),
                ...(accountname&&{accountname:accountname}),
                ...(iban&&{iban:iban}),
                ...(bic&&{bic:bic}) 
            }).then((result)=>{
                return res.send({
                    status:true,
                    message:"Mise à jour réussie de l'Orginiser",
                    data:result
                })
            })
        }else if(op=="d"){
            let organizertodel=await OrganizerModel.findOne({
                email:email
            });
            // organizertodel?.email=organizertodel.email+"_"+Math.random()*100000000;
            organizertodel.email=organizertodel.email+"_"+(Math.random()*1000)+"_deleted";
            organizertodel.is_deleted=true
            organizertodel.save()
                            .then((result)=>{
                                return res.send({
                                    status:true,
                                    message:"Orginiser supprimé avec succès",
                                    data:null
                                })
                            })
        }
    },
    createeditordeletecatagory:async(req,res)=>{
        const {
            name,
            newname,
            op
        }=req.body
        if(op=="c"){
            const photos=[];
     
            if(req.files.image){
                let photoname
                req.files.image?.map((photo)=>{
                    photoname=photo.filename;
                    photos.push(photoname)
                })
            }

            await CatagoryModel.create({
                name:name,
                photos:photos
            }).then((result)=>{
                return res.send({
                    status:true,
                    message:"Catégorie ajoutée succès complet",
                    data:result
                })
            })
        }else if(op=="u"){
            const photos=[];
            var old_photos = req.body.old_photos;
            if(req.files.image)
            {
                let photoname
                req.files.image?.map((photo)=>{
                    photoname=photo.filename;
                    photos.push(photoname)
                })
                // if (fs.existsSync("/static/public/media/"+old_photos ))
                // {
                //     fs.unlink("/static/public/media/"+old_photos , (err) => 
                //     {
                //       console.log("unlink file error "+err);
                //     });
                // }

            }else{
                photos[0] = old_photos;
            }
            
            await CatagoryModel.findOneAndUpdate({
                name:name
            },{name:newname,photos:photos}).then((result)=>{
                return res.send({
                    status:true,
                    message:"Catégorie mise à jour succès complet",
                    data:result
                })
            })
        }else if(op=="d"){
            await CatagoryModel.findOneAndDelete({
                name:name
            }).then((result)=>{
                return res.send({
                    status:true,
                    message:"Catégorie supprimée succès complet",
                    data:null
                })
            })
        }
    },
    createeditbanner:async(req,res)=>{
        const {
            second_text,
            name,
            newname,
            op
        }=req.body
        if(op=="c"){
            const photos=[];
     
            if(req.files.image){
                let photoname
                req.files.image?.map((photo)=>{
                    photoname=photo.filename;
                    photos.push(photoname)
                })
            }

            await EventbannerModel.create({
                second_text:second_text,
                name:name,
                photos:photos
            }).then((result)=>{
                return res.send({
                    status:true,
                    message:"La bannière enregistre un succès complet",
                    data:result
                })
            })
        }else if(op=="u"){
            const photos=[];
            var old_photos = req.body.old_photos;
            if(req.files.image)
            {
                let photoname
                req.files.image?.map((photo)=>{
                    photoname=photo.filename;
                    photos.push(photoname)
                })
                // if (fs.existsSync("/static/public/media/"+old_photos ))
                // {
                //     fs.unlink("/static/public/media/"+old_photos , (err) => 
                //     {
                //       console.log("unlink file error "+err);
                //     });
                // }

            }else{
                photos[0] = old_photos;
            }
            
            await EventbannerModel.findOneAndUpdate({
                
                name:name
            },{name:newname,photos:photos,second_text:second_text}).then((result)=>{
                return res.send({
                    status:true,
                    message:"Enregistrement de la bannière mis à jour avec succès",
                    data:result
                })
            })
        }else if(op=="d"){
            await EventbannerModel.findOneAndDelete({
                name:name
            }).then((result)=>{
                return res.send({
                    status:true,
                    message:"Enregistrement de la bannière supprimé succès complet",
                    data:null
                })
            })
        }
    },

    showbanner:async(req,res)=>{
        const pageno=req.body.pageno;
        if(pageno == null || pageno == "null") 
        {
            console.log("hereeeeeee if 563");
            var len_ev = await EventbannerModel.aggregate([
                {$match:{_id:{$ne:null}}},
                {
                    $count: "total_count"
                }
            ]);

            //console.log("len_ev "+JSON.stringify(len_ev));
            let page_val = len_ev? len_ev[0].total_count : 0;
            const pages=Math.ceil(page_val/10)
            return res.send({
                status:true,
                data:pages
            })
            
                
        }else{
            //console.log("hereeeeeee else 580");
            let limit=10;
            let skip=limit*pageno
            await EventbannerModel.aggregate([
                {$match:{_id:{$ne:null}}},
                {$skip:skip},
                {$limit:limit},
                
            ]).then((result)=>{
                return res.send({
                    status:true,
                    data:result
                })
            })
        }
        
    },
    getallecatagory:async(req,res)=>{
        await CatagoryModel.aggregate([
            {$match:{_id:{$ne:null}}},
            {$project:{
                name:1
            }}
        ]).then((result)=>{
            return res.send({
                status:true,
                data:result
            })
        })
    },
    getallorganizer:async(req,res)=>{
        await OrganizerModel.aggregate([
            {$match:{is_deleted:false}},
            {$project:{
                name:1
            }}
        ]).then((result)=>{
            return res.send({
                status:true,
                data:result
            })
        })
    },
    showcatagories:async(req,res)=>{
        const pageno=req.body.pageno;
        if(pageno == null || pageno == "null") 
        {
            console.log("hereeeeeee if 563");
            var len_ev = await CatagoryModel.aggregate([
                {$match:{_id:{$ne:null}}},
                {
                    $count: "total_count"
                }
            ]);

            //console.log("len_ev "+JSON.stringify(len_ev));
            let page_val = len_ev? len_ev[0].total_count : 0;
            const pages=Math.ceil(page_val/10)
            return res.send({
                status:true,
                data:pages
            })
            
                
        }else{
            //console.log("hereeeeeee else 580");
            let limit=10;
            let skip=limit*pageno
            await CatagoryModel.aggregate([
                {$match:{_id:{$ne:null}}},
                {$skip:skip},
                {$limit:limit},
                
            ]).then((result)=>{
                return res.send({
                    status:true,
                    data:result
                })
            })
        }
        
    },
    showorganizer:async(req,res)=>{
        const pageno=req.body.pageno;
        let limit=10;
        let skip=limit*pageno
        await OrganizerModel.aggregate([
            {$match:{is_deleted:false}},
            {$match:{_id:{$ne:null}}},
            {$skip:skip},
            {$limit:limit},
           
        ]).then((result)=>{
            return res.send({
                status:true,
                data:result
            })
        })
    },
    showorganizerCount:async(req,res)=>{
         
        await OrganizerModel.count(
            {is_deleted:false},
           
        ).then((result)=>{
            console.log("result "+result);
            let perPageRecord = 10;
            let total = result;
            let totalPageNumber = total / perPageRecord;

            totalPageNumber = Math.ceil(totalPageNumber);

            return res.send({
                status:true,
                data:totalPageNumber
            })
        })
    },
    getOrgPayHistoryCount:async(req,res)=>{
         try{
            var id = req.params.id;
            await EPaymentRequestmodel.count(
                {org_id:id},
            
            ).then((result)=>{
                console.log("result "+result);
                let perPageRecord = 10;
                let total = result;
                let totalPageNumber = total / perPageRecord;

                totalPageNumber = Math.ceil(totalPageNumber);

                return res.send({
                    status:true,
                    data:totalPageNumber
                })
            })
         }catch(error)
         {
            return res.send({
                status:false,
                data:0
            })
         }
        
    },
    
    getOrgPayHistory:async(req,res)=>{
        try{
           var id = req.body.id;
           
           const pageno=req.body.pageno;
            let limit=10;
            let skip=limit*pageno
           await EPaymentRequestmodel.aggregate([
               {$match:{org_id:id}},
               {$skip:skip},
               {$limit:limit},
           ]).then((result)=>{
               //console.log("result "+result);

               return res.send({
                   status:true,
                   data:result
               })
           })
        }catch(error)
        {
           return res.send({
               status:false,
               data:error.message
           })
        }
       
   },
    showevent:async(req,res)=>{
        try{
           // mongoose.set("debug",true);
            const pageno=req.body.pageno;
            const status=req.body.status;
            const query={_id:{$ne:null}}
            
            const today=new Date()
            const aquery={}
            const arrayQuery=[]
            arrayQuery.push( {$match:query})
            if(status?.toString()){
                if(status==0){
                    aquery['$addFields']={
                        datetimefilter:{
                        $filter:{
                        input:"$datetime",
                        as:"date",
                        cond: {
                            $lte:["$$date.date",today]
                        },
                        as: "date"
                    }}}
                }else if(status==1){
                    aquery['$addFields']={
                        datetimefilter:{
                            $filter:{
                        input:"$datetime",
                        as:"date",
                        cond: {
                            $gte:["$$date.date",today]
                        },
                        as: "date"
                    }}}
                }
                //arrayQuery.push(aquery)
            }
        
            let limit=10;
            let skip=limit*pageno
            arrayQuery.push({$skip:skip})
            arrayQuery.push({$limit:limit})
            //console.log("query",query,"aquery",aquery,"arrayQuery",arrayQuery,"req.body",req.body)
            await Event.aggregate(arrayQuery).then((result)=>{
                //const newevents=result?.filter((e)=>e.datetimefilter.length>0)
                return res.send({
                    status:true,
                    data:result
                })
            })
        }catch(error)
        {
            return res.send({
                status:false,
                message:error.message,
                data:[]
            })
        }
    },
    showeventCount:async(req,res)=>{
        try{
            //mongoose.set("debug",true);
            const pageno=req.body.pageno;
            const status=req.body.status;
            const query={_id:{$ne:null}}
            
            const today=new Date()
            const aquery={}
            const arrayQuery=[]
            arrayQuery.push( {$match:query})
            if(status?.toString()){
                if(status==0){
                    aquery['$addFields']={
                        datetimefilter:{
                        $filter:{
                        input:"$datetime",
                        as:"date",
                        cond: {
                            $lte:["$$date.date",today]
                        },
                        as: "date"
                    }}}
                }else if(status==1){
                    aquery['$addFields']={
                        datetimefilter:{
                            $filter:{
                        input:"$datetime",
                        as:"date",
                        cond: {
                            $gte:["$$date.date",today]
                        },
                        as: "date"
                    }}}
                }
                //arrayQuery.push(aquery)
            }
        
            // let limit=10;
            // let skip=limit*pageno
            // arrayQuery.push({$skip:skip})
            // arrayQuery.push({$limit:limit})
            arrayQuery.push({$project:{"title":1} })
            //console.log("query",query,"aquery",aquery,"arrayQuery",arrayQuery,"req.body",req.body)
            //console.log(arrayQuery);return false;
            await Event.aggregate(arrayQuery).then((result)=>{
                //const newevents=result?.filter((e)=>e.datetimefilter.length>0)
                let perPageRecord = 10;
                let total = result.length;
                let totalPageNumber = total / perPageRecord;
                //console.log("totalPageNumber"+totalPageNumber);
                totalPageNumber = Math.ceil(totalPageNumber);
                //console.log("totalPageNumber"+totalPageNumber);
                //console.log("totalcount new fun"+totalcount);
                //console.log("all record count "+totalcount);


                return res.send({
                    status:true,
                    data:totalPageNumber
                })
            })
        }catch(error)
        {
            return res.send({
                status:false,
                message:error.message,
                data:[]
            })
        }
    },


    showeventOngoing:async(req,res)=>{
        try{
            //mongoose.set("debug",true);
            let pageno=req.body.pageno;
            
            
            const today=new Date();
            
            pageno = pageno ? pageno :0;
            console.log("pageno ",pageno);
            let limit=10;
            let skip=limit*pageno
           
            
            await Event.aggregate([
                {
                    $match:{
                        "_id":{$ne:null}
                    }
                },
                {
                  $addFields:{
                    startDate:{ $arrayElemAt: [ "$datetime.date", 0 ] }
                  }
                },
                {
                  $addFields:{
                    endDate:{ $arrayElemAt: [ "$datetime.date", -1 ] }
                  }
                },
                {
                    $match:{
                        "startDate":{$lte:today}
                    }
                },
                {
                    $match:{
                        "endDate":{$gte:today}
                    }
                },
                {
                    $skip:skip
                },
                {
                    $limit:limit
                }
            ]).then((result)=>{
                return res.send({
                    status:true,
                    data:result
                })
            })
        }catch(error)
        {
            return res.send({
                status:false,
                message:error.message,
                data:[]
            })
        }
    },
    showeventOngoingCount:async(req,res)=>{
        try{
            //mongoose.set("debug",true);
            const today=new Date();
            // let pageno=req.body.pageno;
            // 
            // pageno = pageno ? pageno :0;
            // console.log("pageno ",pageno);
            // let limit=10;
            // let skip=limit*pageno;

            await Event.aggregate([
                {
                    $match:{
                        "_id":{$ne:null}
                    }
                },
                {
                  $addFields:{
                    startDate:{ $arrayElemAt: [ "$datetime.date", 0 ] }
                  }
                },
                {
                  $addFields:{
                    endDate:{ $arrayElemAt: [ "$datetime.date", -1 ] }
                  }
                },
                {
                    $match:{
                        "startDate":{$lte:today}
                    }
                },
                {
                    $match:{
                        "endDate":{$gte:today}
                    }
                },
                {
                    $count: "totalRecord"
                }
            ]).then((result)=>{

                let perPageRecord = 10;
                let total = result[0].totalRecord;
                let totalPageNumber = total / perPageRecord;

                totalPageNumber = Math.ceil(totalPageNumber);
                return res.send({
                    status:true,
                    data:totalPageNumber
                })
            })
        }catch(error)
        {
            return res.send({
                status:false,
                message:error.message,
                data:[]
            })
        }
    },

    showeventExpired:async(req,res)=>{
        try{
            //mongoose.set("debug",true);
            let pageno=req.body.pageno;
            const today=new Date();
            pageno = pageno ? pageno :0;
            console.log("pageno ",pageno);
            let limit=10;
            let skip=limit*pageno
           
            await Event.aggregate([
                {
                    $match:{
                        "_id":{$ne:null}
                    }
                },
                {
                  $addFields:{
                    endDate:{ $arrayElemAt: [ "$datetime.date", -1 ] }
                  }
                },
                {
                    $match:{
                        "endDate":{$lt:today}
                    }
                },
                {
                    $skip:skip
                },
                {
                    $limit:limit
                }
            ]).then((result)=>{
                return res.send({
                    status:true,
                    data:result
                })
            })
        }catch(error)
        {
            return res.send({
                status:false,
                message:error.message,
                data:[]
            })
        }
    },
    showeventExpiredCount:async(req,res)=>{
        try{
            //mongoose.set("debug",true);
            const today=new Date();
            // let pageno=req.body.pageno;
            // 
            // pageno = pageno ? pageno :0;
            // console.log("pageno ",pageno);
            // let limit=10;
            // let skip=limit*pageno;

            await Event.aggregate([
                {
                    $match:{
                        "_id":{$ne:null}
                    }
                },
                {
                    $addFields:{
                      endDate:{ $arrayElemAt: [ "$datetime.date", -1 ] }
                    }
                },
                {
                    $match:{
                          "endDate":{$lt:today}
                    }
                },
                {
                    $count: "totalRecord"
                }
            ]).then((result)=>{
                let perPageRecord = 10;
                let total = result[0].totalRecord;
                let totalPageNumber = total / perPageRecord;

                totalPageNumber = Math.ceil(totalPageNumber);
                return res.send({
                    status:true,
                    data:totalPageNumber
                })
            })
        }catch(error)
        {
            return res.send({
                status:false,
                message:error.message,
                data:[]
            })
        }
    },


    getcatagorybyid:async(req,res)=>{
        const id=req.params.id;
        
        await CatagoryModel.aggregate([
            {$match:{_id:mongoose.Types.ObjectId(id)}},
            
            
        ]).then((result)=>{
            return res.send({
                status:true,
                data:result
            })
        })
    },
    getbannerbyid:async(req,res)=>{
        const id=req.params.id;
        
        await EventbannerModel.aggregate([
            {$match:{_id:mongoose.Types.ObjectId(id)}},
            
            
        ]).then((result)=>{
            return res.send({
                status:true,
                data:result
            })
        })
    },
    

    getRandombannerbyid:async(req,res)=>{
        const id=req.params.id;
        //db.eventbanners.aggregate([]).pretty()
        await EventbannerModel.aggregate([
            {
                $sample:{size:1}
            }            
        ]).then((result)=>{
            return res.send({
                status:true,
                image_path:"static/public/media/",
                data:result
            })
        })
    },
    getorganizerbyid:async(req,res)=>{
        const id=req.params.id;
        await OrganizerModel.aggregate([
            {$match:{_id:mongoose.Types.ObjectId(id)}},
            
           
        ]).then((result)=>{
            return res.send({
                status:true,
                data:result
            })
        })
    },
    getSingleEvent:async(req,res)=>{
        try{
            await Event.findOne({_id:req.params.id}).then((result)=>{
                return res.send({
                    status:true,
                    message:"Success",
                    data:result
                })
            }).catch((error)=>{
                return res.send({
                    status:false,
                    message:error.message
                })
            });
        }catch(error)
        {
            return res.send({
                status:false,
                message:error.message
            })
        }
    },
    bookaevent:async(req,res)=>{
        const {
            event_id,
            event_name,
            org_id,
            org_name,
            user_id,
            user_name,
            address,
            tickettype,
            ticketprice,
            nooftickets,
            edate,
            etime,
            type,
            payment_type
        }=req.body;
        const tickets=[]
        for(let i=0;i<nooftickets;i++){
            let newticket=stringGen(10);
            tickets.push(newticket)
        }
        const allticketsprice=ticketprice*nooftickets;
        const newService = new bookingmodel();
        newService.user_id=user_id;
        newService.user_name=user_name;
        newService.event_id=event_id;
        newService.event_name=event_name;
        newService.tickettype=tickettype;
        newService.ticketprice=allticketsprice
        newService.address=address;
        newService.tickets=tickets;
        newService.org_id=org_id;
        newService.org_name=org_name;
        newService.edate=new Date(edate);
        newService.etime=etime;
        newService.type=type;
        newService.payment_type=payment_type;
       
        newService.save(async(err, service) => {
            if (err) {
                res.json({
                    status: false,
                    errmessage: err.message,
                    data: null,
                    message: ""
                });
            } else {
                if(ticketprice==0){
                    await bookingmodel.findByIdAndUpdate(service._id,{payment_status:true,payment_amount:0,transaction_id:"noneeded"}).then((reulst)=>{
                        return res.json({
                            status: true,
                            message: 'Rendez-vous pris avec succès',
                            errmessage:"",
                            data: reulst,
            
                        })
                    })
                }else{
                return res.json({
                    status: true,
                    message: 'Rendez-vous pris avec succès',
                    errmessage:"",
                    data: service,
                    url:BASE_URL + "/api/common/getpaymentevent/"+service._id+"/"+allticketsprice+"/"+user_id
                });
                }
            }
        }
        ); 

    },
    markEventascompleted:async(req,res)=>{
        const id = req.params.id;
        const order=await bookingmodel.findById(id);
        if(order.status==0){
            order.status=1
            order.save((result)=>{
                return res.send({
                    status:true,
                    message:"marked as unprocessed"
                })
            })
        }else{
            return res.send({
                status:false,
                message:"La commande est en cours de traitement et ne peut donc pas être annuléemarked as unprocessed"
            })
        }
    },
    markEventascancelled:async(req,res)=>{
        const id = req.params.id;
        const order=await bookingmodel.findById(id);
        if(order.status==1){
            order.status=2
            order.save((result)=>{
                return res.send({
                    status:true,
                    message:"marked as unprocessed"
                })
            })
        }else{
            return res.send({
                status:false,
                message:"La commande est en cours de traitement et ne peut donc pas être annuléemarked as unprocessed"
            })
        }
    },
    getongoingevents:async(req,res)=>{
        const id=req.body.id;
      
        await bookingmodel.aggregate([
            {$match:{user_id:id,date:{$gte:new Date()}}},
            
        ]).then((result)=>{
            return res.send({
                status:true,
                data:result
            })
        })
    },
    getcompletedevent:async(req,res)=>{
         const id=req.body.id;
        await bookingmodel.aggregate([
            {$match:{user_id:id,date:{$lte:new Date()}}},
        ]).then((result)=>{
            return res.send({
                status:true,
                data:result
            })
        })
    },
    validateticket:async(req,res)=>{
        const id=req.body.id;
        const ticket=req.body.ticket;
       const booking=await bookingmodel.findOne({_id:id,ticket:ticket});
       if(booking){
        return res.send({
            status:true,
            data:booking
        })
       }else{
        return res.send({
            status:false,
            data:null
        })
       }
    },
    getallevents:async(req,res)=>{
        //mongoose.set("debug",true);
    const {
        latlong,
        radius,
        catagory,
        start_date,
        end_date,
        paidornot,
        pageno,
        user_id
    }=req.body;
    let distancquery
    // if(latlong){
    //     distancquery= {
     
    //         $geoNear:{
    //           "near":{type: "Point", coordinates:[latlong?.long,latlong?.lat]},
              
    //           "distanceField": "dist.calculated",
    //           includeLocs: "dist.location",
    //           spherical: true,
    //           "maxDistance":radius,
             
    //         }
          
    //     }
    // }
   
    console.log("before if lat long "+latlong);
    const newarrayforquery=[];
    let dquery={};
    if(latlong?.lat && latlong?.long && radius){
        console.log("if lat long "+latlong);
        let newradius=radius*1000;
        dquery ={$geoNear:{
            "near":{type: "Point", coordinates:[latlong?.long,latlong?.lat]},
            "distanceMultiplier": 0.001,
            "distanceField": "dist.calculated",
            includeLocs: "dist.location",
            spherical: true,
            "maxDistance":newradius,
           
          }}
          newarrayforquery.push(dquery)
    }
    let query={$match:{is_deleted:false}}
    
    if(catagory){
        query["$match"]["catagory"]=catagory
    }
    if(paidornot?.toString()=="true"){
        query["$match"]["paidornot"]=true
    }else if(paidornot?.toString()=="false"){
        query["$match"]["paidornot"]=false
    }
    newarrayforquery.push(query)
    let fquery={}
    if(start_date&&!end_date){
        fquery={$filter:{
            input:"$datetime",
            as:"date",
            cond: {
                $gte:["$$date.date",new Date(start_date)]
            }
         }}
    }else if(!start_date&&end_date){
        fquery={$filter:{
            input:"$datetime",
            as:"date",
            cond: {
                $and:[
                    { $lte:["$$date.date",new Date(end_date)]},
                    { $gte:["$$date.date",new Date()]}
                   ]
            }
            
         }}
    }else if(start_date&&end_date){
        fquery={$filter:{
            input:"$datetime",
            as:"date",
            cond: {
                $and:[
                    { $lte:["$$date.date",new Date(end_date)]},
                    { $gte:["$$date.date",new Date(start_date)]}
                   ]
            }
            
         }}
    }else{
        fquery={$filter:{
            input:"$datetime",
            as:"date",
            cond: {
                $and:[
                    { $gte:["$$date.date",new Date()]}
                   ]
            }
            
         }}
    }
    
    // else{
    //     fquery={$filter:{
    //         input:"$datetime",
    //         as:"date",
    //         cond: {
    //             $and:[
    //                 { $gte:["$$date.date",new Date()]}
    //                ]
    //         }
            
    //      }}
    // }
   
    
    const fquerylength=Object.keys(fquery)?.length;
    let addfields;
    if(fquerylength){
         addfields={
            $addFields:{
                datetimefilter:fquery,
                eventidstr:{"$toString":"$_id"}

            }
        }
    }else{
         addfields={
            $addFields:{
                datetimefilter:"$datetime",
                eventidstr:{"$toString":"$_id"}
            }
        }
    }
    newarrayforquery.push(addfields)
    let favlookup= {

        $lookup:{
        from:"favriouts",
        let:{eventidstr:"$eventidstr"},
        pipeline:[
            {$match:{$expr:{
                $and:[
                    {$eq:["$event_id","$$eventidstr"]},
                    {$eq:["$user_id",user_id]}
                ]
            }}}
        ],
        as:"favriouts"
      }};
      newarrayforquery.push(favlookup)
    // [
    //     // latlong?.lng&&dquery,
    //     {$match:query},
    //     fquerylength&&addfields
    // ]
    // console.log("query",query,"fquery",fquery,"dquery",dquery,"newarrayforquery",newarrayforquery)
    let limit=10;
    let skip=limit*pageno
     newarrayforquery.push({$skip:skip})
    newarrayforquery.push({$limit:limit})
    const events=await Event.aggregate(newarrayforquery);
    const newevents=events?.filter((e)=>e.datetimefilter.length>0)
    // const newevents=events
    return res.send({
        status:true,
        data:newevents
        // newarrayforquery:newarrayforquery
    })
    },


    getallevents_without_pagination:async(req,res)=>{
        //mongoose.set("debug",true);
    const {
        latlong,
        radius,
        catagory,
        start_date,
        end_date,
        paidornot,
        pageno,
        user_id
    }=req.body;
    let distancquery

    console.log("before if lat long "+latlong);
    const newarrayforquery=[];
    let dquery={};
    if(latlong?.lat && latlong?.long && radius){
        console.log("if lat long "+latlong);
        let newradius=radius*1000;
        dquery ={$geoNear:{
            "near":{type: "Point", coordinates:[latlong?.long,latlong?.lat]},
            "distanceMultiplier": 0.001,
            "distanceField": "dist.calculated",
            includeLocs: "dist.location",
            spherical: true,
            "maxDistance":newradius,
           
          }}
          newarrayforquery.push(dquery)
    }
    let query={$match:{is_deleted:false}}
    
    if(catagory){
        query["$match"]["catagory"]=catagory
    }
    if(paidornot?.toString()=="true"){
        query["$match"]["paidornot"]=true
    }else if(paidornot?.toString()=="false"){
        query["$match"]["paidornot"]=false
    }
    newarrayforquery.push(query)
    let fquery={}
    if(start_date&&!end_date){
        fquery={$filter:{
            input:"$datetime",
            as:"date",
            cond: {
                $gte:["$$date.date",new Date(start_date)]
            }
         }}
    }else if(!start_date&&end_date){
        fquery={$filter:{
            input:"$datetime",
            as:"date",
            cond: {
                $and:[
                    { $lte:["$$date.date",new Date(end_date)]},
                    { $gte:["$$date.date",new Date()]}
                   ]
            }
            
         }}
    }else if(start_date&&end_date){
        fquery={$filter:{
            input:"$datetime",
            as:"date",
            cond: {
                $and:[
                    { $lte:["$$date.date",new Date(end_date)]},
                    { $gte:["$$date.date",new Date(start_date)]}
                   ]
            }
            
         }}
    }else{
        fquery={$filter:{
            input:"$datetime",
            as:"date",
            cond: {
                $and:[
                    { $gte:["$$date.date",new Date()]}
                   ]
            }
            
         }}
    }
    const fquerylength=Object.keys(fquery)?.length;
    let addfields;
    if(fquerylength){
         addfields={
            $addFields:{
                datetimefilter:fquery,
                eventidstr:{"$toString":"$_id"}

            }
        }
    }else{
         addfields={
            $addFields:{
                datetimefilter:"$datetime",
                eventidstr:{"$toString":"$_id"}
            }
        }
    }
    newarrayforquery.push(addfields)
    let favlookup= {

        $lookup:{
        from:"favriouts",
        let:{eventidstr:"$eventidstr"},
        pipeline:[
            {$match:{$expr:{
                $and:[
                    {$eq:["$event_id","$$eventidstr"]},
                    {$eq:["$user_id",user_id]}
                ]
            }}}
        ],
        as:"favriouts"
      }};
      newarrayforquery.push(favlookup)
       let aa = {
            $addFields:{
                "isFav":{$size:"$favriouts"}
            }
       };
       newarrayforquery.push(aa)
        
        const events=await Event.aggregate(newarrayforquery);
        const newevents=events?.filter((e)=>e.datetimefilter.length>0)
        // const newevents=events
        return res.send({
            status:true,
            data:newevents
            // newarrayforquery:newarrayforquery
        })
    },


    geteventbyid:async(req,res)=>{
        const id=req.body.event_id;
        const user_id=req.body.user_id;
        console.log("req.body",req.body)
        await Event.aggregate([
            {$match:{_id:mongoose.Types.ObjectId(id)}},
            {

                $lookup:{
                from:"favriouts",
                let:{eventidstr:{"$toString":"$_id"}},
                pipeline:[
                    {$match:{$expr:{
                        $and:[
                            {$eq:["$event_id","$$eventidstr"]},
                            {$eq:["$user_id",user_id]}
                        ]
                    }}}
                ],
                as:"favriouts"
              }
            },
            {
                $addFields:{
                    "isFav":{$size:"$favriouts"}
                }
            }
          
        ]).then((result)=>{
            const newevents=result;
            return res.send({
                status:true,
                data:newevents
            })
        })
    },

    getallpendingandpaidamount:async(req,res)=>{
        // const id=req.params.id
        const today=new Date()
        const allvalidorgnizers=await OrganizerModel.aggregate([
            {$match:{is_deleted:false}},
            // {$lookup:{
            //     from:"eventbookings",
            //     let:{org_idstr:{"$toString":"$_id"}},
            //     pipeline:[
            //         {$match:{$expr:{$and:[
            //             {$eq:["$org_id","$$org_idstr"]},
            //             {$eq:["$paid",false]}
            //         ]}}}
            //     ],
            //     as:"bookings",

            // }},
            // {$addFields:{
            //     bookingsamount:{
            //         $map:
            //         {
            //           input: "$bookings",
            //           as: "booking",
            //           in: "$$booking.payment_amount"
            //     }
            // }}},
            // {$addFields:{
            //     pendingamount:{
            //         $reduce:{
            //             input:"$bookingsamount",
            //             initialValue:0,
            //             in:{
            //                 $add:["$$value","$$this"]
            //             }
            //         }
            //     }
            // }}
            // {$project:{
            //     "totalpaidamount": 1,
            //     "totalpendingamount": 1
            // }}
        ]);
        
        // const allpaidbooking=await bookingmodel.aggregate([{$match:{
        //     paid:true,payment_status:true
        // }}]);
        // const allpendingbooking=await bookingmodel.aggregate([{$match:{
        //     paid:false,payment_status:true
        // }}]);
        // const totalpaidamount=allpaidbooking?.reduce((accm,e)=>accm+parseInt(e.payment_amount),0);
        // const totalpendingamount=allpendingbooking?.reduce((accm,e)=>accm+parseInt(e.payment_amount),0);
        // const totalpendingids=allpendingbooking?.map((e)=>e._id+'')
        //const allvalidorgnizers1=allvalidorgnizers?.filter((e)=>e.hasOwnProperty("totalpendingamount")==true)
        return res.send({
            status:true,
            data:{
                allvalidorgnizers:allvalidorgnizers
                // totalpaidamount:totalpaidamount,
                // totalpendingamount:totalpendingamount,
                // totalpendingids
            }
        })
    },
    makepaymentrequest:async(req,res)=>{
        // const ids=req.body.ids
        // const objectids=ids?.map((e)=>mongoose.Types.ObjectId(e))
        const id=req.body.id;
        const amount=req.body.amount;
        const org=await OrganizerModel.findById(id);
        if(org?.totalpendingamount>amount)
        {
            let abc =org.totalpaidamount - amount;
            
            await OrganizerModel.updateOne({_id:id},{totalpendingamount:abc});

            const newPayoutrequest=new EPaymentRequestmodel();
            newPayoutrequest.org_id=org?.id;
            newPayoutrequest.org_name=org?.name;
            newPayoutrequest.email=org?.email;
            newPayoutrequest.request_amount=amount;
            newPayoutrequest.bank=org?.bank;
            newPayoutrequest.accountname=org?.accountname;
            newPayoutrequest.iban=org?.iban;
            newPayoutrequest.bic=org?.bic;
            newPayoutrequest.payment_status=true;
            newPayoutrequest.save().then((result)=>{
                org.totalpendingamount=org.totalpendingamount-amount;
                org.totalpaidamount=org.totalpaidamount+amount;
                org.save().then((result)=>{
                    return res.send({
                        status:true,
                        message:"Demande de paiement effectuée avec succès"
                    })
                })
            })
           

        }else{
            return res.send({
                status:false,
                message:"Le montant en suspens est supérieur au montant total en suspens"
            })
        }
        // await OrganizerModel.updateOne({_id:mongoose.Types.ObjectId(id)},{paid:true}).then((result)=>{
        //     return res.send({
        //         status:true
        //     })
        // })
    },
    markpaymentrequestaspaid:async(req,res)=>{
        // const ids=req.body.ids
        // const objectids=ids?.map((e)=>mongoose.Types.ObjectId(e))
        const id=req.params.id;
       
     await EPaymentRequestmodel.findByIdAndUpdate(id,{payment_status:true}).then((result)=>{
        return res.send({
            status:true,
            message:"payment marked as paid"
        })
     })
            
     
    },
    getallpaymenttransactionbyorgid:async(req,res)=>{
        // const ids=req.body.ids
        // const objectids=ids?.map((e)=>mongoose.Types.ObjectId(e))
        const id=req.params.id;
       
     await EPaymentRequestmodel.find({org_id:id}).then((result)=>{
        return res.send({
            status:true,
            data:result
        })
     })
    },
    downloadtickets:async(req,res)=>{
        //console.log(req.params)
        try{
        const id=req.params.id;
        // const transaction_id=req.params.transaction_id;
        let invoice = await bookingmodel.findOne({_id:id});
        
        //console.log("invoice "+invoice);return false;
        let tickets=invoice?.tickets;
        //console.log("tickets "+tickets);
        let randome=Math.random();
        let ticketfiles=[]
        if(invoice)
        {   


            const today_1 = new Date(invoice.date_of_transaction);
            const yyyy_1 = today_1.getFullYear();
            let mm_1 = today_1.getMonth() + 1; // Months start at 0!
            let dd_1 = today_1.getDate();
            if (dd_1 < 10) dd_1 = '0' + dd_1;
            if (mm_1 < 10) mm_1 = '0' + mm_1;
            let formattedToday_1 = dd_1 + '.' + mm_1 + '.' + yyyy_1;
            let hourss_1 = today_1.getHours();
            let minutess_1 = today_1.getMinutes();
            // console.log("hourss ",hourss);
            // console.log("minutess ",minutess);
            formattedToday_1 = formattedToday_1 +" "+hourss_1+"h"+minutess_1;
            invoice["payment_date"] = formattedToday_1;




            const today = new Date(invoice.sdate);
            const yyyy = today.getFullYear();
            let mm = today.getMonth() + 1; // Months start at 0!
            let dd = today.getDate();
            if (dd < 10) dd = '0' + dd;
            if (mm < 10) mm = '0' + mm;
            let start_date = dd + '.' + mm + '.' + yyyy;
            invoice["start_date"] = start_date;

            const today1 = new Date(invoice.edate);
            const yyyy1 = today1.getFullYear();
            let mm1 = today.getMonth() + 1; // Months start at 0!
            let dd1 = today.getDate();
            if (dd1 < 10) dd1 = '0' + dd1;
            if (mm1 < 10) mm1 = '0' + mm1;
            let start_date1 = dd1 + '.' + mm1 + '.' + yyyy1;
            invoice["end_date"] = start_date1;

            let event_record = await Event.findOne({_id:invoice.event_id});
            for(let i=0;i<tickets.length;i++)
            {
                let ticket=tickets[i]
                const filepath=path.join(__dirname, "../../views/admin-panel/ticket.ejs");
                //console.log(filepath)
               
                
                const options = { format: 'Letter' };
                let filename='Billet_'+invoice.order_id+'.pdf';
                ticketfiles.push(filename);

                //console.log("ticket ", ticket);

                let QRCodeoutput = await QRCode.toDataURL(ticket)
                //console.log("QRCodeoutput ---->>>> "+QRCodeoutput);
                const html = await ejs.renderFile(filepath,{ QRCodeoutput:QRCodeoutput,invoice:invoice,event_record:event_record,ticket:ticket });
                //console.log("html ---->>>> "+html);
                //return false;
                pdf.create(html,{
                    childProcessOptions: {
                      env: {
                        OPENSSL_CONF: '/dev/null',
                      },
                    } }).toFile(path.join(__dirname, "../../views/admin-panel/public/invoices/")+filename, function(err, result) {
                    if (err){
                        return console.log("in pdf create",err);
                    }
                    // console.log(BASE_URL+"/static/public/invoices/invoice"+id+".pdf")
                    // console.log(path.join(__dirname, "../../views/admin-panel/public/invoices/invoice"+id+".pdf"));
                    // return res.send({
                    // status:true,
                    // url:BASE_URL+"/static/public/invoices/invoice"+id+'_'+randome+".pdf",
                    // message:"Facture téléchargée avec succès"
                    // })
                });
            }
            return res.send({
                status:true,
                ticketfiles:ticketfiles,
                message:"Facture téléchargée avec succès"
            })
        }else{
            res.send({
            status:false,
            message:"Facture non trouvée"
            })
        }
        
        }catch(err){
        console.log("in catch block",err)
        res.send(err.message);
        }
    },
    ///cart function

    addEventtocart:async(req,res)=>{
        const userid=req.body.userid;
        const eventid=req.body.eventid;
        const price=req.body.price;
       
        const  ticketype=req.body.ticketype;
       
        const quantity=req.body.quantity;
        const event= await Event.findById(eventid);
        //console.log("event ", event );return false;
        if(event.totoal_ticket < quantity)
        {
            return res.send({
                status:false,
                message:"Les billets d'événements ne sont pas disponibles pour l'instant"
            })
        }
        const  edate=event.datetime[1]?.date;
        const  etime=event.datetime[1]?.time;
        const  sdate=event.datetime[0]?.date;
        const  stime=event.datetime[0]?.time;
        const cartdata={
            event:event,
            quantity:quantity,
            price:price,
            edate:edate,
            sdate:sdate,
            stime:stime,
            etime:etime,
            ticketype:ticketype
        }
       // console.log("cartdata",cartdata)
        const cart=(await User.findById(userid))?.eventcart;
        let isproductthere=cart?.filter(e=>{
            let upid=e.event[0]?._id+"";
           
            
          
            if(upid==eventid){
                return true
            }else{
                return false
            }
        })[0];
       
        let exceptproduct=cart?.filter(e=>e._id+""!=isproductthere?._id+"");
        if(isproductthere){
            cartdata["quantity"]=isproductthere.quantity+quantity;
            // exceptproduct.concat(cartdata);
            exceptproduct.push(cartdata);
            await User.findByIdAndUpdate(userid,{
                "eventcart":exceptproduct
            }).then((result)=>{
                return res.send({
                    status:true,
                    message:"L'événement a été ajouté avec succès"
                })
            })
        }else{
            await User.findByIdAndUpdate(userid,{
                "$push":{"eventcart":cartdata}
            }).then((result)=>{
                return res.send({
                    status:true,
                    message:"L'événement a été ajouté avec succès"
                })
            })
        }
       
        
    },
    deleteEventfromcart:async(req,res)=>{
        const userid=req.body.userid;
        const eventid=req.body.eventid;
        // const productidOBJ=mongoose.Types.ObjectId(productid);
        const user=await User.findById(userid);
        const cartdata=user?.eventcart?.filter((e)=>{
            //console.log("eeeeeeeeeeee ---->>>>> "+e);
            const stringid=e.event[0]?._id+"";
            //console.log("stringid -->>> "+ stringid);
            //console.log("eventid ---->>>> "+ eventid);
            // console.log("stringid",stringid,"productid",productid,productid==stringid)
            return stringid!=eventid
        });

        
        //console.log("cartdata",cartdata);
        user.eventcart=cartdata;
        user.save().then((result)=>{
            return res.send({
                status:true,
                message:"Produit retiré avec succès"
            })
        })

        
    },

    //     const userid=req.body.userid;
    //     // const productwithquantity=[
    //     //     {
    //     //         productid:"",
    //     //         quantity:""
    //     //     }
    //     // ]
    //     const productwithquantity=req.body.productwithquantity;
    //     let cartdata=[];
    //    for(let k=0;k<productwithquantity.length;k++){
    //         let e=productwithquantity[k];
    //         let product=await Product.findById(e.productid);
    //         let quantity=e.quantity;
    //         let price=e.price;
    //         let variation_id=e.variation_id;
    //         cartdata.push({
    //             product:product,
    //             quantity:quantity,
    //             price:price,
    //             variation_id:variation_id
    //         })
           
    //     };
       
    //     await User.findByIdAndUpdate(userid,{
    //         "cart":cartdata
    //     }).then((result)=>{
    //         return res.send({
    //             status:true,
    //             data:result,
    //             message:"product added successfully"
    //         })
    //     })
    // },
    clearcart: async(req,res)=>{
        const userid=req.body.userid;
        const user=await User.findById(userid);
        user.eventcart=[]
        user.save().then((result)=>{
            return res.send({
                status:true,
                message:"cart cleared successfully"
            })
        })
    },
    getcartbyuserid: async(req,res)=>{
        const userid=req.body.userid;
        const cart=(await User.findById(userid))?.eventcart;
        //console.log("cart ",JSON.stringify(cart));
         let gg = [];
         gg = cart;
         let all_available = true;
         if(cart)
         {
            for(let x=0; x<cart.length; x++)
             {
                // console.log("aa ",cart[x].event[0]._id);
                let aa = cart[x].event[0]._id.toString();
               // console.log("aa ",aa);
                let eve_rec = await Event.findOne({_id:aa} ,{totoal_ticket:1});
                if(eve_rec)
                {
                    //console.log("eve_rec ", eve_rec);
                   // cart.push(["dsf",eve_rec.totoal_ticket]);
                   let is_available = true;
                   if(eve_rec.totoal_ticket <= 0)
                   {
                        is_available = false;
                        all_available = false;
                   }else{
                   }
                   cart[x].event.push({"available_totoal_ticket" : eve_rec.totoal_ticket,"is_available":is_available});
                   // gg.push({totoal_ticket : eve_rec.totoal_ticket});
                }else{
                    //gg.push({totoal_ticket : 0});
                    let is_available = false;
                   cart[x].event.push({"available_totoal_ticket" : 0,"is_available":is_available});
                   all_available = false;
                }
                
             } 
         }
         
         //console.log("gg ",gg);
         //return false;
        const totalprice=cart?.reduce((acc,e)=>{
            return acc+(e?.price)*(e?.quantity)
        },0)
        // console.log("newcartdata",newcartdata)
        return res.send({
            status:true,
            message:"cart fetched successfully",
            all_available:all_available,
            data:{cart:cart},
            
            totalprice:totalprice||0
        })
    },

    incEventtocartWeb:async(req,res)=>{
        const userid=req.body.userid;
        const eventid=req.body.eventid;
        const quantityyyyy=req.body.quantity;
        const event= await Event.findById(eventid);
        if(quantityyyyy < 1)
        {
            return res.send({
                status:false,
                message:"La quantité ne doit pas être inférieure à un"
            })
        }
       
       
        const cart=(await User.findById(userid))?.eventcart;
        let isproductthere=cart?.filter(e=>{
            let upid=e.event[0]?._id+"";
           
            
          
            if(upid==eventid){
                return true
            }else{
                return false
            }
        })[0];
       
        let exceptproduct=cart?.filter(e=>e._id+""!=isproductthere?._id+"");
        if(isproductthere){
            isproductthere["quantity"]=quantityyyyy;
            // exceptproduct.concat(cartdata);
            exceptproduct.push(isproductthere);
            await User.findByIdAndUpdate(userid,{
                "eventcart":exceptproduct
            }).then((result)=>{
                return res.send({
                    status:true,
                    message:"Mise à jour de la quantité réussie"
                })
            })
        }
       
        
    },


    incEventtocart:async(req,res)=>{
        const userid=req.body.userid;
        const eventid=req.body.eventid; 
        const event= await Event.findById(eventid);
         
       
       
        const cart=(await User.findById(userid))?.eventcart;
        let isproductthere=cart?.filter(e=>{
            let upid=e.event[0]?._id+"";
           
            
          
            if(upid==eventid){
                return true
            }else{
                return false
            }
        })[0];
       
        let exceptproduct=cart?.filter(e=>e._id+""!=isproductthere?._id+"");
        if(isproductthere){
            isproductthere["quantity"]=isproductthere.quantity+1;
            // exceptproduct.concat(cartdata);
            exceptproduct.push(isproductthere);
            await User.findByIdAndUpdate(userid,{
                "eventcart":exceptproduct
            }).then((result)=>{
                return res.send({
                    status:true,
                    message:"Mise à jour de la quantité réussie"
                })
            })
        }
       
        
    },
    decEventtocart:async(req,res)=>{
        const userid=req.body.userid;
        const eventid=req.body.eventid;
        const event= await Event.findById(eventid);
        
       
       
        const cart=(await User.findById(userid))?.eventcart;
        let isproductthere=cart?.filter(e=>{
            let upid=e.event[0]?._id+"";
           
            
          
            if(upid==eventid){
                return true
            }else{
                return false
            }
        })[0];
       
        let exceptproduct=cart?.filter(e=>e._id+""!=isproductthere?._id+"");
        if(isproductthere){
            isproductthere["quantity"]=isproductthere.quantity-1;
            // exceptproduct.concat(cartdata);
            exceptproduct.push(isproductthere);
            await User.findByIdAndUpdate(userid,{
                "eventcart":exceptproduct
            }).then((result)=>{
                return res.send({
                    status:true,
                    message:"L'événement a été ajouté avec succès"
                })
            })
        }
       
        
    },
    eventcartcheckout:async(req,res)=>
    {
        //https://f5f2-2405-201-300c-9deb-5cd4-cd96-f454-35.in.ngrok.io
        //console.log("here BASE_URL "+BASE_URL);return false;
        const userid=req.body.user_id;
        const user=await User.findById(userid);
        const combo_id = stringGen(20);
        const cart=(await User.findById(userid))?.eventcart;
        let alleventsprice=0
        for(let i=0;i<cart?.length;i++){
           let event=cart[i];
           
           let nooftickets=event.quantity;
           let eventid=event.event[0]?._id;
           let eventorgid=event.event[0]?.org_id;
           let eventtitle=event.event[0]?.title;
           let eventaddress=event.event[0]?.address[0];
           let eventedate=event.edate;
           let eventetime=event.etime;
           let eventsdate=event.sdate;
           let eventstime=event.stime;
           let eventticketype=event.ticketype;
           let payment_type="Cinetpay"
           const org=await OrganizerModel.findById(eventorgid);
            const tickets=[]
            for(let i=0;i<nooftickets;i++){
                let newticket=stringGen(10);
                tickets.push(newticket)
            }
            const allticketsprice=event.price*nooftickets;
            alleventsprice+=allticketsprice
            const newService = new bookingmodel();
            newService.user_id=userid;
            newService.combo_id=combo_id;
            newService.user_name=user.first_name+" "+user.last_name;
            newService.event_id=eventid;
            newService.event_name=eventtitle;
            newService.tickettype=eventticketype;
            newService.ticketprice=allticketsprice
            newService.address=eventaddress;
            newService.tickets=tickets;
            newService.org_id=eventorgid;
            newService.org_name=org?.name;
            newService.edate=new Date(eventedate);
            newService.etime=eventetime;
            newService.sdate=new Date(eventsdate);
            newService.stime=eventstime;
            newService.type=eventticketype;
            newService.payment_type=payment_type;
            newService.purchased_ticket=nooftickets;
           
            newService.save(); 
        }
        const totalprice=cart?.reduce((acc,e)=>{
            return acc+(e?.price)*(e?.quantity)
        },0)
        if(totalprice==0){
            await bookingmodel.updateMany({combo_id:combo_id},{payment_status:true,payment_amount:0,transaction_id:"noneeded"}).then((reulst)=>{
                return res.json({
                    status: true,
                    message: 'Rendez-vous pris avec succès',
                    errmessage:"",
                    data: reulst,
    
                })
            })
        }else{
        return res.json({
            status: true,
            message: 'Rendez-vous pris avec succès',
            errmessage:"",
            url:BASE_URL + "/api/common/getpaymentevent/"+combo_id+"/"+alleventsprice+"/"+userid
        });
        }
        
    },
    getongoingeventsbyuserid:async(req,res)=>{
        const id=req.params.id;
        //mongoose.set('debug',true);
        // await bookingmodel.find({user_id:id,edate: {$gte:new Date()} }).then((result)=>{
        //     return res.send({
        //         status:true,
        //         data:result
        //     })
        // });
        var event_record = await bookingmodel.aggregate([
            {
			    $match: {
                            user_id:id  ,
                            edate: {$gte:new Date()} ,
					}
			},
            
            {$addFields:{
                event_id:{"$toObjectId":"$event_id"},
            }},
            {$lookup:{
                from:"events",
                localField:"event_id",
                foreignField:"_id",
                as:"event"
            }},
            {
                "$project": {
                    "combo_id":1,
                    "event_id":1,
                    "type":1,
                    "event_name":1,
                    "org_name":1,
                    "org_id":1,
                    "edate":1,
                    "etime":1,
                    "sdate":1,
                    "stime":1,
                    "catagory":1,
                    "payment_status":1,
                    "promo_code":1,
                    "payment_method":1,
                    "transaction_id":1,
                    "tickets":1,
                    "date_of_transaction":1,
                    "payment_amount":1,
                    "paid":1,
                    "discounted_amount":1,
                    "status":1,
                    "user_id":1,
                    "user_name":1,
                    "createdAt":1,
                    "order_id":1,
                    "event.address": 1,
                    "event.media":1,
                    "event.title":1,
                    
                }
            },
        ]);
        //console.log("event_record "+JSON.stringify(event_record));
        return res.send({
                    status:true,
                    data:event_record
                })
    },
    getcompletedeventsbyuserid:async(req,res)=>{
        const id=req.params.id
        await bookingmodel.aggregate([
                {
                    $match:{
                    user_id:id,edate:{$lt:new Date()}
                }
            },
            {
                $addFields:{
                    event_id:{"$toObjectId":"$event_id"}
                }
            },
            {
                $lookup:{
                    from:"events",
                    localField:"event_id",
                    foreignField:"_id",
                    as:"event",
                }
            },{
                "$project": {
                    "combo_id":1,
                    "event_id":1,
                    "type":1,
                    "event_name":1,
                    "org_name":1,
                    "org_id":1,
                    "edate":1,
                    "etime":1,
                    "sdate":1,
                    "stime":1,
                    "catagory":1,
                    "payment_status":1,
                    "promo_code":1,
                    "payment_method":1,
                    "transaction_id":1,
                    "tickets":1,
                    "date_of_transaction":1,
                    "payment_amount":1,
                    "paid":1,
                    "discounted_amount":1,
                    "status":1,
                    "user_id":1,
                    "user_name":1,
                    "createdAt":1,
                    "order_id":1,
                    "event.address": 1,
                    "event.media":1,
                    "event.title":1,
                    
                }
            },
            
            
            ]
            
            
            ).then((result)=>{
            return res.send({
                status:true,
                data:result
            })
        })
    },
    getsinglebookingbyid:async(req,res)=>{
        const id=req.params.id
         await bookingmodel.findById(id).then((result)=>{
            return res.send({
                status:true,
                data:result
            })
        })
    },
    getallbookingbyeventidCount:async(req,res)=>{
        try{
            const id=req.params.id
            let resulttt = await bookingmodel.count({event_id:id,paid: true});
            const pages=Math.ceil(resulttt/10);
            return res.send({
                status:true,
                data:pages
            })

        }catch(error)
        {
            return res.send({
                status:false,
                errorMessage:error.message
            })
        }
    },
    getallbookingbyeventid:async(req,res)=>{
        try{

            const id=req.body.id;
            const pageno=req.body.pageno;
            let skipp = pageno*10;

            await bookingmodel.find({event_id:id,paid: true}).skip(skipp).limit(10).then((result)=>{
                return res.send({
                    status:true,
                    data:result
                })
            })
        }catch(error)
        {
            return res.send({
                status:false,
                errorMessage:error.message
            })
        }
    },
    mark_user_fav_event_category:async(req,res)=>{
        try{
            //mongoose.set("debug",true);
            var {user_id,category_name} = req.body;
            //console.log("user_id "+user_id);
            //console.log("category_name "+category_name);
            //let get_record = (await User.findById(user_id))?.fav_event_cat;
            //console.log("get_record "+get_record);
            //return false;

            if(user_id == "" || user_id == null || category_name == "" || category_name == null)
            {
                return res.send({
                    status:false,
                    message:"L'identifiant de l'utilisateur et l'identifiant de la commande sont tous deux requis"
                })
            }else{
                await User.findByIdAndUpdate(user_id,{
                    "fav_event_cat":[]
                }).catch((error)=>{
                    return res.send({
                        status:false,
                        message:error
                    })
                });

                let obj_of_cat = {};
                let category = [];
                for(let x=0; x<category_name.length; x++)
                {
                    //console.log("category_name[x] "+category_name[x]);
                    // $and:[
                    //     { $lte:["$$date.date",new Date(end_date)]},
                    //     { $gte:["$$date.date",new Date(start_date)]}
                    //    ]
                    //fav_event_cat
                    // let get_record = await User.find({
                    //     "_id":user_id,
                    //     "fav_event_cat.category":category_name[x]
                    // });
                    // //console.log("get_record "+get_record);
                    // if(get_record == "")
                    // {}
                        console.log("iffff");
                        let obj_of_cat = {"category":category_name[x]};
                        await User.findByIdAndUpdate(user_id,{
                            "$push":{"fav_event_cat":obj_of_cat}
                            //"fav_event_cat":[]
                        }).catch((error)=>{
                            return res.send({
                                status:false,
                                message:error
                            })
                        });
                    
                }
                return res.send({
                    status:true,
                    message:"Catégorie ajouter un article favori"
                })
            }
        }catch(error)
        {
            console.log(error);
            return res.send({
                status:false,
                message:error
            })
        }
        
    },
    get_mark_user_fav_event_category:async(req,res)=>{
        try{
            var user_id = req.params.user_id;
            if(user_id == "" || user_id == null)
            {
                return res.send({
                    status:false,
                    message:"Identifiant de l'utilisateur requis"
                })
            }
            let get_record = (await User.findById(user_id))?.fav_event_cat;
            console.log("get_record "+get_record);
            return res.send({
                status:true,
                message:"Succès",
                data:get_record
            })
        }catch(error)
        {
            console.log(error);
            return res.send({
                status:false,
                message:error
            })
        }
    },


    downloadinvoice:async(req,res)=>{
        //console.log(req.params)
        try{
            const id=req.params.id;
            // const transaction_id=req.params.transaction_id;
            let invoice = await bookingmodel.findOne({_id:id});
            //console.log("invoice ", invoice);
            //return false;
            let randome=Math.random()
            if(invoice)
            {
                const frenchdate=frenchDayDate(invoice.date_of_transaction);

                const today = new Date(invoice.date_of_transaction);
                const yyyy = today.getFullYear();
                let mm = today.getMonth() + 1; // Months start at 0!
                let dd = today.getDate();
                if (dd < 10) dd = '0' + dd;
                if (mm < 10) mm = '0' + mm;
                let formattedToday = dd + '.' + mm + '.' + yyyy;
                let hourss = today.getHours();
                let minutess = today.getMinutes();
                // console.log("hourss ",hourss);
                // console.log("minutess ",minutess);
                formattedToday = formattedToday +" "+hourss+"h"+minutess;

                // console.log("formattedToday ",formattedToday);
                // return false;

                invoice=JSON.parse(JSON.stringify(invoice));
                invoice["frenchdate"]=formattedToday;
                //console.log(invoice)
                const filepath=path.join(__dirname, "../../views/admin-panel/clientinvoice.ejs");
                //console.log(filepath)
                const html = await ejs.renderFile(filepath,{invoice:invoice});
                //console.log(html)
                const options = { format: 'Letter' };
                
                pdf.create(html,{
                    childProcessOptions: {
                    env: {
                        OPENSSL_CONF: '/dev/null',
                    },
                    } 
                }).toFile(path.join(__dirname, "../../views/admin-panel/public/invoices/")+'Facture-'+invoice.order_id+'.pdf', function(err, result) 
                {
                    if (err) return console.log("in pdf create",err);
                    console.log(BASE_URL+"/static/public/invoices/invoice"+id+".pdf")
                    // console.log(path.join(__dirname, "../../views/admin-panel/public/invoices/invoice"+id+".pdf"));
                    res.send({
                    status:true,
                    url:BASE_URL+"/static/public/invoices/"+'Facture-'+invoice.order_id+".pdf",
                    message:"Facture téléchargée avec succès"
                    })
                });
            }else{
                res.send({
                status:false,
                message:"Facture non trouvée"
                })
            }
        }catch(err){
            console.log("in catch block",err)
            res.send(err.message);
        }
    },

    createOrderInvoice:async(req,res)=>{
        try{
            const id=req.params.id;
            // const transaction_id=req.params.transaction_id;
            //const invoice = await OrderModel.findOne({_id:id});

            const invoice = await OrderModel.findOne({ order_id:id });

            //console.log("invoice herrrrrrrrr ", invoice);
            // //return false;
            // return res.send({
            //     status: true,
            //     message: "payemtn success",
            //     invoice:invoice
            // })


            let randome=Math.random()
            if(invoice)
            {
                  // fullname: 'rajesh.test',
                // shopname: 'j', 
                // email: 'rajesh.test1@gmail.com',
                // countrycode: '91',
                // phone: '9876543457',

                let provider_id = invoice.products[0].provider_id;
                let userId = invoice.userId;
                let result_seller = await sellerModel.findOne({_id:provider_id},{fullname:1,shopname:1,email:1, countrycode:1, phone:1,pickupaddress:1} );


                // first_name: 'Edward ',
                // last_name: 'Auditore',
                // email: 'edward@mailinator.com',
                // phone: '0897631256',

                let result_user = await User.findOne({_id:userId},{first_name:1,last_name:1,email:1, country_code:1, phone:1 } );


                //console.log("result_seller ", result_seller);

                // const frenchdate=frenchDayDate(invoice.date_of_transaction);

                const today = new Date(invoice.date_of_transaction);
                const yyyy = today.getFullYear();
                let mm = today.getMonth() + 1; // Months start at 0!
                let dd = today.getDate();
                if (dd < 10) dd = '0' + dd;
                if (mm < 10) mm = '0' + mm;
                let formattedToday = dd + '.' + mm + '.' + yyyy;
                let hourss = today.getHours();
                let minutess = today.getMinutes();
                // console.log("hourss ",hourss);
                // console.log("minutess ",minutess);
                formattedToday = formattedToday +" "+hourss+"h"+minutess;

                // // console.log("formattedToday ",formattedToday);
                // // return false;

                // invoice=JSON.parse(JSON.stringify(invoice));
                 invoice["frenchdate"]=formattedToday;

                //console.log(invoice)
                const filepath=path.join(__dirname, "../../views/admin-panel/paymentinvoice.ejs");
                //console.log(filepath)

              

                
                const html = await ejs.renderFile(filepath,{invoice:invoice,result_seller:result_seller,result_user:result_user});
                //console.log(html)
                const options = { format: 'Letter' };
                let order_id = invoice.order_id;
                pdf.create(html,{
                    childProcessOptions: {
                    env: {
                        OPENSSL_CONF: '/dev/null',
                    },
                    } 
                }).toFile(path.join(__dirname, "../../views/admin-panel/public/invoices/")+'order_'+order_id+'.pdf', function(err, result) 
                {
                    if (err) return console.log("in pdf create",err);
                    console.log(BASE_URL+"/static/public/invoices/invoice"+order_id+".pdf")
                    // console.log(path.join(__dirname, "../../views/admin-panel/public/invoices/invoice"+id+".pdf"));
                    res.send({
                    status:true,
                    url:BASE_URL+"/static/public/invoices/"+'order_'+order_id+".pdf",
                    message:"order téléchargée avec succès"
                    })
                });
            }else{
                res.send({
                status:false,
                message:"Facture non trouvée"
                })
            }
            
           }catch(err){
             console.log("in catch block",err)
             res.send(err.message);
           }
    },
    
    createOrderInvoiceQrCode:async(req,res)=>{
        try{
            const id=req.params.id;
            var base_url_server = customConstant.base_url;
            // const transaction_id=req.params.transaction_id;
            //const invoice = await OrderModel.findOne({_id:id});

            const invoice = await OrderModel.findOne({ order_id:id });

            //console.log("invoice herrrrrrrrr ", invoice);
            // //return false;
            // return res.send({
            //     status: true,
            //     message: "payemtn success",
            //     invoice:invoice
            // })


            let randome=Math.random()
            if(invoice)
            {
                  // fullname: 'rajesh.test',
                // shopname: 'j', 
                // email: 'rajesh.test1@gmail.com',
                // countrycode: '91',
                // phone: '9876543457',

                let provider_id = invoice.products[0].provider_id;
                let userId = invoice.userId;
                let result_seller = await sellerModel.findOne({_id:provider_id},{fullname:1,shopname:1,email:1, countrycode:1, phone:1,pickupaddress:1} );


                // first_name: 'Edward ',
                // last_name: 'Auditore',
                // email: 'edward@mailinator.com',
                // phone: '0897631256',

                let result_user = await User.findOne({_id:userId},{first_name:1,last_name:1,email:1, country_code:1, phone:1 } );


                //console.log("result_seller ", result_seller);

                // const frenchdate=frenchDayDate(invoice.date_of_transaction);

                const today = new Date(invoice.date_of_transaction);
                const yyyy = today.getFullYear();
                let mm = today.getMonth() + 1; // Months start at 0!
                let dd = today.getDate();
                if (dd < 10) dd = '0' + dd;
                if (mm < 10) mm = '0' + mm;
                let formattedToday = dd + '.' + mm + '.' + yyyy;
                let hourss = today.getHours();
                let minutess = today.getMinutes();
                // console.log("hourss ",hourss);
                // console.log("minutess ",minutess);
                formattedToday = formattedToday +" "+hourss+"h"+minutess;

                // // console.log("formattedToday ",formattedToday);
                // // return false;

                // invoice=JSON.parse(JSON.stringify(invoice));
                 invoice["frenchdate"]=formattedToday;

                //console.log(invoice)
                const filepath=path.join(__dirname, "../../views/admin-panel/paymentinvoice_2.ejs");
                //console.log(filepath)

                let order_id = invoice.order_id;
                var imagUrl = base_url_server+"static/public/invoices/order_"+order_id+".pdf";
                //http://localhost:9000/static/public/invoices/order_499.pdf
                let QRCodeoutput = await QRCode.toDataURL(imagUrl);
                //console.log("QRCodeoutput ---->>>> "+QRCodeoutput);
                //const html = await ejs.renderFile(filepath,{ QRCodeoutput:QRCodeoutput,invoice:invoice,event_record:event_record,ticket:ticket });


                
                const html = await ejs.renderFile(filepath,{invoice:invoice,result_seller:result_seller,result_user:result_user,QRCodeoutput:QRCodeoutput});
                //console.log(html)
                const options = { format: 'Letter' };
                
                pdf.create(html,{
                    childProcessOptions: {
                    env: {
                        OPENSSL_CONF: '/dev/null',
                    },
                    } 
                }).toFile(path.join(__dirname, "../../views/admin-panel/public/invoices/")+'order_'+order_id+'.pdf', function(err, result) 
                {
                    if (err) return console.log("in pdf create",err);
                    console.log(BASE_URL+"/static/public/invoices/invoice_2"+order_id+".pdf")
                    // console.log(path.join(__dirname, "../../views/admin-panel/public/invoices/invoice"+id+".pdf"));
                    res.send({
                    status:true,
                    url:BASE_URL+"/static/public/invoices/"+'order_'+order_id+".pdf",
                    message:"order téléchargée avec succès"
                    })
                });
            }else{
                res.send({
                status:false,
                message:"Facture non trouvée"
                })
            }
            
           }catch(err){
             console.log("in catch block",err)
             res.send(err.message);
           }
    }
}