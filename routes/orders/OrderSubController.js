//const request = require("request");
const orderModel=require("../../models/orders/order");
const User = require('../../models/user/User');
const AuctioninitiateModel = require("../../models/products/AuctioninitiateModel")
const http = require('http');
const  PaymentkeysModel  = require("../../models/admin/PaymentkeysModel");
//const { curly } = require('node-libcurl')
const axios = require('axios');
const BASE_URL=process.env.BASE_URL;
const  customConstant  = require("../../helpers/customConstant");
// const querystring = require("querystring");
// const { Curl } = require("node-libcurl");
//const terminate = curlTest.close.bind(curlTest);
const sellerModel=require("../../models/seller/Seller");
const nodemailer = require("nodemailer");
const mongoose=require("mongoose")
const async = require("async");
const subscriptions = require("../../models/subscriptions/Subscription");
const NotificationPermissionModel=require("../../models/NotificationPermission");
const Notifications = require("../../models/Notifications"); 
const fs = require('fs');
const path = require("path");


module.exports = {

    requestPayment: async (req, res) => {
      try{
        //   const DigiPayGatway =  require("./DigiPayGatway.js") ;
        //   let id = req.body.id;
        //   let other = req.body.other;
        //   /*START SDA*/
        //   let payment = new DigiPayGatway();

        //   /*CONFIG APPLICATION START*/
        //     payment.success_url = ('https://mainserver.rawgadou.com/api/order/getSuccessDGPayment');
        //     payment.cancel_url = ('https://mainserver.rawgadou.com/api/order/getCancelDGPayment');
        //     payment.ipn_url = ('https://mainserver.rawgadou.com/api/order/ipn');
        //     payment.app_key = "AF965D0E-5298-4046-88F6-1091E9E04C8E";
        //     payment.secrete_key = "YOUR_SECRETE_KEY";
        //     payment.secrete_key_test = "YOUR_SECRETE_TEST_KEY";
        //     payment.public_key = "D6EFC0C4CE8BB3A92E52BA2333AB7D9FBABAD3E7FF767FB5A09621E041D77173";
        //     payment.public_key_test = "AAFB60C50AA22D9A96C97AA7D2A052DEBFDF74F51607C3AFDB8E325553CEBB03";
        //     payment.currency = "EUR";
        //   /*CONFIG APPLICATION END*/

        //   /*Client info */
        //   payment.client_name = "Pape Ndour"
        //   payment.client_phone = "777293282"
        //   payment.client_email = "papesambandour@hotmail.com"
        //   /*Client info */

        //   /*ADD PRODUCT INFOS START*/
        //   payment.mode = 'test';
        //   payment.amount = 10;
        //   payment.ref_commande = Math.random().toString().substr(3,20);
        //   payment.commande_name ="Achat Produit sur Digi-shop";
        //   payment.mode =DigiPayGatway.MODE_TEST;
        //   payment.data_transactions ={
        //       "myProductId" : id,
        //       "otherInfo" : other
        //   };
        //   /*ADD PRODUCT INFOS END*/
        //   /*PROCESS  GET  URL PAYMENT*/
        //   //let abc = await payment.start();
        //  // console.log(abc);
        //  await payment.start()
        //   //return  res.json(await payment.start())
        let payment_record = await PaymentkeysModel.findOne();
        if(!payment_record)
        {
          return res.send({
            error:true,
            message:"Les clés de paiement ne sont pas disponibles",
            data:{}
          });
        }else{
          //console.log("hereeeeeeeeeeeeeeeeee");
          let id = req.params.id;
          //console.log("id =============== ", id );
          const order=await orderModel.findOne({transaction_id:id});
          if(!order)
          {
            return res.send({
              error:true,
              status:false,
              message:"Identifiant de commande invalide",
              data:{}
            })
          }
          let totalPrice = order.totalPrice;
          //console.log("order ============>>>>>>>>>>>>>> ", order);
          let for_order_rec = await orderModel.aggregate([
            {
              $match:{
                "transaction_id":id
              }
            },
            {
              $group:{
                _id:null,
                totalAmount:{ $sum:"$totalPrice" }
              }
            }
          ]);
          console.log("for_order_rec ", for_order_rec);
          if(for_order_rec.length > 0)
          {
            totalPrice = for_order_rec[0].totalAmount
          }
          //return false;
          //let totalPrice = order.totalPrice;
          //totalPrice = 2;


          let userId = order.userId;
          let user_rec = await User.findOne({_id:userId})
          //console.log("user_rec ", user_rec);
          
          let first_name = '';
          let last_name = '';
          let email = '';
          let phone = '';
          if(user_rec)
          {
            first_name = user_rec.first_name;
            last_name = user_rec.last_name;
            email = user_rec.email;
            phone = user_rec.phone;
          }
          client_name = first_name+" "+last_name;
          
          //return false;


            //let id = req.params.id;
            let temp_order_id = id;
            let data_json = {
              'myProductId':id
            }
        //     const { data } = await curly.post('https://proxy.bgpay.digital/api/payment/request', {
        //     postFields: JSON.stringify(
        //       {
        //         app_key:payment_record.app_key,
        //         secrete_key:payment_record.secrete_key,
        //         public_key:payment_record.public_key,
        //         mode:payment_record.mode,
        //         amount:5,
        //         commande_name:"Achat Produit sur Digi-shop",
        //         ref_commande:temp_order_id,
        //         currency:"XOF",
        //         data_transactions:JSON.stringify(data_json),
        //         ipn_url:"https://mainserver.rawgadou.com/api/order/ipn",
        //         success_url:"https://mainserver.rawgadou.com/api/order/getSuccessDGPayment",cancel_url:"https://mainserver.rawgadou.com/api/order/getCancelDGPayment",failed_url:"https://mainserver.rawgadou.com/api/order/getFailedDGPayment",client_name:"Test client name",
        //         client_phone:"775695300",
        //         client_email:"test789@mailinator.com"
        //       }
        //     ),
        //     httpHeader: [
        //       'Content-Type: application/json',
        //       'Accept: application/json'
        //     ],
        //   })
        //   console.log(data)
        //   if(data.error == true)
        //   {
        //     return res.send({
        //       error:true,
        //       message:data.msg,
        //       data:{}
        //     })
        //   }else{
        //     let redirect_url_web = data.data.url_payment;
        //     res.redirect(303, redirect_url_web);
        //     // return res.send({
        //     //   error:false,
        //     //   message:data.msg,
        //     //   data:data
        //     // })
        //   }
            
            let p_name = "Order - "+temp_order_id;
            axios({
              method: 'POST',
              url: 'https://proxy.bgpay.digital/api/payment/request',
              data: {
                app_key:payment_record.app_key,
                secrete_key:payment_record.secrete_key,
                public_key:payment_record.public_key,
                mode:payment_record.mode,
                amount:totalPrice,
                commande_name:p_name,
                ref_commande:temp_order_id,
                currency:"XOF",
                data_transactions:JSON.stringify(data_json),
                ipn_url:"https://mainserver.rawgadou.com/api/order/ipn",
                success_url:"https://mainserver.rawgadou.com/api/order/getSuccessDGPayment?payment_id="+temp_order_id,cancel_url:"https://mainserver.rawgadou.com/api/order/getCancelDGPayment",failed_url:"https://mainserver.rawgadou.com/api/order/getFailedDGPayment",client_name:client_name,
                client_phone:phone,
                client_email:email
              }
            }).then(function (response) {
                // console.log("response ", response.data);
                // console.log("response ", response);
                let final_res = {};
                if(response)
                {
                    if(response.data)
                    {
                        let data = response.data;
                        console.log("data ", data);
                        if(data.error == true)
                          {
                            return res.send({
                              error:true,
                              message:data.msg,
                              data:{}
                            })
                          }else{
                            let redirect_url_web = data.data.url_payment;
                            res.redirect(303, redirect_url_web);
                            // return res.send({
                            //   error:false,
                            //   message:data.msg,
                            //   data:data
                            // })
                          }
                    }else{
                        return res.send({
                              error:true,
                              message:"Error",
                              data:{}
                            })
                    }
                }else{
                        return res.send({
                              error:true,
                              message:"Error",
                              data:{}
                            })
                    }
            }).catch((err)=>{
                console.log("errrr",err) ;
            });
            

        }
        
      }catch(e){
        return res.send({
          error:true,
          message:e.message
        })
      }
    },
    requestPaymentAuction:async(req,res)=>{
      try{
        console.log(req.params);
        let id = req.params.id;
        let payment_record = await PaymentkeysModel.findOne();
        if(!payment_record)
        {
          return res.send({
            error:true,
            status:false,
            message:"Les clés de paiement ne sont pas disponibles",
            data:{}
          });
        }else{
          //console.log("hereeeeeeeeeeeeeeeeee");
          
          //console.log("id =============== ", id );
          //mongoose.set("debug",true);

          const order=await AuctioninitiateModel.findOne({_id:id});
          //console.log("order ", order);
          //return false;
          if(!order)
          {
            return res.send({
              error:true,
              status:false,
              message:"Identifiant de commande invalide",
              data:{}
            })
          }
          //console.log("order ", order);return false;
          let totalPrice = order.amount;
          
          //return false;
          //let totalPrice = order.totalPrice;
          //totalPrice = 2;


          let userId = order.userId;
          let user_rec = await User.findOne({_id:order.user_id})
          //console.log("user_rec ", user_rec);
          
          let first_name = '';
          let last_name = '';
          let email = '';
          let phone = '';
          if(user_rec)
          {
            first_name = user_rec.first_name;
            last_name = user_rec.last_name;
            email = user_rec.email;
            phone = user_rec.phone;
          }
          client_name = first_name+" "+last_name;
          
          //console.log("totalPrice ", totalPrice);
          //return false;


            //let id = req.params.id;
            let temp_order_id = id;
            let data_json = {
              'myProductId':id
            }
            
            let p_name = "Order - "+temp_order_id;
            axios({
              method: 'POST',
              url: 'https://proxy.bgpay.digital/api/payment/request',
              data: {
                app_key:payment_record.app_key,
                secrete_key:payment_record.secrete_key,
                public_key:payment_record.public_key,
                mode:payment_record.mode,
                amount:totalPrice,
                commande_name:p_name,
                ref_commande:temp_order_id,
                currency:"XOF",
                data_transactions:JSON.stringify(data_json),
                ipn_url:"https://mainserver.rawgadou.com/api/order/ipn",
                success_url:"https://mainserver.rawgadou.com/api/order/getSuccessDGPaymentAuction?payment_id="+temp_order_id,cancel_url:"https://mainserver.rawgadou.com/api/order/getCancelDGPaymentAuction",failed_url:"https://mainserver.rawgadou.com/api/order/getFailedDGPaymentAuction",client_name:client_name,
                client_phone:phone,
                client_email:email
              }
            }).then(function (response) {
                // console.log("response ", response.data);
                // console.log("response ", response);
                let final_res = {};
                if(response)
                {
                    if(response.data)
                    {
                        let data = response.data;
                        console.log("data ", data);
                        if(data.error == true)
                          {
                            return res.send({
                              error:true,
                              message:data.msg,
                              data:{}
                            })
                          }else{
                            let redirect_url_web = data.data.url_payment;
                            res.redirect(303, redirect_url_web);
                            // return res.send({
                            //   error:false,
                            //   message:data.msg,
                            //   data:data
                            // })
                          }
                    }else{
                        return res.send({
                              error:true,
                              message:"Error",
                              data:{}
                            })
                    }
                }else{
                        return res.send({
                              error:true,
                              message:"Error",
                              data:{}
                            })
                    }
            }).catch((err)=>{
                console.log("errrr",err) ;
            });
            

        }
      }catch(e){
        return res.send({
          status:false,
          error:true,
          message:e.message
        })
      }
    },
    getSuccessDGPayment:async(req,res)=>{
      console.log("BASE_URL ", BASE_URL);
    //   console.log("success url called");
    //   console.log("res ", res);
    //   console.log("---------------------------------------------------");
    //   console.log("req ", req);
    //   console.log("success url called");
        console.log(req.query);
        //http://localhost:9001/api/order/getSuccessDGPayment?payment_id=729575333493145
        let payment_id = req.query.payment_id;
        const order=await orderModel.findOne({transaction_id:payment_id},{_id:1,combo_id:1});
        if(order.combo_id == "")
        {
          var url = BASE_URL + "/api/common/getpayment/"+order._id
        }else{
          //const url = BASE_URL + "/api/common/getpaymentforcombo/" + combo_id + "/" + totalprice + "/" + userId + "/" + from;
          var url = BASE_URL + "/api/common/getpaymentforcombo/"+order.combo_id
        }

        
        res.redirect(303, url);
        //console.log("payment_id ------------>>>", req.query.payment_id);
      // return res.send({
      //     error:false,
      //     message:"Payment success"
      // })
    },
    getSuccessDGPaymentAuction:async(req,res)=>{
      
      //   console.log("success url called");
      //   console.log("res ", res);
      //   console.log("---------------------------------------------------");
      //   console.log("req ", req);
      //   console.log("success url called");
          console.log(req.query);
          //http://localhost:9001/api/order/getSuccessDGPayment?payment_id=729575333493145
          
          let payment_id = req.query.payment_id;
          const order=await AuctioninitiateModel.findOne({_id:payment_id});
          if(!order)
          {
            return res.send({
              status:false,
              error:true,
              message:"Identifiant non valide"
            });
          }else{
            await AuctioninitiateModel.updateOne({_id:payment_id},{is_payment:true}).then((result)=>{
              let url = customConstant.base_url_user_site+"bidCheckout/"+order.user_id+"/"+order.product_id;
              res.redirect(303, url);
            }).catch((e)=>{
              return res.send({
                status:false,
                error:true,
                message:e.message
              });
            });
          }
          
          
          //console.log("payment_id ------------>>>", req.query.payment_id);
        // return res.send({
        //     error:false,
        //     message:"Payment success"
        // })
      },
    getCancelDGPayment:async(req,res)=>{
      console.log("cancel url called");
      
      let url = customConstant.base_url_user_site+"paymentFailed";
      res.redirect(303, url);
      //  return res.send({
      //     error:true,
      //     message:"Payment cancel"
      //   })
    },

    getFailedDGPayment:async(req,res)=>{
      console.log("failed url");
      let url = customConstant.base_url_user_site+"paymentFailed";
      res.redirect(303, url);
      // return res.send({
      //     error:true,
      //     message:"Payment failed"
      //   })
    },
    getCancelDGPaymentAuction:async(req,res)=>{
      console.log("cancel url called");
      let url = customConstant.base_url_user_site+"paymentFailed";
      res.redirect(303, url);
      //  return res.send({
      //     error:true,
      //     message:"Payment cancel"
      //   })
    },

    getFailedDGPaymentAuction:async(req,res)=>{
      console.log("failed url");
      let url = customConstant.base_url_user_site+"paymentFailed";
      res.redirect(303, url);
      
      // return res.send({
      //     error:true,
      //     message:"Payment failed"
      //   })
    },
    ipn:async(req,res)=>{
      
    },
    getPaymentForSub:async(req,res)=>{
      try{
        let {id,amount} = req.params;
        console.log("id ", id);
        console.log("amount ", amount);



        let payment_record = await PaymentkeysModel.findOne();
        if(!payment_record)
        {
          return res.send({
            error:true,
            message:"Les clés de paiement ne sont pas disponibles",
            data:{}
          });
        }else{
          //console.log("hereeeeeeeeeeeeeeeeee");
          //let id = req.params.id;
          //console.log("id =============== ", id );
          const order=await subscriptions.findOne({order_id:id});
          if(!order)
          {
            return res.send({
              error:true,
              status:false,
              message:"Identifiant de commande invalide",
              data:{}
            })
          }
          let totalPrice = order.payment_amount;
          //console.log("order ============>>>>>>>>>>>>>> ", order);
           
          //return false;
          //let totalPrice = order.totalPrice;
          //totalPrice = 2;


          let userId = order.provider_id;
          let user_rec = await sellerModel.findOne({_id:userId})
          //console.log("user_rec ", user_rec);
          
          let first_name = '';
          let last_name = '';
          let email = '';
          let phone = '';
          if(user_rec)
          {
            first_name = user_rec.fullname; 
            email = user_rec.email;
            phone = user_rec.phone;
          }
          client_name = first_name;
          
          //return false;


            //let id = req.params.id;
            let temp_order_id = id;
            let data_json = {
              'myProductId':id
            }
         
            let seler_success_subscription = customConstant.base_url+"api/order/getSuccessPaymentSellerSubscription?payment_id="+temp_order_id;
            let seler_cancel_subscription = customConstant.base_url+"api/order/getCancelPaymentSellerSubscription";



            let p_name = "Order - "+temp_order_id;
            axios({
              method: 'POST',
              url: 'https://proxy.bgpay.digital/api/payment/request',
              data: {
                app_key:payment_record.app_key,
                secrete_key:payment_record.secrete_key,
                public_key:payment_record.public_key,
                mode:payment_record.mode,
                amount:totalPrice,
                commande_name:p_name,
                ref_commande:temp_order_id,
                currency:"XOF",
                data_transactions:JSON.stringify(data_json),
                ipn_url:"https://mainserver.rawgadou.com/api/order/ipn",
                success_url:"https://mainserver.rawgadou.com/api/order/getSuccessPaymentSellerSubscription?payment_id="+temp_order_id,cancel_url:"https://mainserver.rawgadou.com/api/order/getCancelPaymentSellerSubscription",failed_url:"https://mainserver.rawgadou.com/api/order/getFailedSellerSubscription",client_name:client_name,
                client_phone:phone,
                client_email:email
              }
            }).then(function (response) {
                // console.log("response ", response.data);
                // console.log("response ", response);
                let final_res = {};
                if(response)
                {
                    if(response.data)
                    {
                        let data = response.data;
                        console.log("data ", data);
                        if(data.error == true)
                          {
                            return res.send({
                              error:true,
                              message:data.msg,
                              data:{}
                            })
                          }else{
                            let redirect_url_web = data.data.url_payment;
                            res.redirect(303, redirect_url_web);
                            // return res.send({
                            //   error:false,
                            //   message:data.msg,
                            //   data:data
                            // })
                          }
                    }else{
                        return res.send({
                              error:true,
                              message:"Error",
                              data:{}
                            })
                    }
                }else{
                        return res.send({
                              error:true,
                              message:"Error",
                              data:{}
                            })
                    }
            }).catch((err)=>{
                console.log("errrr",err) ;
            });
            

        }

      }catch(e){
        return res.send({
          status:false,
          message:e.message
        })
      }
    },
    getSuccessPaymentSellerSubscription:async(req,res)=>
    {
      console.log(req.query);
      let payment_id = req.query.payment_id;
      console.log("payment_id ",payment_id);
      let id = payment_id;
      const sub=await subscriptions.findOne({order_id:id});
      if(sub)
      { 
        //console.log(sub);return false;
        const seller = await sellerModel.findById(sub.provider_id);
        if(!sub || !seller){
            return res.send({
                status:false,
                message:"L'utilisateur ou l'ordre n'existe pas"
            })
        }
        
        const pathtofileforprovider = path.resolve("views/notifications/updateprovideraboutorderconfirmation.ejs");
        
        let pushmessagetouser=``;
        
     
        // console.log("seller ",seller);
        // seller.sub_id=sub._id;
        //     seller.save();
        //mongoose.set("debug",true);
        await sellerModel.updateOne({_id:sub.provider_id},{sub_id:sub._id});
        let notipermissionPermissionforProvider = await NotificationPermissionModel.findOne({ id: sub.provider_id });
        
        sub.payment_status = true;
        sub.date_of_transaction = new Date();
        sub.payment_method = "Mobile money";
        sub.save().then(async (result) => {
            //await savetransactionclient(sub, "");
            // const message = process.env.MESSAGE_PAYMENT_SUCCESS;
            // const messageToUser="somethinb"
            // // console.log("user.email",user.email)
            // if (notipermissionPermissionforProvider) {
            //     if (notipermissionPermissionforProvider.push_reservation) {
            //         sendpushnotificationtouser(message, seller, seller._id);
            //     } else {
            //         console.log("push notification is disabled for user")
            //     } if (notipermissionPermissionforProvider.email_reservation) {
            //         sendmail(pathtofileforprovider,
            //             { name: seller.first_name+" "+seller.last_name, message:messageToUser},
            //             "Succès du paiement",
            //             seller.email);
            //     } else {
            //         console.log("email notification is disabled for user")
            //     }
            // } else {
            //     sendpushnotificationtouser(message, seller, seller._id);
            //     sendmail(pathtofileforprovider,
            //         { name: seller.first_name+" "+seller.last_name, message:messageToUser},
            //         "Succès du paiement",
            //         seller.email);
            // }
            
        })
        // return res.send({
        //     status: true,
        //     message: "payment success",
        // })
        // return res.redirect("/api/common/paymentsucess/success=true");
        let url = customConstant.base_url_seller_site+"paymentSuccess";
        res.redirect(303, url);
      }else{
        if(!sub || !seller){
          return res.send({
              status:false,
              message:"L'utilisateur ou l'ordre n'existe pas"
          })
        }
      }
        

    },

    getCancelPaymentSellerSubscription:async(req,res)=>{
      console.log("failed url");
      let url = customConstant.base_url_seller_site+"paymentFailed";
      res.redirect(303, url);
      
      // return res.send({
      //     error:true,
      //     message:"Payment failed"
      //   })
    },

    getFailedSellerSubscription:async(req,res)=>{
      console.log("failed url");
      let url = customConstant.base_url_seller_site+"paymentFailed";
      res.redirect(303, url);
      
      // return res.send({
      //     error:true,
      //     message:"Payment failed"
      //   })
    }
};