const orders = require("../../models/orders/order");
const ordersModel = require("../../models/orders/order");
const Ratingnreviews = require("../../models/actions/Ratingnreviews");
const User= require("../../models/user/User");
const seller=require("../../models/seller/Seller");
const mongoose=require("mongoose");
const VariationsPrice =require("../../models/products/VariationPrice");
const {calculateShippingcostforproducts} =require("../../routes/shipping/ShippingController");
const sellerModel=require("../../models/seller/Seller");
const AdminModel = require("../../models/admin/AdminModel");
const Notifications = require("../../models/Notifications");

const sales=require("../../models/sales/Sales");
const sellers=require("../../models/seller/Seller");
const pageViewsModel=require("../../models/products/pageViews");
const {Product}=require("../../models/products/Product");
const usersmodel= require("../../models/user/User");
const base_url = process.env.BASE_URL + "/admin-panel/";
const root_url = process.env.BASE_URL;
const {getdateinadvance,getDays,getDate} =require("../../modules/dates.js")
const moment=require("moment")

// async function updatearrayelement(model,olddata,dataarray,elemid,elemidtype,modelid,datatoupdate) {
//     let isaddressalreadythere = olddata[dataarray]?.filter((add) => add?._id+'' == elemid)
//     console.log("isaddressalreadythere",isaddressalreadythere)
//     const query = { _id: modelid };
//     let fieldtoqueryinarray=`${dataarray}.$[item]`
//     console.log("fieldtoqueryinarray",fieldtoqueryinarray)
//     const updateDocument = {
//         "products.$[item]":{
//             "readytopickup":true,
//             "provider_id":products.$[item]['provider_id']
//         }
//     };
//     // updateDocument[fieldtoqueryinarray]["$[item]"]=datatoupdate
//     console.log("updateDocument",updateDocument)
//     const arrayfilterfield=`item.${elemidtype}`;
//     const options = {
//         arrayFilters: [
//             {
                
//                 "item.provider_id": elemid,

//             },
//         ],
//     };
//     // options["arrayFilters"][0][arrayfilterfield]=elemid;
//     console.log("updateDocument",updateDocument,"arrayfilterfield",arrayfilterfield)
//     const result = await model.updateOne(query, updateDocument, options);
// }
module.exports = {
    createorders: async (req, res) => {
        const {
            products,
            address,
            userId,
            shipping_type,
            id,
            totaltax,
            shipping_city,
            totalshipping
        } = req.body;
        let totalprice=0
        let totalproductsinqty=0;
        const shippingcostsArray=[];
        if (products.length) {
            totalprice = products?.reduce((accm, elem) => {
                // console.log("elem", elem)
                if ((elem?.product?.price||elem.varientPrice) && elem.quantity) {
                    totalproductsinqty+=elem.quantity;
                    let productprice=elem.varientPrice||elem?.product?.price;
                    return accm + (productprice * elem?.quantity)
                }
                return accm;
            }, 0);
            for(let i=0;i<products.length;i++){
                product=products[i]
                let productprice=product.varientPrice||product?.product?.price;
                let data3={
                    productprice:productprice,product_id:product.product._id,product_qty:product.quantity,shipping_city:shipping_city
                }
               await calculateShippingcostforproducts(data3).then((result)=>{
                    console.log("product",product,"result",result)
                    if(!result.result.length){
                    product["is_shippable"]=false;
                    product["shipping_cost"]=0
                    }else{
                        product["shipping_cost"]=result.result[0].shippingcostforproducts
                    }
                   
                    product["tax"]=result.producttax
                    product["commission"]=result.productcommission
                    console.log("product",product)
                    // data={
                    //     productcommission:result.productcommission,
                    //     producttax:result.producttax,
                    //     shippingcost:result[0].costforproducts
                    // }
                    // shippingcostsArray.push(data);
                })
               
            }
        }
        const totalcommissions=200;
        if (id) {
            console.log("products=============>",products)
            const datatoupdate = {
                ...(products.length && { products: products }),
                ...(address && { address: address }),
                ...(shipping_type && { shipping_type: shipping_type }),
                ...(totalprice && { totalPrice: totalprice }),
                ...(totalcommissions && { totalcommission: totalcommissions }),
                ...(products.length&&{totalproductsinorder:products.length}),

            }
            await orders.findByIdAndUpdate(id, datatoupdate)
                .then((result) => {
                    return res.send({
                        status: true,
                        message: "order updated successfully"
                    })
                })
        } else {
            const newOrder = new orders();
            newOrder.products = products;
            newOrder.address = address;
            newOrder.shipping_type=shipping_type;
            newOrder.userId = userId;
            newOrder.totalPrice = totalprice;
             newOrder.totalcommission = totalcommissions;
            newOrder.totalproductsinorder = products.length;
            newOrder.save().then((result) => {
                return res.send({
                    status: true,
                    message: "created successfully"
                })
            })
        }
    },
    createordersfunc: async (products,address,userId,id) => {

        let totalproductsinqty=0;
        let totalprice = undefined;
        const shippingcostsArray=[];
        if (products.length) {
            totalprice = products?.reduce((accm, elem) => {
                console.log("elem", elem)
                if (elem?.product?.price && elem.quantity) {
                    totalproductsinqty+=elem.quantity;
                    return accm + (elem?.product?.price * elem?.quantity)
                }
                return accm;
            }, 0);
            products?.map((product)=>{
                calculateShippingcostforproducts(product_id,product_qty,shipping_city).then((result)=>{
                    data={
                        productcommission:result.productcommission,
                        producttax:result.producttax,
                        shippingcost:result[0].costforproducts
                    }
                    shippingcostsArray.push(data);
                })
            })
        }
        const totalcommissions=200;
        
        const newOrder = new orders();
        newOrder.products = products;
        newOrder.address = address;
        newOrder.userId = userId;
        newOrder.totalproductsinqty = totalproductsinqty;
        newOrder.totalPrice = totalprice;
         newOrder.totalcommission = totalcommissions;
        newOrder.totalproductsinorder = products.length;
        newOrder.save().then((result) => {
            // console.log("result",result)
          
            
        })
        return newOrder
    },
    deleteorder: async (req, res) => {
        const id = req.body.id;
        await orders.findByIdAndDelete(id)
            .then((result) => {
                return res.send({
                    status: true,
                    message: "deleted successfully"
                })
            });
    },
    getallordersbyuseridCount: async (req, res) => 
    {
        try{
            const userid = req.params.userid;
            let total = 0;
            var totalPageNumber = 0;
            var perPageRecord = 10;
            const allordersCount = await orders.count({ userId: userid,payment_status:true});
            
            total = allordersCount;
            totalPageNumber = total / perPageRecord;
            //console.log("totalPageNumber"+totalPageNumber);
            totalPageNumber = Math.ceil(totalPageNumber);
            return res.send({
                status: true,
                message: "Succès",
                totalPageNumber:totalPageNumber
            })
        }catch(e){
            return res.send({
                status: true,
                message: e.message,
                data: []
            })
        }
        
    },
    getAllBidOrdersByUserIdCount: async (req, res) => 
    {
        try{
            const userid = req.params.userid;
            let total = 0;
            var totalPageNumber = 0;
            var perPageRecord = 10;
            const allordersCount = await orders.count({ userId: userid,payment_status:true,bid_id:{$ne:null} });
            
            total = allordersCount;
            totalPageNumber = total / perPageRecord;
            //console.log("totalPageNumber"+totalPageNumber);
            totalPageNumber = Math.ceil(totalPageNumber);
            return res.send({
                status: true,
                message: "Succès",
                totalPageNumber:totalPageNumber
            })
        }catch(e){
            return res.send({
                status: true,
                message: e.message,
                data: []
            })
        }
        
    },
    getAllBidOrdersByUserId: async (req, res) => 
    {
        try{
            console.log("req.params ",req.params);
            const userid = req.params.userid;
            const page_no = req.params.page_no;
            let howMuchRecord = 10;
            let fromm = 0;
            if(page_no)
            {
                if(page_no > 0)
                {
                    fromm = page_no * howMuchRecord;
                }
            }
            const allorders = await orders.find({ userId: userid,payment_status:true,bid_id:{$ne:''} }).skip(fromm).limit(howMuchRecord).sort({"createdAt":-1});
            return res.send({
                status: true,
                message: "Succès",
                data: allorders
            })
        }catch(e){
            return res.send({
                status: true,
                message: e.message,
                data: []
            })
        }
        
    },
    getallordersbyuserid: async (req, res) => 
    {
        try{
            console.log("req.params ",req.params);
            const userid = req.params.userid;
            const page_no = req.params.page_no;
            let howMuchRecord = 10;
            let fromm = 0;
            if(page_no)
            {
                if(page_no > 0)
                {
                    fromm = page_no * howMuchRecord;
                }
            }
            const allorders = await orders.find({ userId: userid,payment_status:true}).skip(fromm).limit(howMuchRecord).sort({"createdAt":-1});
            return res.send({
                status: true,
                message: "Succès",
                data: allorders
            })
        }catch(e){
            return res.send({
                status: true,
                message: e.message,
                data: []
            })
        }
        
    },
    getallordersbyuseridPostMethod: async (req, res) => 
    {
        //console.log(req.body);
        // var datetime = new Date();
        // console.log(datetime);  


        //mongoose.set("debug",true);
        const userid = req.body.userid;
        var status = req.body.status;
        var delivery_status = req.body.delivery_status;
        let start_date = req.body.start_date;
        let end_date = req.body.end_date;
        const query={};
        query["userId"]=userid;
        query["payment_status"]=true;
        if(start_date && !end_date){
            let start_date_f=new Date(start_date);
            query["date_of_transaction"]={
                $gte:start_date_f
            }
        }
        else if(end_date && !start_date){
            let end_date_f=new Date(end_date);
            query["date_of_transaction"]={
                $lte:end_date_f
            }
        }else if(start_date && end_date){
            let start_date_f=new Date(start_date);
            let end_date_f=new Date(end_date);
            // query["$and"]=[
            //     {
            //         $gte:start_date_f
            //     },
            //     {
            //         $lte:end_date_f
            //     }
            // ]
            query["$and"] = [
                {date_of_transaction:{'$gte':start_date_f}},
                {date_of_transaction:{'$lte':end_date_f}}
            ]
        }

        if(status == 2)
        {
            query["products"] = {
                $elemMatch: 
                    { status: 2 } 
            } 
        }
        if(delivery_status == 1)
        {
            query["products"] = {
                $elemMatch: 
                    { delivery_status: {$lt:1} } 
            } 
        }
        
    

        // { 
        //     $match:{ userId: userid}
        // },
        // {
        //     $match:{ payment_status:true }
        // }
        const allorders = await orders.aggregate([
            {$match:query},{
                $sort:{"createdAt":-1}
            }
        ]).then((reult)=>{
            return res.send({
                status: true,
                message: "fetched successfully",
                data: reult
            })
        }).catch((error)=>{
            return res.send({
                status: false,
                message: error,
                data: []
            })
        });
        
    },
    getorderdetailbyorderid:async(req,res)=>{

        try{
            const id=req.params.id;
            if(id == "" || id == null || id == "null")
            {
                return res.send({
                    status:false,
                    message:"Id requis",
                    data:[]
                })
            }
            console.log("id "+ id);
            let objectid;
            if(id){
                objectid=mongoose.Types.ObjectId(id)
            }
            console.log("objectid "+objectid);
            const orders=await ordersModel.aggregate([
                {$match:{_id:objectid}},
                {
                    $addFields:{
                        userObjID:{"$toObjectId":"$userId"}
                    }
                },
                {
                    $lookup:{
                        from:"users",
                        foreignField:"_id",
                        localField:"userObjID",
                        as:"user"
                    }
                },
            

                {$addFields:{
                    "userobject":{$arrayElemAt:["$user",0]},
                    "sellerearning":{
                        $subtract:["$totalPrice",{$add:["$totalcommission",{$add:["$totaltax",{$add:["$totalplusperproduct","$totalshipping"]}]}]}]
                    },
                    "singleprovider":{
                        arrayElemAt:["$provider",0]
                    },
                    "stringorderid":{"$toString":"$order_id"}
                }},
                {
                    $addFields:{
                        // allproducts:"$products",
                        sellerinfo:{
                            $map:
                            {
                            input: "$products",
                            as: "product",
                            in: { 
                                "isreadytopickup":{
                                    $cond:{
                                        if:{$gt:["$$product.readytopickup",null]},
                                        then:"$$product.readytopickup",
                                        else:false
                                    }
                                },
                                // "part_id":{$concat: ["$stringorderid", {$concat:["-",{"$toString":{$add:[{$indexOfArray:["$products","$$product"]},1]}}]}]},
                                "price":"$$product.pricetouser",
                                "part_id":"$$product.part_id",
                                "delivery_status":"$$product.delivery_status",
                                "sellername":"$$product.provider_name",
                                "tax":"$$product.tax",
                                "shipping_cost":"$$product.shipping_cost",
                                "commission":"$$product.commission",
                                "sellerprofit":"$$product.sellerprofit",
                                "variation_id":"$$product.variation_id",
                                "readytopickup":"$$product.readytopickup",
                                "userfirstname":"$userobject.first_name",
                                "userlastname":"$userobject.last_name",
                                "product":"$$product",
                                "deliveryaddress":"$shippingaddress"
                            }
                            }
                        }
                    }
                },
                // {$project:{
                //     "order_id":1,
                //     "sellerinfo":1,
                //     "totalPrice":1,
                //     "sellerearning":1,
                //     "totalcommission":1,
                //     "totalshipping":1,
                //     "totaltax":1,
                //     "status":1,
                //     "createdAt":1,
                //     "status":1
                // }}
            ]);
            //console.log("orders    ----->>>>>"+JSON.stringify(orders));
            //return false;
            const neworderdata=[]
            const sellerinfo=orders[0].sellerinfo;
            for(let i=0;i<sellerinfo.length;i++){
                e=sellerinfo[i];
                //console.log("e.variation_id "+e.variation_id);return false;
                
                let varid= e.variation_id ? mongoose.Types.ObjectId(e.variation_id) : "";
                //console.log("e",e)
                const datatoadd=await VariationsPrice.aggregate([
                    {$match:{_id:varid}},
                    
                    {
                                $addFields:{
                                    phototoshow:{
                                        $cond:{
                                            if:{$gt:["$images",null]},
                                            then:{
                                                $cond:{
                                                    if:{$gt:[{$size:"$images"},0]},
                                                    // then:{$gt:[{$size:"$singlevariations.images"},0]},
                                                    // else:{$gt:[{$size:"$singlevariations.images"},0]}
                                                    then:"$images",
                                                    else:e?.product.product?.images
                                                }
                                            },
                                            // then:"$singlevariations.images",
                                            else:e?.product.product?.images
                                            }
                                        }
                                    // 
                                    // phototoshow:[]
                                }
                            },
                ])
                const seller=await sellers.findById(e.product.product?.provider_id)
                const address= seller?.pickupaddress;
                //console.log("address",address)
                //console.log("seller",seller)
                let phototoshowarray;
                let photo_array = [];
                if(datatoadd.length>0)
                {
                    phototoshowarray=datatoadd[0]?.phototoshow
                    // console.log("here 894 "+typeof(datatoadd[0]?.phototoshow));
                    // console.log("here 894 "+JSON.stringify(datatoadd[0]?.phototoshow));
                    if(datatoadd[0]?.phototoshow != "")
                    {
                        // console.log("ifff value");
                        // console.log("here 894 "+datatoadd[0]?.phototoshow[0]);
                        photo_array = datatoadd[0]?.phototoshow[0];
                    }
                    
                }else{
                    phototoshowarray=e?.product.product?.images
                }
                
                // let newobjectopush={e,phototoshow:phototoshowarray}

            //console.log("phototoshowarray "+ e?.product.product?.images);
                e["phototoshow"]=phototoshowarray;
                e["photo_array"] = photo_array;
                //e["photoToShowArray"]=phototoshowarray.split(",");
                e["address"]=address[0]
                // neworderdata.push(e)
            }
            orders["sellerinfo"]=neworderdata
            return res.send({
                status:true,
                data:orders
            })
        }catch(error)
        {
            return res.send({
                status:false,
                message:error,
                data:[]
            })
        }
    },

    get_order_detail_by_part_id:async(req,res)=>{
        try{
            const {id,part_id}=req.body;
            //console.log(id,part_id);return false;
            if(id == "" || id == null || id == "null" || part_id == "" || part_id == null || part_id == "null")
            {
                return res.send({
                    status:false,
                    message:"Id requis",
                    data:[],
                    all_info:[]
                })
            }
            //console.log("id "+ id);
            let objectid;
            if(id){
                objectid=mongoose.Types.ObjectId(id)
            }
            //console.log("objectid "+objectid);
            
            const orders = await ordersModel.aggregate([
                {$match:{_id:objectid}}
            ]);
            if(!orders)
            {
                return res.send({
                    status:false,
                    message:"Identifiant de commande invalide",
                    data:[],
                    all_info:[]
                })
            }else{
                //console.log("orders "+ JSON.stringify(orders));
                //console.log("orders "+ JSON.stringify(orders[0].products));
                var product_array = orders[0]?.products?.filter((val)=>{
                    
                    return val.part_id == part_id;
                });
                //console.log("here product_array "+ JSON.stringify(product_array));
                var res_order = {"_id":orders[0]?._id,"products":product_array}
                return res.send({
                    status:true,
                    message:"Success",
                    data:[res_order],
                    all_info:orders
                })
            }
            
        }catch(error)
        {
            console.log("error "+error);
            return res.send({
                status:false,
                message:error,
                data:[],
                all_info:[]
            })
        }
    },
    cancel_order_by_part_id:async(req,res)=>{
        try{
            //mongoose.set("debug",true);
            //console.log(req.body);return false;
            const {id,part_id}=req.body;
            let  provider_id = "";
            var product_array = [];
            //console.log(id,part_id);return false;
            if(id == "" || id == null || id == "null" || part_id == "" || part_id == null || part_id == "null")
            {
                return res.send({
                    status:false,
                    message:"Id requis",
                    data:[]
                })
            }
            let admin_record = await AdminModel.findOne({});
            //console.log("id "+ id);
            let objectid;
            if(id){
                objectid=mongoose.Types.ObjectId(id)
            }
            //console.log("objectid "+objectid);
            
            const orders = await ordersModel.aggregate([
                {$match:{_id:objectid}},
                
            ]);
            if(!orders)
            {
                return res.send({
                    status:false,
                    message:"Identifiant de commande invalide",
                    data:[]
                })
            }else{
                var seller_earning_error = false;
                var update_if = false;
                //console.log("orders "+ JSON.stringify(orders));
                //console.log("orders "+ JSON.stringify(orders[0].products));
                
                let sellerprofit = 0;
                let seller_totoal_income = 0;
                let chekc_condition2 = [];
                //provider_id
                //  console.log("order_cnancel_check2 ",order_cnancel_check2);
                //  return false;
                for(let x=0; x<orders[0]?.products.length; x++)
                {
                    //console.log("products ----> ", orders[0]?.products[x]);
                    let val = orders[0]?.products[x];
                    //console.log("val ----> ", val);
                    if(val.part_id == part_id )
                    {
                        //sellerprofit = val.sellerprofit;
                        provider_id = val.provider_id;
                        //let result_seller = await sellerModel.findOne({_id:provider_id},{"totalpendingamount":1});
                        //seller_totoal_income = result_seller.totalpendingamount;
                    }
                }
                //console.log("val ",val);
                //  return val;
                
                



                product_array = orders[0]?.products?.map((val)=>{
                    if(val.part_id == part_id)
                    {
                        // console.log("val sellerprofit ", val.sellerprofit);
                        // console.log("val provider_id ", val.provider_id);
                        if(val.delivery_status == 0)
                        {
                            // console.log("seller_totoal_income ",seller_totoal_income);
                            // console.log("val sellerprofit ", val.sellerprofit);
                            //return false;
                            val.status = 2;

                            // if(val.is_paid == false)
                            // {
                            //     seller_earning_error = true;
                            // }
                            // val.is_paid = false;
                            // if(seller_totoal_income <  val.sellerprofit)
                            // {
                            //     seller_earning_error = true;
                            // }
                            
                            update_if = true;
                        }
                    }
                    return val;
                });
                // if(seller_earning_error == true)
                // {
                //     return res.send({
                //         status:false,
                //         message:"Vous ne pouvez pas annuler la commande",
                //         data:[]
                //     });
                // }
                //return false;
                if(update_if == false)
                {
                    return res.send({
                        status:false,
                        message:"Il n'est plus possible d'annuler la commande",
                        data:[]
                    });
                }else{
                    // var update_array = orders[0]?.products?.map((val)=>{
                    //     //console.log("val "+val);
                    //     if(val.part_id == part_id)
                    //     {
                    //         //console.log("ifff here 487");
                    //         val.status = 1;
                    //     }
                    //     return val;
                    // });
                    //console.log("product_array"+product_array);
                    let datatoupdate= {
                        products:product_array,
                    };
                    //console.log(JSON.stringify(datatoupdate));
                    
                    // let seller_remainin_amount = seller_totoal_income - sellerprofit;
                    // //mongoose.set("debug",true);
                    // await sellerModel.updateOne({_id:provider_id},{totalpendingamount:seller_remainin_amount});

                    var res_update_q = await ordersModel.findByIdAndUpdate(id,datatoupdate);

                    

                    let message = "Commande annulée par l'utilisateur dont l'ID de commande est - "+part_id;
                    let data_save = {
                        to_id:provider_id,
                        user_type:"seller",
                        title:"Commande annulée",
                        message:message,
                        status: 'unread',
                        notification_type:"user_cancel_order"
                    }
                    await Notifications.create(data_save);
                    if(admin_record)
                    {
                        let admin_id = admin_record._id.toString();
                        let data_save_1 = {
                            to_id:admin_id,
                            user_type:"admin",
                            title:"Commande annulée",
                            message:message,
                            status: 'unread',
                            notification_type:"user_cancel_order"
                        }
                        await Notifications.create(data_save_1);

                    }

                    //console.log("res_update_q "+res_update_q);
                    if(res_update_q)
                    {
                        return res.send({
                            status:true,
                            message:"La commande a été annulée avec succès.",
                            data:[]
                        });
                    }else{
                        return res.send({
                            status:false,
                            message:"Une erreur est survenue",
                            data:[]
                        });
                    }
                }
                //console.log("here product_array "+ JSON.stringify(product_array));
                // var res_order = {"_id":orders[0]?._id,"products":product_array}
                // return res.send({
                //     status:true,
                //     message:"Success",
                //     data:[res_order]
                // })
                
                
            }
        }catch(error)
        {
            console.log("error "+error);
            return res.send({
                status:false,
                message:error,
                data:[]
            })
        }
    },
    return_order_by_part_id:async(req,res)=>{
        try{
            //mongoose.set("debug",true);
            const {id,part_id,returned_reason}=req.body;
            var product_array = [];
            //console.log(id,part_id);return false;
            if(id == "" || id == null || id == "null" || part_id == "" || part_id == null || part_id == "null")
            {
                return res.send({
                    status:false,
                    message:"Id requis",
                    data:[]
                })
            }
            //console.log("id "+ id);
            let objectid;
            if(id){
                objectid=mongoose.Types.ObjectId(id)
            }
            //console.log("objectid "+objectid);
            
            const orders = await ordersModel.aggregate([
                {$match:{_id:objectid}},
                
            ]);
            if(!orders)
            {
                return res.send({
                    status:false,
                    message:"Identifiant de commande invalide",
                    data:[]
                })
            }else{
                var update_if = false;
                //console.log("orders "+ JSON.stringify(orders));
                //console.log("orders "+ JSON.stringify(orders[0].products));
                product_array = orders[0]?.products?.map((val)=>{
                    //console.log("val "+val);
                    if(val.part_id == part_id)
                    {
                        if(val.delivery_status == 3)
                        {
                            val.status = 3;
                            val.returned_reason = returned_reason;
                            update_if = true;
                        }
                    }
                    return val;
                });
                if(update_if == false)
                {
                    return res.send({
                        status:false,
                        message:"Il n'est plus possible de renvoyer la commande",
                        data:[]
                    });
                }else{
                    // var update_array = orders[0]?.products?.map((val)=>{
                    //     //console.log("val "+val);
                    //     if(val.part_id == part_id)
                    //     {
                    //         //console.log("ifff here 487");
                    //         val.status = 1;
                    //     }
                    //     return val;
                    // });
                    //console.log("product_array"+product_array);
                    let datatoupdate= {
                        products:product_array,
                    };
                    //console.log(JSON.stringify(datatoupdate));
                    
                    var res_update_q = await ordersModel.findByIdAndUpdate(id,datatoupdate);
                    //console.log("res_update_q "+res_update_q);
                    if(res_update_q)
                    {
                        return res.send({
                            status:true,
                            message:"Envoi de votre demande de retour.",
                            data:[]
                        });
                    }else{
                        return res.send({
                            status:false,
                            message:"Une erreur est survenue",
                            data:[]
                        });
                    }
                }
                //console.log("here product_array "+ JSON.stringify(product_array));
                // var res_order = {"_id":orders[0]?._id,"products":product_array}
                // return res.send({
                //     status:true,
                //     message:"Success",
                //     data:[res_order]
                // })
                
                
            }
        }catch(error)
        {
            console.log("error "+error);
            return res.send({
                status:false,
                message:error,
                data:[]
            })
        }
    },
    get_order_detail_by_combo_id:async(req,res)=>{
        const id=req.params.id;
        console.log("id "+ id);
        // let objectid;
        // if(id){
        //     objectid=mongoose.Types.ObjectId(id)
        // }
        // console.log("objectid "+objectid);
        const orders=await ordersModel.aggregate([
            {$match:{combo_id:id}},
            {
                $addFields:{
                    userObjID:{"$toObjectId":"$userId"}
                }
            },
            {
                $lookup:{
                    from:"users",
                    foreignField:"_id",
                    localField:"userObjID",
                    as:"user"
                }
            },
           

            {$addFields:{
                "userobject":{$arrayElemAt:["$user",0]},
                "sellerearning":{
                    $subtract:["$totalPrice",{$add:["$totalcommission",{$add:["$totaltax",{$add:["$totalplusperproduct","$totalshipping"]}]}]}]
                },
                "singleprovider":{
                    arrayElemAt:["$provider",0]
                },
                "stringorderid":{"$toString":"$order_id"}
            }},
            {
                $addFields:{
                    // allproducts:"$products",
                    sellerinfo:{
                        $map:
                        {
                          input: "$products",
                          as: "product",
                          in: { 
                            "isreadytopickup":{
                                $cond:{
                                    if:{$gt:["$$product.readytopickup",null]},
                                    then:"$$product.readytopickup",
                                    else:false
                                }
                            },
                            // "part_id":{$concat: ["$stringorderid", {$concat:["-",{"$toString":{$add:[{$indexOfArray:["$products","$$product"]},1]}}]}]},
                            "price":"$$product.pricetouser",
                            "part_id":"$$product.part_id",
                            "delivery_status":"$$product.delivery_status",
                            "sellername":"$$product.provider_name",
                            "tax":"$$product.tax",
                            "shipping_cost":"$$product.shipping_cost",
                            "commission":"$$product.commission",
                            "sellerprofit":"$$product.sellerprofit",
                            "variation_id":"$$product.variation_id",
                            "readytopickup":"$$product.readytopickup",
                            "userfirstname":"$userobject.first_name",
                            "userlastname":"$userobject.last_name",
                            "product":"$$product",
                            "deliveryaddress":"$shippingaddress"
                           }
                        }
                    }
                }
            },
            {$project:{
                "order_id":1,
                "sellerinfo":1,
                "totalPrice":1,
                "sellerearning":1,
                "totalcommission":1,
                "totalshipping":1,
                "totaltax":1,
                "status":1,
                "createdAt":1,
                "status":1
            }}
        ]);
        //console.log("orders    ----->>>>>"+JSON.stringify(orders));
        //return false;
        const neworderdata=[]
        const sellerinfo=orders[0].sellerinfo;
        for(let i=0;i<sellerinfo.length;i++){
            e=sellerinfo[i];
            //console.log("e.variation_id "+e.variation_id);return false;
            
            let varid= e.variation_id ? mongoose.Types.ObjectId(e.variation_id) : "";
            //console.log("e",e)
            const datatoadd=await VariationsPrice.aggregate([
                {$match:{_id:varid}},
                
                {
                            $addFields:{
                                phototoshow:{
                                    $cond:{
                                        if:{$gt:["$images",null]},
                                        then:{
                                            $cond:{
                                                if:{$gt:[{$size:"$images"},0]},
                                                // then:{$gt:[{$size:"$singlevariations.images"},0]},
                                                // else:{$gt:[{$size:"$singlevariations.images"},0]}
                                                then:"$images",
                                                else:e?.product.product?.images
                                            }
                                        },
                                        // then:"$singlevariations.images",
                                        else:e?.product.product?.images
                                        }
                                    }
                                // 
                                // phototoshow:[]
                            }
                        },
            ])
            const seller=await sellers.findById(e.product.product?.provider_id)
            const address= seller?.pickupaddress;
            //console.log("address",address)
            //console.log("seller",seller)
            let phototoshowarray;
            let photo_array = [];
            if(datatoadd.length>0)
            {
                phototoshowarray=datatoadd[0]?.phototoshow
                // console.log("here 894 "+typeof(datatoadd[0]?.phototoshow));
                // console.log("here 894 "+JSON.stringify(datatoadd[0]?.phototoshow));
                if(datatoadd[0]?.phototoshow != "")
                {
                    // console.log("ifff value");
                    // console.log("here 894 "+datatoadd[0]?.phototoshow[0]);
                    photo_array = datatoadd[0]?.phototoshow[0];
                }
                
            }else{
                phototoshowarray=e?.product.product?.images
            }
            
            // let newobjectopush={e,phototoshow:phototoshowarray}

           //console.log("phototoshowarray "+ e?.product.product?.images);
            e["phototoshow"]=phototoshowarray;
            e["photo_array"] = photo_array;
            //e["photoToShowArray"]=phototoshowarray.split(",");
            e["address"]=address[0]
            // neworderdata.push(e)
        }
        orders["sellerinfo"]=neworderdata
        return res.send({
            status:true,
            data:orders
        })
    },
    getallordersbyselleridnew: async (req, res) => {
        const seller_id = req.body.id;
        let start_date = req.body.start_date;
        let end_date = req.body.end_date;
        const order_status=req.body.order_status;
        const delivery_status=req.body.delivery_status;
        const query={products:{$elemMatch:{"product.provider_id":seller_id}}}
        if(start_date && !end_date){
            let start_date_f=new Date(start_date);
            query["date_of_transaction"]={
                $gte:start_date_f
            }
        }
        else if(end_date && !start_date){
            let end_date_f=new Date(end_date);
            query["date_of_transaction"]={
                $lte:end_date_f
            }
        }else if(start_date && end_date){
            let start_date_f=new Date(start_date);
            let end_date_f=new Date(end_date);
            // query["$and"]=[
            //     {
            //         $gte:start_date_f
            //     },
            //     {
            //         $lte:end_date_f
            //     }
            // ]
            query["$and"] = [
                {date_of_transaction:{'$gte':start_date_f}},
                {date_of_transaction:{'$lte':end_date_f}}
            ]
        }
        if(order_status){
            query['status']=parseInt(order_status)
        }
        if(delivery_status){
            query['delivery_status']=parseInt(delivery_status)
        }
        console.log("query",query)
        const allorders = await orders.aggregate([
            {$match:query},
           
        ]);
        return res.send({
            status: true,
            message: "fetched successfully",
            data: allorders
        })
    },
     getallordersbysellerid: async (req, res) => {
        //mongoose.set('debug',true);
        const seller_id = req.body.id;
        let start_date = req.body.start_date;
        let end_date = req.body.end_date;
        const order_status=req.body.order_status;
        const delivery_status=req.body.delivery_status;
        var monthrange = {};
        const query={products:{$elemMatch:{"product.provider_id":seller_id}}}
        if(start_date && !end_date){
            let start_date_f=new Date(start_date);
            query["date_of_transaction"]={
                $gte:start_date_f
            }
        }
        else if(end_date && !start_date){
            let end_date_f=new Date(end_date);
            query["date_of_transaction"]={
                $lte:end_date_f
            }
        }else if(start_date && end_date){
            let start_date_f=new Date(start_date);
            let end_date_f=new Date(end_date);
            //,"status" : {$lte : 4 },"status" : {$gte : 0 }
            // query["date_of_transaction"]= {
            
            //     $and:[
            //     {date_of_transaction:{
            //     $gte:start_date_f
            //     }},
            //     {date_of_transaction:{
            //         $lte:end_date_f
            //     }}
            // ]};

            // {
            //     products: {
            //       '$elemMatch': { 'product.provider_id': '63f5c1ec5bf0bda586960971' }
            //     },
            //     date_of_transaction: { '$lte': 2023-03-05T00:00:00.000Z }
            // }
            // {
            //      '$and': [ { date_of_transaction: { '$gte': 2023-03-04T00:00:00.000Z } }, { date_of_transaction: { '$lte': 2023-03-05T00:00:00.000Z } } ] 
            // }

            query["$and"] = [
                {date_of_transaction:{'$gte':start_date_f}},
                {date_of_transaction:{'$lte':end_date_f}}
            ]
            // monthrange={
            
            //     $and:[
            //     {date_of_transaction:{
            //         $gte:start_date_f
            //     }},
            //     {date_of_transaction:{
            //         $lte:end_date_f
            //     }}
            // ]};
            
        }
        if(order_status){
            query['status']=parseInt(order_status)
        }
        if(delivery_status){
            query['delivery_status']=parseInt(delivery_status)
        }
        //console.log("query",query);return false;
        const allorders = await orders.aggregate([
            {$match:{payment_status: true}},
            {$match:query},
            {$match:monthrange},
            {
                $addFields:{
                    "sellerproducts":{"$map": {
                        "input": {
                          "$filter": {
                            "input": "$products",
                            "as": "product",
                            "cond": {$eq:["$$product.product.provider_id",seller_id]}
                          }
                        },
                        "as": "sellerproducts1",
                        "in": {
                          "products": "$$sellerproducts1",
                          
                        }}}
                }
            },
            {
                $project:{
                    "products":0
                }
            }
        ]);
        return res.send({
            status: true,
            message: "fetched successfully",
            data: allorders
        })
    },
     
    getCancelOrdersBySellerId: async (req, res) => {
       //mongoose.set('debug',true);
       const seller_id = req.body.id;
       let start_date = req.body.start_date;
       let end_date = req.body.end_date;
       const order_status=req.body.order_status;
       const delivery_status=req.body.delivery_status;
       var monthrange = {};
        //    const query=
        //    $and:[
            
        //    ]
        const query = {};
        query["$and"]=[
        {products:{$elemMatch:{"product.provider_id":seller_id}}},
        //{ products:{$elemMatch:{status:2}}}
        ] 
        //mongoose.set("debug",true);
       //console.log("query",query);return false;
       const allorders = await orders.aggregate([
           {$match:{payment_status: true}},
           {$match:query},
           
           {$match:monthrange},
           {
               $addFields:{
                   "sellerproducts":{"$map": {
                       "input": {
                         "$filter": {
                           "input": "$products",
                           "as": "product",
                           "cond": {      
                            $and:[
                                     {$eq:["$$product.product.provider_id",seller_id]},
                                     {$eq:["$$product.status",2]},
                                        // {"$$product.is_paid":{$gt:null,$eq:true}}
                                ]
                            }
                                
                                    
                                   
                                    
                            
                         }
                       },
                       "as": "sellerproducts1",
                       "in": {
                         "products": "$$sellerproducts1",
                         
                       }}}
               }
           },
           {
               $project:{
                   "products":0
               }
           }
       ]);
       let new_order = allorders.filter((val)=>{
            //console.log(val.sellerproducts);
            if(val.sellerproducts.length > 0)
            {
                return val;
            }
       })
       return res.send({
           status: true,
           message: "Toutes les commandes annulées",
           data: new_order
       })
    },

    getCompletedOrdersBySellerId: async (req, res) => 
   {
        //mongoose.set('debug',true);
        const seller_id = req.body.id;
        let start_date = req.body.start_date;
        let end_date = req.body.end_date;
        const order_status=req.body.order_status;
        const delivery_status=req.body.delivery_status;
        var monthrange = {};
        //    const query=
        //    $and:[
            
        //    ]
        const query = {};
        query["$and"]=[
        {products:{$elemMatch:{"product.provider_id":seller_id}}},
        //{ products:{$elemMatch:{status:2}}}
        ] 
        //mongoose.set("debug",true);
        //console.log("query",query);return false;
        const allorders = await orders.aggregate([
            {$match:{payment_status: true}},
            {$match:query},
            
            {$match:monthrange},
            {
                $addFields:{
                    "sellerproducts":{"$map": {
                        "input": {
                        "$filter": {
                            "input": "$products",
                            "as": "product",
                            "cond": {      
                            $and:[
                                    {$eq:["$$product.product.provider_id",seller_id]},
                                    {$eq:["$$product.status",1]},
                                        // {"$$product.is_paid":{$gt:null,$eq:true}}
                                ]
                            }
                        }
                        },
                        "as": "sellerproducts1",
                        "in": {
                        "products": "$$sellerproducts1",
                        
                        }}}
                }
            },
            {
                $project:{
                    "products":0
                }
            }
        ]);
        let new_order = allorders.filter((val)=>{
            //console.log(val.sellerproducts);
            if(val.sellerproducts.length > 0)
            {
                return val;
            }
        })
        return res.send({
            status: true,
            message: "Toutes les commandes annulées",
            data: new_order
        })
    },
    getProcessOrdersBySellerId: async (req, res) => 
    {
         //mongoose.set('debug',true);
         const seller_id = req.body.id;
         let start_date = req.body.start_date;
         let end_date = req.body.end_date;
         const order_status=req.body.order_status;
         const delivery_status=req.body.delivery_status;
         var monthrange = {};
         //    const query=
         //    $and:[
             
         //    ]
         const query = {};
         query["$and"]=[
         {products:{$elemMatch:{"product.provider_id":seller_id}}},
         //{ products:{$elemMatch:{status:2}}}
         ] 
         //mongoose.set("debug",true);
         //console.log("query",query);return false;
         const allorders = await orders.aggregate([
             {$match:{payment_status: true}},
             {$match:query},
             
             {$match:monthrange},
             {
                 $addFields:{
                     "sellerproducts":{"$map": {
                         "input": {
                         "$filter": {
                             "input": "$products",
                             "as": "product",
                             "cond": {      
                             $and:[
                                     {$eq:["$$product.product.provider_id",seller_id]},
                                     {$eq:["$$product.status",0]},
                                         // {"$$product.is_paid":{$gt:null,$eq:true}}
                                 ]
                             }
                         }
                         },
                         "as": "sellerproducts1",
                         "in": {
                         "products": "$$sellerproducts1",
                         
                         }}}
                 }
             },
             {
                 $project:{
                     "products":0
                 }
             }
         ]);
         let new_order = allorders.filter((val)=>{
             //console.log(val.sellerproducts);
             if(val.sellerproducts.length > 0)
             {
                 return val;
             }
         })
         return res.send({
             status: true,
             message: "Toutes les commandes annulées",
             data: new_order
         })
     },
    getallordersbyadmin: async (req, res) => {
        const delivery_status=req.body.delivery_status;
        const order_status=req.body.order_status;
        const customer_id=req.body.customer_id;
        const pageno=req.body.pageno;
        const limit=20;
        const skip=limit*pageno
       
        let start_date = req.body.start_date;
        let end_date = req.body.end_date;
        const query={payment_status: true};
        if(delivery_status){
            query['delivery_status']=parseInt(delivery_status)
        }
         if(customer_id){
            query['userId']=customer_id
        }
       
        // const query={products:{$elemMatch:{"product.provider_id":seller_id}}}
        if(start_date && !end_date){
            let start_date_f=new Date(start_date);
            query["date_of_transaction"]={
                $gte:start_date_f
            }
        }
        else if(end_date && !start_date){
            let end_date_f=new Date(end_date);
            query["date_of_transaction"]={
                $lte:end_date_f
            }
        }else if(start_date && end_date){
            let start_date_f=new Date(start_date);
            let end_date_f=new Date(end_date);
            // query["$and"]=[
            //     {
            //         date_of_transaction:{$gte:start_date_f}
            //     },
            //     {
            //         date_of_transaction:{$lte:end_date_f}
            //     }
            // ]
            query["$and"] = [
                {date_of_transaction:{'$gte':start_date_f}},
                {date_of_transaction:{'$lte':end_date_f}}
            ]
        }
        if(order_status){
            query['status']=parseInt(order_status)
        }
        console.log("query",query)
       
        if(pageno==null){
            const count = (await orders.aggregate([
                {$match:query},
                {
                    $addFields:{
                        userIDOBJ:{"$toObjectId":"$userId"},
                        providersArray:{
                            $map: {
                                input: "$products",
                                as: "product",
                                in: {"provider_id":{"$toString":"$$product.product.provider_id"}}
                              }
                        }
                    }
                },
                {
                    $lookup:{
                        from:"users",
                        localField:"userIDOBJ",
                        foreignField:"_id",
                        as:"customer"
                    }
                },
                 {
                    $lookup:{
                        from:"sellers",
                        pipeline:[
                            {
                                $match:{
                                    "providersArray":{
                                        $elemMatch:{
                                            "provider_id":"$_id"
                                        }
                                    }
                                }
                                // $match:{
                                //     "_id":{$in:"$providersArray"}
                                // }
                            }
                        ],
                        as:"vendors"
                    }
                }
            ]))?.length;
            const pages=Math.ceil(count/limit)
            return res.send({
                status:true,
                pages:pages
            })
        }
        const allorders = await orders.aggregate([
            {$match:query},
            {
                $addFields:{
                    userIDOBJ:{"$toObjectId":"$userId"},
                    providersArray:{
                        $map: {
                            input: "$products",
                            as: "product",
                            in: {"provider_id":{"$toString":"$$product.product.provider_id"}}
                          }
                    }
                }
            },
            {
                $lookup:{
                    from:"users",
                    localField:"userIDOBJ",
                    foreignField:"_id",
                    as:"customer"
                }
            },
             {
                $lookup:{
                    from:"sellers",
                    pipeline:[
                        {
                            $match:{
                                "providersArray":{
                                    $elemMatch:{
                                        "provider_id":"$_id"
                                    }
                                }
                            }
                            // $match:{
                            //     "_id":{$in:"$providersArray"}
                            // }
                        }
                    ],
                    as:"vendors"
                }
            },
            {
                $sort:{
                    "createdAt":-1
                }
            },
            {$skip:skip},
            {$limit:limit}
        ]);
        if(allorders.length > 0)
        {
            return res.send({
                status: true,
                message: "fetched successfully",
                data: allorders,
                pageno:skip
                
            })
        }else{
            return res.send({
                status: false,
                message: "fetched successfully",
                data: [],
                pageno:0
                
            })
        }
        
    },

    getAllProcessingOrdersByAdminCount: async (req, res) => {
        //{awards: {$elemMatch: {award:'National Medal', year:1975}}}
        //const pages = Math.ceil(count/limit)
        let limit = 20;
        const allorders = await orders.count({
                
                payment_status:true,
                products: {
                    $elemMatch: {
                        delivery_status:0
                    }
                }
                
            });
            console.log("latest count "+allorders);
        const pages = Math.ceil(allorders/limit)
        return res.send({
            status: true,
            message: "fetched successfully",
            pages: pages
        })
    },
    getAllProcessingOrdersByAdmin: async (req, res) => {
        //{awards: {$elemMatch: {award:'National Medal', year:1975}}}
        //const pages = Math.ceil(count/limit)
        const pageno=req.body.pageno;
        const limitt=20;
        const skipp=limitt*pageno;
        const query={payment_status: true};
        const allorders = await orders.aggregate([
            {$match:query},
            {
                $match:{
                    products: {
                        $elemMatch: {
                            delivery_status:0
                        }
                    }
                }
            },
            {
                $addFields:{
                    userIDOBJ:{"$toObjectId":"$userId"},
                    providersArray:{
                        $map: {
                            input: "$products",
                            as: "product",
                            in: {"provider_id":{"$toString":"$$product.product.provider_id"}}
                          }
                    }
                }
            },
            {
                $lookup:{
                    from:"users",
                    localField:"userIDOBJ",
                    foreignField:"_id",
                    as:"customer"
                }
            },
             {
                $lookup:{
                    from:"sellers",
                    pipeline:[
                        {
                            $match:{
                                "providersArray":{
                                    $elemMatch:{
                                        "provider_id":"$_id"
                                    }
                                }
                            }
                            // $match:{
                            //     "_id":{$in:"$providersArray"}
                            // }
                        }
                    ],
                    as:"vendors"
                }
            },
            {
                $sort:{
                    "createdAt":-1
                }
            },
            {$skip:skipp},
            {$limit:limitt}
        ]);
            if(allorders.length > 0)
            {
                return res.send({
                    status: true,
                    message: "fetched successfully",
                    data: allorders,
                    pageno:skipp
                    
                })
            }else{
                return res.send({
                    status: false,
                    message: "fetched successfully",
                    data: [],
                    pageno:0
                    
                })
            }
    },
    getAllRadyToPickUpByAdminCount: async (req, res) => {
        //{awards: {$elemMatch: {award:'National Medal', year:1975}}}
        //const pages = Math.ceil(count/limit)
        let limit = 20;
        const allorders = await orders.count({
                
                payment_status:true,
                products: {
                    $elemMatch: {
                        delivery_status:0,
                        is_pickedup:true
                    }
                }
                
            });
            console.log("latest count "+allorders);
        const pages = Math.ceil(allorders/limit)
        return res.send({
            status: true,
            message: "fetched successfully",
            pages: pages
        })
    },
    getAllRadyToPickUpByAdmin: async (req, res) => {
        //{awards: {$elemMatch: {award:'National Medal', year:1975}}}
        //const pages = Math.ceil(count/limit)
        const pageno=req.body.pageno;
        const limitt=20;
        const skipp=limitt*pageno;
        const query={payment_status: true};
        const allorders = await orders.aggregate([
            {$match:query},
            {
                $match:{
                    products: {
                        $elemMatch: {
                            delivery_status:0
                        }
                    }
                }
            },
            {
                $match:{
                    products: {
                        $elemMatch: {
                            is_pickedup:true
                        }
                    }
                }
            },
            {
                $addFields:{
                    userIDOBJ:{"$toObjectId":"$userId"},
                    providersArray:{
                        $map: {
                            input: "$products",
                            as: "product",
                            in: {"provider_id":{"$toString":"$$product.product.provider_id"}}
                          }
                    }
                }
            },
            {
                $lookup:{
                    from:"users",
                    localField:"userIDOBJ",
                    foreignField:"_id",
                    as:"customer"
                }
            },
             {
                $lookup:{
                    from:"sellers",
                    pipeline:[
                        {
                            $match:{
                                "providersArray":{
                                    $elemMatch:{
                                        "provider_id":"$_id"
                                    }
                                }
                            }
                            // $match:{
                            //     "_id":{$in:"$providersArray"}
                            // }
                        }
                    ],
                    as:"vendors"
                }
            },
            {
                $sort:{
                    "createdAt":-1
                }
            },
            {$skip:skipp},
            {$limit:limitt}
        ]);
            if(allorders.length > 0)
            {
                return res.send({
                    status: true,
                    message: "fetched successfully",
                    data: allorders,
                    pageno:skipp
                    
                })
            }else{
                return res.send({
                    status: false,
                    message: "fetched successfully",
                    data: [],
                    pageno:0
                    
                })
            }
    },
    
    getAllPickedUpByAdminCount: async (req, res) => {
        //{awards: {$elemMatch: {award:'National Medal', year:1975}}}
        //const pages = Math.ceil(count/limit)
        let limit = 20;
        const allorders = await orders.count({
                
                payment_status:true,
                products: {
                    $elemMatch: {
                        delivery_status:1
                    }
                }
                
            });
            console.log("latest count "+allorders);
        const pages = Math.ceil(allorders/limit)
        return res.send({
            status: true,
            message: "fetched successfully",
            pages: pages
        })
    },
    getAllPickedUpByAdmin: async (req, res) => {
        //{awards: {$elemMatch: {award:'National Medal', year:1975}}}
        //const pages = Math.ceil(count/limit)
        const pageno=req.body.pageno;
        const limitt=20;
        const skipp=limitt*pageno;
        const query={payment_status: true};
        const allorders = await orders.aggregate([
            {$match:query},
            {
                $match:{
                    products: {
                        $elemMatch: {
                            delivery_status:1
                        }
                    }
                }
            },
            {
                $addFields:{
                    userIDOBJ:{"$toObjectId":"$userId"},
                    providersArray:{
                        $map: {
                            input: "$products",
                            as: "product",
                            in: {"provider_id":{"$toString":"$$product.product.provider_id"}}
                          }
                    }
                }
            },
            {
                $lookup:{
                    from:"users",
                    localField:"userIDOBJ",
                    foreignField:"_id",
                    as:"customer"
                }
            },
             {
                $lookup:{
                    from:"sellers",
                    pipeline:[
                        {
                            $match:{
                                "providersArray":{
                                    $elemMatch:{
                                        "provider_id":"$_id"
                                    }
                                }
                            }
                            // $match:{
                            //     "_id":{$in:"$providersArray"}
                            // }
                        }
                    ],
                    as:"vendors"
                }
            },
            {
                $sort:{
                    "createdAt":-1
                }
            },
            {$skip:skipp},
            {$limit:limitt}
        ]);
            if(allorders.length > 0)
            {
                return res.send({
                    status: true,
                    message: "fetched successfully",
                    data: allorders,
                    pageno:skipp
                    
                })
            }else{
                return res.send({
                    status: false,
                    message: "fetched successfully",
                    data: [],
                    pageno:0
                    
                })
            }
    },
    getAllCompletedByAdminCount: async (req, res) => {
        //{awards: {$elemMatch: {award:'National Medal', year:1975}}}
        //const pages = Math.ceil(count/limit)
        let limit = 20;
        const allorders = await orders.count({
                
                payment_status:true,
                products: {
                    $elemMatch: {
                        status:1
                    }
                }
                
            });
           // console.log("latest count "+allorders);
        const pages = Math.ceil(allorders/limit)
        return res.send({
            status: true,
            message: "fetched successfully",
            pages: pages
        })
    },
    getAllCompletedByAdmin: async (req, res) => {
        //{awards: {$elemMatch: {award:'National Medal', year:1975}}}
        //const pages = Math.ceil(count/limit)
        const pageno=req.body.pageno;
        const limitt=20;
        const skipp=limitt*pageno;
        const query={payment_status: true};
        const allorders = await orders.aggregate([
            {$match:query},
            {
                $match:{
                    products: {
                        $elemMatch: {
                            status:1
                        }
                    }
                }
            },
            {
                $addFields:{
                    userIDOBJ:{"$toObjectId":"$userId"},
                    providersArray:{
                        $map: {
                            input: "$products",
                            as: "product",
                            in: {"provider_id":{"$toString":"$$product.product.provider_id"}}
                          }
                    }
                }
            },
            {
                $lookup:{
                    from:"users",
                    localField:"userIDOBJ",
                    foreignField:"_id",
                    as:"customer"
                }
            },
             {
                $lookup:{
                    from:"sellers",
                    pipeline:[
                        {
                            $match:{
                                "providersArray":{
                                    $elemMatch:{
                                        "provider_id":"$_id"
                                    }
                                }
                            }
                            // $match:{
                            //     "_id":{$in:"$providersArray"}
                            // }
                        }
                    ],
                    as:"vendors"
                }
            },
            {
                $sort:{
                    "createdAt":-1
                }
            },
            {$skip:skipp},
            {$limit:limitt}
        ]);
            if(allorders.length > 0)
            {
                return res.send({
                    status: true,
                    message: "fetched successfully",
                    data: allorders,
                    pageno:skipp
                    
                })
            }else{
                return res.send({
                    status: false,
                    message: "fetched successfully",
                    data: [],
                    pageno:0
                    
                })
            }
    },
    getAllUserCancledByAdminCount: async (req, res) => {
        //{awards: {$elemMatch: {award:'National Medal', year:1975}}}
        //const pages = Math.ceil(count/limit)
        let limit = 20;
        const allorders = await orders.count({
                
                payment_status:true,
                products: {
                    $elemMatch: {
                        status:2
                    }
                }
                
            });
           // console.log("latest count "+allorders);
        const pages = Math.ceil(allorders/limit)
        return res.send({
            status: true,
            message: "fetched successfully",
            pages: pages
        })
    },
    getAllUserCancledByAdmin: async (req, res) => {
        //{awards: {$elemMatch: {award:'National Medal', year:1975}}}
        //const pages = Math.ceil(count/limit)
        const pageno=req.body.pageno;
        const limitt=20;
        const skipp=limitt*pageno;
        const query={payment_status: true};
        const allorders = await orders.aggregate([
            {$match:query},
            {
                $match:{
                    products: {
                        $elemMatch: {
                            status:2
                        }
                    }
                }
            },
            {
                $addFields:{
                    userIDOBJ:{"$toObjectId":"$userId"},
                    providersArray:{
                        $map: {
                            input: "$products",
                            as: "product",
                            in: {"provider_id":{"$toString":"$$product.product.provider_id"}}
                          }
                    }
                }
            },
            {
                $lookup:{
                    from:"users",
                    localField:"userIDOBJ",
                    foreignField:"_id",
                    as:"customer"
                }
            },
             {
                $lookup:{
                    from:"sellers",
                    pipeline:[
                        {
                            $match:{
                                "providersArray":{
                                    $elemMatch:{
                                        "provider_id":"$_id"
                                    }
                                }
                            }
                            // $match:{
                            //     "_id":{$in:"$providersArray"}
                            // }
                        }
                    ],
                    as:"vendors"
                }
            },
            {
                $sort:{
                    "createdAt":-1
                }
            },
            {$skip:skipp},
            {$limit:limitt}
        ]);
            if(allorders.length > 0)
            {
                return res.send({
                    status: true,
                    message: "fetched successfully",
                    data: allorders,
                    pageno:skipp
                    
                })
            }else{
                return res.send({
                    status: false,
                    message: "fetched successfully",
                    data: [],
                    pageno:0
                    
                })
            }
    },
    getorderbyid: async (req, res) => {
        const id = req.params.id;
        const order = await orders.findById(id);
        return res.send({
            status: true,
            message: "fetched successfully",
            data: order
        })
    },
    saveratingandreview: async (req, res) => {
        const user_id = req.body.user_id;
        const user_name=req.body.user_name;
        const type = req.body.type;
        const order_id = req.body.order_id;
        const product_id = req.body.product_id;
        const rating = req.body.rating;
        const review = req.body.review;
        const provider_id=req.body.provider_id;
        const provider_name=req.body.provider_name;
        const photos=[];
        if(req.files)
        {
            if(req.files.photo){
                let photoname
                req.files.photo?.map((photo)=>{
                    photoname="products/"+photo.filename;
                    photos.push(photoname)
                })
            }
        }
        
        const order = await orders.findById(order_id);
        //if (order.status == 1) {
            const isproductthere = order?.products?.filter((elem) => elem.product._id == product_id);
            //console.log("isproductthere",isproductthere,"order?.products",order?.products)
            if(isproductthere)
            {
                if (isproductthere.length) {
                    const alreadyexists = await Ratingnreviews.findOne({ product_id: product_id, order_id: order_id });
                    
                    //console.log("alreadyexists ", alreadyexists );

                    if (alreadyexists) {
                        // const datatoupdate={
                        //     ...(rating&&{rating:rating}),
                        //     ...(review&&{review:review}),
                        //     ...(photos.length&&{media:photos})
                        // }
                        // await Ratingnreviews.findByIdAndUpdate(alreadyexists._id,datatoupdate)
                        // .then((result)=>{
                        //     return res.send({
                        //         status: false,
                        //         message: "rating updated successfully",
                        //         data: null,
                        //         errmessage: ""
                        //     });
                        // })
                        return res.send({
                            status: false,
                            message: "Vous pouvez déjà évaluer ce produit pour cette commande",
                            data: null,
                            errmessage: ""
                        });
                    }
                    else {
                        // console.log("isproductthere 1056 "+isproductthere);
                        // console.log("isproductthere.delivery_status "+isproductthere[0].delivery_status);
                        //return false;
                        if(isproductthere[0].delivery_status == 3 )
                        {
                            const newrating = new Ratingnreviews();
                            newrating.product_id = product_id;
                            newrating.user_id = user_id;
                            newrating.provider_id = provider_id;
                            newrating.user_name = user_name;
                            newrating.provider_name = provider_name;
                            newrating.order_id = order_id;
                            newrating.rating = rating;
                            newrating.review = review;
                            newrating.media  = photos;
                            newrating.save().then((rating) => {
    
                            return res.send({
                                    status: true,
                                    message: "La révision a été sauvegardée avec succès",
                                    data: rating,
                                    errmessage: "",
                                });
    
                            });
                        }else{
                            return res.send({
                                        status:false,
                                        message:"La commande n'est pas encore terminée, attendez qu'elle le soit."
                                    })
                        }
                        
                    }
                }else{
                    return res.send({
                        status:false,
                        message:"Il n'y a pas de produit de ce type dans cette commande"
                    })
                }
            }else{
                return res.send({
                    status:false,
                    message:"Il n'y a pas de produit de ce type dans cette commande"
                })
            }
            
        // }else{
        //     return res.send({
        //         status:false,
        //         message:"order is not completed yet, wait for it to get completed"
        //     })
        // }
    },
    getratingandreviewbyproductid: async (req, res) => {
        const product_id = req.params.product_id;
        const rating= await Ratingnreviews.aggregate([
            {$match:{ product_id: product_id }},
            {$addFields:{
                productIDOBJ:{"$toObjectId":"$product_id"},
                orderIDOBJ:{"$toObjectId":"$order_id"},
                userIDOBJ:{"$toObjectId":"$user_id"}

            }},
            {$lookup:{
                from:"products",
                localField:"productIDOBJ",
                foreignField:"_id",
                as:"product"
            }},
            {$lookup:{
                from:"orders",
                localField:"orderIDOBJ",
                foreignField:"_id",
                as:"order"
            }},
            
            {$lookup:{
                from:"users",
                localField:"userIDOBJ",
                foreignField:"_id",
                as:"user"
            }}
        ]);
        return res.send({
            status: true,
            message: "La revue a été récupérée avec succès",
            data: rating,
            errmessage: "",
        });
    },
    getratingandreviewbyuserid: async (req, res) => {
        const user_id = req.params.user_id;
        const rating =await Ratingnreviews.aggregate([
            {$match:{ user_id: user_id }},
            {$addFields:{
                productIDOBJ:{"$toObjectId":"$product_id"},
                orderIDOBJ:{"$toObjectId":"$order_id"}
            }},
            {$lookup:{
                from:"products",
                localField:"productIDOBJ",
                foreignField:"_id",
                as:"product"
            }},
            {$lookup:{
                from:"orders",
                localField:"orderIDOBJ",
                foreignField:"_id",
                as:"order"
            }}
        ]);
        return res.send({
            status: true,
            message: "Les coordonnées bancaires ont été sauvegardées avec succès",
            data: rating,
            errmessage: "",

        });
    },
    deleteratingandreview: async(req, res) => {
        const rating_id = req.params.review_id;
      await  Ratingnreviews.findByIdAndRemove(rating_id).then((rating) => {
            return res.send({
                status: true,
                message: "La révision a été supprimée avec succès",
                data: rating,
                errmessage: "",
            });
        }).catch((err) => {
            return res.send({
                status: false,
                message: "",
                data: null,
                errmessage: "Erreur dans la récupération des avis ",
            });
        })
    },
    markOderasUnProcessed:async(req,res)=>{
        const id = req.params.id;
         await orders.findByIdAndUpdate(id,{
            status:0
        }).then((result)=>{
            return res.send({
                status:true,
                message:"marked as unprocessed"
            })
        });
    },

    //shipping status 0 for processing,1 shipped,2 completed,3 canclelled
    markOderasShippedforshipping:async(req,res)=>{
        const id = req.params.id;
         await orders.findByIdAndUpdate(id,{
            delivery_status:1
        }).then((result)=>{
            return res.send({
                status:true,
                message:"marked as unprocessed"
            })
        });
    },
    markOderasCompletedforshipping:async(req,res)=>{
        const id = req.params.id;
         await orders.findByIdAndUpdate(id,{
            delivery_status:2
        }).then((result)=>{
            return res.send({
                status:true,
                message:"marked as unprocessed"
            })
        });
    },
    markOderasCancelledforshipping:async(req,res)=>{
        const id = req.params.id;
        const order=await orders.findById(id);
        if(order.delivery_status==0){
            order.delivery_status=3
            order.save((result)=>{
                return res.send({
                    status:true,
                    message:"marked as unprocessed"
                })
            })
        }else{
            return res.send({
                status:false,
                message:"La commande est en cours de traitement et ne peut donc pas être annuléemarked as unprocessed"
            })
        }
    },
    markOderasReadytopickup:async(req,res)=>{
        
        
        const id = req.body.order_id;
        const provider_id=req.body.provider_id;
        const product_id=req.body.product_id;
        const order=await orders.findById(id);
        let part_id = "";
        //console.log(req.body);
        //console.log("order "+order);
        let admin_record = await AdminModel.findOne({});
        //status
        if(!order)
        {
            return res.send({
                status:false,
                message:"ID de commande invalide"
            })
        }
        if(order.status==0)
        {
            var is_update = false;
            const productsdatatoupdate=order.products.map((e)=>{
                console.log("hereeee 1472");
                console.log("e.provider_id ", e.part_id);
                console.log("e.provider_id ", e.provider_id);
                console.log("provider_id ", provider_id);
                console.log("e.e.product._id ", e.product._id.toString());
                console.log("product_id ", product_id);
                   
                if(e.provider_id==provider_id&&e.product._id.toString()==product_id)
                {
                    if(e.status == 0)
                    {
                        part_id = e.part_id;
                        e.readytopickup=true;
                        is_update = true;
                    }
                    
                }
                return e;
            })
            // let neworder=[...order];
            // neworder.products=productsdatatoupdate;

            //return false;

            if(is_update == true)
            {
                if(admin_record)
                {
                    let message = "L'enlèvement de la commande est prêt auprès du vendeur dont l'ID de commande est - "+part_id;
                    let admin_id = admin_record._id.toString();
                    let data_save_1 = {
                        to_id:admin_id,
                        user_type:"admin",
                        title:"Enlèvement prêt",
                        message:message,
                        status: 'unread',
                        notification_type:"pick_ready_order"
                    }
                    await Notifications.create(data_save_1);

                }

                //console.log("productsdatatoupdate ",productsdatatoupdate);
                //return false;
                await orders.findByIdAndUpdate(id,{products:productsdatatoupdate});
                // await updatearrayelement(orders,order,"products",provider_id,"provider_id",id)
                return res.send({
                    status:true,
                    message:"La commande est prête à être enlevée"
                })
            }else{
                return res.send({
                    status:false,
                    message:"La commande est en cours de traitement et ne peut donc pas être annulée"
                })
            }            
        }else{
            return res.send({
                status:false,
                message:"La commande est en cours de traitement et ne peut donc pas être annulée"
            })
        }
    },

    updateorderstatus:async(req,res)=>{
        
        
        const {
        id ,
        part_id,
        updatetype,        
        order_status,
        deli_status,
        typeOfDelivery
        }=req.body
        // const order=await orders.findById(id);
        // if(updatetype=="all"){
        //     const productsdatatoupdate=order.products.map((e)=>{
        //         // console.log(e.provider_id==provider_id&&e.product._id+''==product_id)
        //         e.delivery_status=true
        //         return e
        //     })
        //     // let neworder=[...order];
        //     // neworder.products=productsdatatoupdate;
        //     let datatoupdate= {
        //         products:productsdatatoupdate,
        //         ...(order_status?.toString()&&{status:order_status})
        //     }
        //     await orders.findByIdAndUpdate(id,datatoupdate);
        // }else{
        //     const productsdatatoupdate=order.products.map((e)=>{
        //         // console.log(e.provider_id==provider_id&&e.product._id+''==product_id)
        //         if(e.part_id==part_id){
        //             e.delivery_status=true
        //         }
        //         return e
        //     })
        //     // let neworder=[...order];
        //     // neworder.products=productsdatatoupdate;
        //     let datatoupdate= {
        //         products:productsdatatoupdate,
        //         ...(order_status?.toString()&&{status:order_status})
        //     }
        //     await orders.findByIdAndUpdate(id,datatoupdate);
        // }
        
        const order=await orders.findById(id);
           
                const productsdatatoupdate=order.products.map((e)=>
                {
                    console.log("e.part_id",e.part_id,"e.delivery_status",e.delivery_status)
                  if(typeOfDelivery == 'Delivery')
                  {
                    // console.log(e.provider_id==provider_id&&e.product._id+''==product_id)
        
                    if(e.part_id==part_id && deli_status==3)
                    {
                      if(e.delivery_status == 1)
                      {
                        e.delivery_status=deli_status
                      }
                      
                    }     
                    if(e.part_id==part_id && deli_status!=3)
                    {
                        e.delivery_status=deli_status
                     
                    }     
                    return e       
                  }else{
                    // console.log(e.provider_id==provider_id&&e.product._id+''==product_id)
                    if(e.part_id==part_id)
                    {
                      
                        e.delivery_status=deli_status
                    }
                    return e
                  }
                    
                })
                // let neworder=[...order];
                // neworder.products=productsdatatoupdate;
                let datatoupdate= {
                    products:productsdatatoupdate,
                    ...(order_status?.toString()&&{status:order_status})
                }
                await orders.findByIdAndUpdate(id,datatoupdate);
            
            // await updatearrayelement(orders,order,"products",provider_id,"provider_id",id)
            // return true;
        // await updatearrayelement(orders,order,"products",provider_id,"provider_id",id)
        return res.send({
            status:true,
            message:"marked as unprocessed"
        })
    },
    markOderaspickupup:async(req,res)=>{
        
        
        const id = req.body.order_id;
        const provider_id=req.body.provider_id;
        const product_id=req.body.product_id;
        const order=await orders.findById(id);
        if(order.status==0){
            const productsdatatoupdate=order.products.map((e)=>{
                console.log(e.provider_id==provider_id&&e.product._id+''==product_id)
                if(e.provider_id==provider_id&&e.product._id+''==product_id){
                    e.is_pickedup=true
                }
                return e
            })
            // let neworder=[...order];
            // neworder.products=productsdatatoupdate;
            await orders.findByIdAndUpdate(id,{products:productsdatatoupdate});
            // await updatearrayelement(orders,order,"products",provider_id,"provider_id",id)
            return res.send({
                status:true,
                message:"marked as unprocessed"
            })
        }else{
            return res.send({
                status:false,
                message:"La commande est en cours de traitement et ne peut donc pas être annuléemarked as unprocessed"
            })
        }
    },
    //for order status 0 for processing,1 completed,2 canclelled
    markOderasCompleted:async(req,res)=>{
        const id = req.params.id;
        const order=await orders.findById(id);
        if(order.status==1){
            return res.send({
                status:false,
                message:"already marked as done"
            })
        }
        order.status=1
        const products=order.products;
        let revenuetoadd=0;
        order.save()
        // await Promise.all(products?.map(async(product)=>{
        //     revenuetoadd+=product.sellerprofit
        //     const sellerdoc=await seller.findById(product.product.provider_id);
        //     sellerdoc.totalpendingamount=sellerdoc.totalpendingamount+revenuetoadd;
           
        //     sellerdoc.save()
        // })).then((result)=>{
        //     console.log("result",result);
        //     return res.send({
        //         status:true,
        //         message:"result",

        //     })
        // })
         
    },
    markOderasCancelled:async(req,res)=>{
        const id = req.params.id;
        const order=await orders.findById(id);
        if(order.status==0){
            order.status=2
            order.save((result)=>{
                return res.send({
                    status:true,
                    message:"Marqué comme annulé"
                })
            })
        }else{
            return res.send({
                status:false,
                message:"La commande est en cours de traitement et ne peut donc pas être annulée"
            })
        }
    },
    // order status methods end here
    //for order return 
    markOderasReturned:async(req,res)=>{
        const id = req.params.id;
        const order=await orders.findById(id);
        if(order.status==1){
            order.status=3
            order.save((result)=>{
                return res.send({
                    status:true,
                    message:"Marqué comme retourné"
                })
            })
        }else{
            return res.send({
                status:false,
                message:"La commande est en cours de traitement et ne peut donc pas être annulée"
            })
        }
    },
    markOderasRefunded:async(req,res)=>{
        const id = req.params.id;
        const order=await orders.findById(id);
        if(order.status==5){
            order.status=6
            order.save((result)=>{
                return res.send({
                    status:true,
                    message:"Marqué comme remboursé"
                })
            })
        }else{
            return res.send({
                status:false,
                message:"La commande est en cours de traitement et ne peut donc pas être annulée"
            })
        }
    },
    markDeliveryasPending:async(req,res)=>{
        const id = req.params.id;
        await orders.findByIdAndUpdate(id,{
            delivery_status:0
       }).then((result)=>{
           return res.send({
               status:true,
               message:"Marqué comme en attente"
           })
       });
    },
    markDeliveryasAssingedPickup:async(req,res)=>{
        const id = req.params.id;
        const order=await orders.findById(id);
        if(order.delivery_status==0){
            order.delivery_status=1
            if(order.status==0){
                order.status=1;
            }
            order.save((result)=>{
                return res.send({
                    status:true,
                    message:"marked as processing"
                })
            })
        }else{
            return res.send({
                status:false,
                message:"La commande est en cours de traitement et ne peut donc pas être annulée"
            })
        }
    },
    markDeliveryasAssingedDelivery:async(req,res)=>{
        const id = req.params.id;
        const order=await orders.findById(id);
        if(order.delivery_status==1){
            order.delivery_status=2
            order.save((result)=>{
                return res.send({
                    status:true,
                    message:"marked as unprocessed"
                })
            })
        }else{
            return res.send({
                status:false,
                message:"La commande est en cours de traitement et ne peut donc pas être annulée"
            })
        }
    },
    markDeliveryasDelivered:async(req,res)=>{
        const id = req.params.id;
        const order=await orders.findById(id);
        if(order.delivery_status==2){
            order.delivery_status=3
            order.save((result)=>{
                return res.send({
                    status:true,
                    message:"marked as unprocessed"
                })
            })
        }else{
            return res.send({
                status:false,
                message:"La commande est en cours de traitement et ne peut donc pas être annulée"
            })
        }
    },
    getshopratings:async(req,res)=>{
        const {
            product_name,
            user_name,
            review_time,
            rating,
            to_reply,
            replied,
            seller_id
        }=req.body;
        query={provider_id:seller_id}

        if(product_name){
            query["product_name"]=product_name
        }
        if(user_name){
            query["user_name"]=user_name
        }
        if(rating){
            query["rating"]=rating
        }
        if(review_time){
            query["createdAt"]={$gte:new Date(review_time)}
        }
        if(to_reply){
            query["replies"]={ $exists: true, $eq: [] }
        }
        if(replied){
            query["replies"]={ $exists: true, $ne: [] }
        }
        const reviews=await Ratingnreviews.aggregate([
            {$match:query},
            {
                $addFields:{
                    product_idOBJ:{
                        "$toObjectId":"$product_id"
                    }
                }
            },
            {
                $lookup:{
                    
                        from:"products",
                        localField:"product_idOBJ",
                        foreignField:"_id",
                        as:"product"
                    
                }
            }
        ])
        return res.send({
            status:true,
            data:reviews
        })
    },
    replytoreview:async(req,res)=>{
        const review_id=req.body.id;
        const reply=req.body.reply;
        await Ratingnreviews.findByIdAndUpdate(review_id,{
            $push:{replies:reply}
        }).then((result)=>{
            return res.send({
                status:true,
                message:"replied"
            })
        })
    },
    updatetrackingdetails:async(req,res)=>{
        const {
            order_id,
            date,
            time,
            location,
            comment
        }=req.body;
        await orders.findByIdAndUpdate(order_id,{
            $push:{
                tracking_details:{
                    date:date,
                    time:time,
                    location:location,
                    comment:comment
                }
            }
        }).then((result)=>{
            return res.send({
                status:true,
                message:"updated successfully"
            })
        })
    },
    getTrackingDetails:async(req,res)=>{
        //mongoose.set("debug",true);
        var {
            id,order_id
        }=req.params;
        // console.log("id ",id);
        // console.log("order_id ",order_id );
        order_id = parseFloat(order_id);
        //const tracking_details=await orders.findOne({tracking_number:id});
        const tracking_details=await orders.aggregate([
            {$match: {tracking_number:id} },
            {
                $match:{
                    order_id:order_id
                }
            }
        ]);
        // console.log("tracking_details ",tracking_details);
        // return false;
        if(tracking_details)
        {
            return res.send({
                status:true,
                data:tracking_details
            })
        }else{
            return res.send({
                status:false,
                data:[]
            })
        }
        
    },
    getallprocessingordersbyuserid: async (req, res) => {
        //{awards: {$elemMatch: {award:'National Medal', year:1975}}}
        const userid = req.params.userid;
        const page_no = req.params.page_no;
        let howMuchRecord = 10;
        let fromm = 0;
        if(page_no)
        {
            if(page_no > 0)
            {
                fromm = page_no * howMuchRecord;
            }
        }
        const allorders = await orders.find({
                userId: userid,
                payment_status:true,
                products: {
                    $elemMatch: {
                        status:0
                    }
                }
            }).skip(fromm).limit(howMuchRecord).sort({"createdAt":-1});
        return res.send({
            status: true,
            message: "Succès",
            data: allorders
        })
    },
    getallprocessingordersbyuseridCount: async (req, res) => {
        //{awards: {$elemMatch: {award:'National Medal', year:1975}}}
        try{
            const userid = req.params.userid;
            const allordersCount = await orders.count({
                    userId: userid,
                    payment_status:true,
                    products: {
                        $elemMatch: {
                            status:0
                        }
                    }
                });

            let total = 0;
            var totalPageNumber = 0;
            var perPageRecord = 10;
             
            total = allordersCount;
            totalPageNumber = total / perPageRecord;

            totalPageNumber = Math.ceil(totalPageNumber);
            return res.send({
                status: true,
                message: "Succès",
                totalPageNumber: totalPageNumber
            })
        }catch(e){
            return res.send({
                status: false,
                message: e.message,
                totalPageNumber: 0
            })
        }
        
    },
    getallcompletedordersbyuserid: async (req, res) => {
        const userid = req.params.userid;
        const allorders = await orders.find({ userId: userid,status:1 }).sort({"createdAt":-1});
        return res.send({
            status: true,
            message: "fetched successfully",
            data: allorders
        })
    },
    getallcancelledordersbyuseridCount: async (req, res) => 
    {
        try{
            const userid = req.params.userid;
            const allordersCount = await orders.count({ userId: userid,
                products: {$elemMatch: {status:2}} 
            });
            let total = 0;
            var totalPageNumber = 0;
            var perPageRecord = 10;
             
            total = allordersCount;
            totalPageNumber = total / perPageRecord;
            totalPageNumber = Math.ceil(totalPageNumber);
            return res.send({
                status: true,
                message: "Succès",
                totalPageNumber: totalPageNumber
            })
        }catch(e){
            return res.send({
                status: false,
                message: e.message,
                totalPageNumber: 0
            })
        }
        
    },
    getallcancelledordersbyuserid: async (req, res) => {
        const userid = req.params.userid;
        const page_no = req.params.page_no;
        let howMuchRecord = 10;
        let fromm = 0;
        if(page_no)
        {
            if(page_no > 0)
            {
                fromm = page_no * howMuchRecord;
            }
        }

        const allorders = await orders.find({ userId: userid,
            products: {$elemMatch: {status:2}} 
         }).skip(fromm).limit(howMuchRecord).sort({"createdAt":-1});
        return res.send({
            status: true,
            message: "Succès",
            data: allorders
        })
    },
    getaddressbypartid:async(req,res)=>
    {
        const id=req.body.id;
        const order_id=req.body.order_id;
        const type=req.body.type;
        const part_id = req.body.part_id
        const order=await orders.findById(id);
        const products=order.products;
        let provider_id = "";
        //console.log(req.body);
        // console.log("product",products)
        var indexofarray=parseInt(order_id)
        //console.log("indexofarray",indexofarray)
        indexofarray = indexofarray > 1 ? indexofarray - 1 : indexofarray;
        let orderparts={}
        if(type=="Pickup")
        {

            let aa = order.products.map((val)=>{
                console.log(val);
                if(val.part_id == part_id)
                {
                    provider_id = val.provider_id;
                }
            });
            ///part_id
            // console.log("provider_id ",provider_id);
            // return false;
            singleproduct=products[indexofarray];
            //console.log("hereeee 2277 Pickup ",singleproduct);
            //console.log("order "+order);
            if(provider_id)
            {
                const provider=await seller.findById(provider_id);
                if(provider)
                {
                    if(provider?.pickupaddress.length > 0)
                    {
                        const pickupaddress=provider?.pickupaddress[0];
                        orderparts={order_id:order.order_id+"_"+part_id,_id:order?._id,address:pickupaddress}
                    }
                }
                
            }
        }else if(type=="Delivery"){
            singleproduct=products[indexofarray];
            // const provider=await seller.findById(singleproduct.provider_id);
            // const pickupaddress=provider?.pickupaddress;
            let address=order?.shippingaddress.address.address||"";
            let latlong=order?.shippingaddress.address.latlong||[];
            const shipping_address={
                address:address,
                latlong:latlong,
            }
            orderparts={order_id:order.order_id+"_"+singleproduct?.part_id,_id:order?._id,address:shipping_address}
        }
        

       
        
       
        return res.send({
            status:true,
            data:orderparts
        })
    },
    getorderdeliveryaddress:async(req,res)=>{
        const id=req.params.id;
        const order=await orders.findById(id);
        const products=order.products;
        const orderparts=[]
        for(let i=0;i<products.length;i++){
            singleproduct=products[i];
            if(singleproduct.readytopickup){
                const provider=await seller.findById(singleproduct.provider_id);
                const pickupaddress=provider?.pickupaddress;
                orderparts.push({order_id:order?.order_id+"-"+i+1,_id:order?._id,shipping_address:order?.shipping_address})
            }
        }
      
        
        return res.send({
            status:true,
            data:orderparts
        })
    },
    getallreadytopickuporderparts:async(req,res)=>{
        const id=req.params.id;
        const allorders=await orders.find({ status: 0,payment_status:true });
        const orderparts=[]
        for(let j=0;j<allorders.length;j++){
            let order=allorders[j]
            let products=order.products;
            // console.log("products",products)
        for(let i=0;i<products.length;i++){
            singleproduct=products[i];
             //console.log("singleproduct 1757",singleproduct)
            let provider_id=singleproduct.provider_id||singleproduct.product?.provider_id;
            // console.log("provider_id==>",provider_id)
            if(singleproduct?.readytopickup&&provider_id)
            {
                if(singleproduct?.status == 0)
                {
                    let provider=await seller.findById(provider_id);
                    let pickupaddress=provider?.pickupaddress[0];
                    if(pickupaddress.address)
                    {
                        orderparts.push({order_id:order.order_id+"_"+singleproduct?.part_id,_id:order?._id,index:i+1})
                    }
                }
                
            }
        }
        }
        return res.send({
            status:true,
            data:orderparts
        })
    },
    getallorderdeliveryaddress:async(req,res)=>{
        const allorders=await orders.find({});
        const orderparts=[]
        for(let j=0;j<allorders.length;j++){
            let order=allorders[j]
            let products=order.products;
            // console.log("products",products)
        for(let i=0;i<products.length;i++){
            singleproduct=products[i];
            // console.log("singleproduct",singleproduct)
            let provider_id=singleproduct.provider_id||singleproduct.product?.provider_id;
            console.log("provider_id==>",provider_id)
            if(singleproduct.is_pickedup&&provider_id){
                let provider=await seller.findById(provider_id);
                let pickupaddress=provider?.pickupaddress[0];
                if(pickupaddress.address){
                    orderparts.push({order_id:order?.order_id+"-"+(i+1),_id:order?._id,pickupaddress:pickupaddress})
                }
            }
        }
        }
        return res.send({
            status:true,
            data:orderparts
        })
    }
}