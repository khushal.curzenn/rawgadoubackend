const Subscription=require("../../models/subscriptions/SubscriptionPackages");
module.exports={
   
    createSubPackage :async (req, res) => {
        const { package, price, duration, description,id,type,is_limited,Numberofitems,plusperproduct,
          extracommission } = req.body;
        // let type="seller"
        if(!id){
          const subscription = Subscription.create({
          package,
          price,
          duration,
          description,
         type,
         is_limited,
         Numberofitems,
         plusperproduct,
          extracommission
        }) .then((result) => {
          res.send({"status": "success", "message": "Abonnement créé avec succès", "data": result});
        })
        .catch((err) => {
          res.send(err);
        });
       
         
        }else{
          await Subscription.findByIdAndUpdate(id,{
            ...(package&&{package:package}),
            ...(price.toString()&&{price:price}),
            ...(Numberofitems?.toString()&&{Numberofitems:Numberofitems}),
            ...(duration&&{duration:duration}),
            ...(description&&{description:description}),
            ...(is_limited?.toString()&&{is_limited:is_limited}),
            ...(type?.toString()&&{type:type}),
            ...(plusperproduct?.toString()&&{plusperproduct:plusperproduct}),
            ...(extracommission?.toString()&&{extracommission:extracommission})
          }).then((result) => {
            res.send({"status": "success", "message": "Mise à jour de l'abonnement réussie", "data": result});
          })
          .catch((err) => {
            res.send(err);
          });
        }
      },
    getAllSubscriptions :async(req, res) => {
      console.log("here 47");
    await Subscription
            .aggregate([
              {$match:{_id:{$ne:null}}},
            ])
            .then((result) => {
            res.send(result);
            })
            .catch((err) => {
            res.send(err);
            });
    },
    getSubscriptionById :async (req, res) => {
        const subscription_id  = req.params.id;
        Subscription
            .findById(subscription_id)
            .then((result) => {
            res.send(result);
            })
            .catch((err) => {
            res.send(err);
            });
    },
    deleteSubscription :async (req, res) => {
        const subscription_id  = req.params.id;
        await Subscription
            .findByIdAndDelete(subscription_id)
            .then((result) => {
            res.send(result);
            })
            .catch((err) => {
            res.send(err);
            });
    }
}