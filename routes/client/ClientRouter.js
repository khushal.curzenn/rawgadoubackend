const express =require('express');
const ClientController = require('./ClientController');
const router = express.Router();
const multer=require("multer");
const path=require("path");



router.post('/addClientSubmit',ClientController.addClientSubmit);
router.post('/allClientMerchant',ClientController.allClientMerchant);
router.post('/allClientMerchantCount',ClientController.allClientMerchantCount);
router.post('/getClientDetail',ClientController.getClientDetail);
router.post('/getSingleClientDetail',ClientController.getSingleClientDetail);
router.post('/allClientOnShipment',ClientController.allClientOnShipment);
router.post('/allClientOnShipmentSingle',ClientController.allClientOnShipmentSingle);
router.post('/editClientSubmit',ClientController.editClientSubmit);


module.exports=router;