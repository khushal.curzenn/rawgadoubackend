// utils
// const makeValidation = require('@withvoid/make-validation');
// models
const  { CHAT_ROOM_TYPES ,ChatRoom} = require('../../models/chat/ChatRoom');
const {ChatMessage} = require('../../models/chat/ChatMessage.js');
const UserModel = require('../../models/user/User');
const Chat = require('../../models/chat/chat.js');
const ProductChatModel = require('../../models/chat/ProductChatModel');
const ReportChatModel = require('../../models/chat/ReportChatModel');
const mongoose = require("mongoose");
module.exports= io=> {
  io.on("connection", client => {
    console.log("new connection fdgfdgfdg");

    client.on("disconnect", () => {
      console.log("user disconnected");
    });
    client.on("sendMessage", (data) => {
      try{
        console.log("sendMessage");
        console.log("data 22 No line ");
        //console.log(data);
        //mongoose.set("debug",true);
        var {product_id,seller_id,user_id,message,message_by} = data;
        if(product_id == "" || product_id == null || product_id == "null" || product_id == undefined  )
        { 
          // return res.send({
          //   status:false,
          //   message:"L'identifiant du produit est requis"
          // });
          io.emit("getResponseInitate", {"status":false,success: false,message:"L'identifiant du produit est requis" });
        }
        if(seller_id == "" || seller_id == null || seller_id == "null" || seller_id == undefined  )
        { 
          // return res.send({
          //   status:false,
          //   message:"L'identifiant du vendeur est requis"
          // });
          io.emit("getResponseInitate", {"status":false,success: false,message:"L'identifiant du vendeur est requis" });
        }
        if(user_id == "" || user_id == null || user_id == "null" || user_id == undefined  )
        { 
          // return res.send({
          //   status:false,
          //   message:"L'identifiant de l'utilisateur est requis"
          // });
          io.emit("getResponseInitate", {"status":false,success: false,message:"L'identifiant de l'utilisateur est requis" });
        }
        if(message == "" || message == null || message == "null" || message == undefined  )
        { 
          // return res.send({
          //   status:false,
          //   message:"L'identifiant du message est requis"
          // });
          io.emit("getResponseInitate", {"status":false,success: false,message:"L'identifiant du message est requis" });
        }
        if(message_by == "" || message_by == null || message_by == "null" || message_by == undefined  )
        { 
          // return res.send({
          //   status:false,
          //   message:"Un message par est requis"
          // });
          io.emit("getResponseInitate", {"status":false,success: false,message:"Un message par est requis" });
        }
        const old_chat =  ProductChatModel.findOne({
          "user_id":user_id,
          "product_id":product_id,
        }).exec((err,resulttt)=>{
          if(err)
          {
            console.log(err)
          }else{
            if(resulttt)
            {
              var room_id = resulttt.room_id;
            }else{
              var room_id = Array.from(Array(30), () => Math.floor(Math.random() * 36).toString(36)).join('');
            }
            const pro_chat = new ProductChatModel();
              pro_chat.room_id = room_id;
              pro_chat.product_id = product_id;
              pro_chat.seller_id = seller_id;
              pro_chat.user_id = user_id;
              pro_chat.message = message;
              pro_chat.message_by = message_by;
              pro_chat.save().then((result)=>{
                console.log("line no 87")
                // return res.send({
                //   status:true,
                //   message:"Votre message a été envoyé avec succès",
                //   room_id:room_id
                // })

                //io.emit("getResponseInitate", {"status":true,success: true,message:"Succès",room_id:room_id,chat:[]});

                ProductChatModel.aggregate([
                  {  
                    $match:{
                      room_id:room_id
                    }
                  },
                  
                  {
                    $addFields:{
                      "usr_id":{"$toObjectId":"$user_id"}
                    }
                  },
                  {
                    $lookup:{
                      from:"users",
                      localField:"usr_id",
                      foreignField:"_id",
                      as:"user_rec"
                    }
                  },
                  {
                    $unwind:"$user_rec"
                  },
                  {
                    $addFields:{
                      "first_name":"$user_rec.first_name",
                      "photo":"$user_rec.photo",
                      "last_name":"$user_rec.last_name"
                    }
                  },
                  {
                    $project:{
                      "user_rec":0
                    }
                  },
                  {
                    $addFields:{
                      "slr_id":{"$toObjectId":"$seller_id"}
                    }
                  },
                  {
                    $lookup:{
                      from:"sellers",
                      localField:"slr_id",
                      foreignField:"_id",
                      as:"seller_rec"
                    }
                  },
                  {
                    $unwind:"$seller_rec"
                  },
                  {
                    $addFields:{
                      "shopname":"$seller_rec.shopname",
                      "photo":"$seller_rec.photo",
                      "fullname":"$seller_rec.fullname"
                    }
                  },
                  {
                    $project:{
                      "seller_rec":0
                    }
                  },
                  {
                    $sort:{"created_at":1}
                  }
                ]).then((result1)=>{
                  console.log("line no 135 ");
                  //console.log(result1);
                  io.emit("getResponseInitate", {"status":true,success: true,message:"Succès",room_id:room_id,chat:result1});
                }).catch((error)=>{
                  console.log("139 line no");
                  console.log(error);
                });
              }).catch((err)=>{
                // return res.send({
                //     status:false,
                //     message:err.message
                // })
                // console.log(err);
                io.emit("getResponseInitate", {"status":false,success: false,message:err });
            })
          }
        });
        
      }catch(error)
      {
        console.log(error);
      }

    });

  });



};

