const jwt = require("jsonwebtoken");
const adminmodel = require("../models/admin/Admin");
const config = process.env;
// var LocalStorage = require('node-localstorage').LocalStorage;
//   localStorage = new LocalStorage('./scratch');
const verifyToken = async(req, res, next) => {
  var token =req.session.user_id;
 

  if (token) {
   
   

    return res.redirect("/admin-panel");
   
  }
  
  return next();
};

module.exports = verifyToken;   